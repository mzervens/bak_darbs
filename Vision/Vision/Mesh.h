#pragma once;
#include "ImgHeightMap.cpp";
#include <opencv2/core.hpp>
#include <opencv2/imgproc.hpp>
#include <map>


class Mesh
{
	ImgHeightMap pixelMap;
	map<pair<int, int>, vector<double>> points;
	/// Points of the other side of the leaf veins (these are just to fill bottom)
	map<pair<int, int>, vector<double>> bottomPoints;
	vector<vector<pair<int, int>>> triangles;
	vector<vector<pair<int, int>>> faces;
	cv::Mat image;
	
	double lowestPnt;

	vector<pair<pair<int, int>, vector<double>>> smoothPoints;
	vector<vector<pair<int, int>>> borderTris;

	

private:
	bool outOfBounds(pair<int, int>);

public:
	double maxHeight;
	Mesh(ImgHeightMap pixelMap, cv::Mat img);

	vector<vector<vector<double>>> getVoxelGrid();
	vector<vector<vector<double>>> getVoxelGrid2();
	void boxMesh();
	void findFaces2();
	void findFaces();
	void triangulate();
	double roundTo2Places(double num);

	void generatePointsFromMap();
	void generatePointsFromMap2();
	void generatePointsFromSimpMap(vector<vector<int>> map);

	/// used by the generateBottomPoints
	void setBorderPixelDists(vector<vector<double>> &dists, cv::Mat &border, pair<int, int> pnt);
	void generateBottomPoints();
	void generateBottomPoints2();
	void generateBotPntsPaper();
	vector<pair<int, int>> pixelBorderParents(cv::Mat &border, pair<int, int> pnt);
	vector<pair<int, int>> pixelNeighbours(cv::Mat &border, pair<int, int> pnt);
	vector<pair<int, int>> pixelNeighboursWithNoBordPnts(cv::Mat &border, pair<int, int> pnt);

	void exportPointsToWavefront(string objectName, string filePath);
	void exportBottomPointsToWavefront(string objectName, string filePath);
	void exportFrontBottomPointsToWavefront(string objectName, string filePath);
	void exportPointsToPCL(string objectName, string filePath); 
};
