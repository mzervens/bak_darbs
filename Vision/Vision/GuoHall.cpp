#pragma once;

#include <opencv2/core.hpp>
#include <opencv2/imgproc.hpp>
#include <iostream>
#include <string>

using namespace std;
using namespace cv;

namespace Thinning
{
	class GuoHall
	{
		/**
		* Perform one thinning iteration.
		*
		* @param  im    Binary image with range = 0-1
		* @param  iter  0=even, 1=odd
		*/
		static void thinningIteration(Mat& im, int iter)
		{
			Mat marker = Mat::zeros(im.size(), CV_8UC1);

			for (int i = 1; i < im.rows; i++)
			{
				for (int j = 1; j < im.cols; j++)
				{

					uchar p2 = im.at<uchar>(i - 1, j);
					uchar p8 = im.at<uchar>(i, j - 1);
					uchar p9 = im.at<uchar>(i - 1, j - 1);
					uchar p3;
					uchar p4;
					uchar p5;
					uchar p6;
					uchar p7;

					if (j + 1 >= im.cols)
					{
						p3 = 0;
						p4 = 0;
					}
					else
					{
						p3 = im.at<uchar>(i - 1, j + 1);
						p4 = im.at<uchar>(i, j + 1);
					}

					if (i + 1 >= im.rows)
					{
						p6 = 0;
						p7 = 0;
					}
					else
					{
						p6 = im.at<uchar>(i + 1, j);
						p7 = im.at<uchar>(i + 1, j - 1);
					}

					if (i + 1 >= im.rows || j + 1 >= im.cols)
					{
						p5 = 0;
					}
					else
					{
						p5 = im.at<uchar>(i + 1, j + 1);
					}


					int C = (!p2 & (p3 | p4)) + (!p4 & (p5 | p6)) +
						(!p6 & (p7 | p8)) + (!p8 & (p9 | p2));
					int N1 = (p9 | p2) + (p3 | p4) + (p5 | p6) + (p7 | p8);
					int N2 = (p2 | p3) + (p4 | p5) + (p6 | p7) + (p8 | p9);
					int N = N1 < N2 ? N1 : N2;
					int m = iter == 0 ? ((p6 | p7 | !p9) & p8) : ((p2 | p3 | !p5) & p4);

					if (C == 1 && (N >= 2 && N <= 3) & m == 0)
						marker.at<uchar>(i, j) = 1;
				}
			}

			im &= ~marker;
		}

	public:
		
		/**
		* Function for thinning the given binary image
		*
		* @param  im  Binary image with range = 0-255
		*/
		static void thinn(Mat& im)
		{
			im /= 255;

			Mat prev = Mat::zeros(im.size(), CV_8UC1);
			Mat diff;

			do {
				thinningIteration(im, 0);
				thinningIteration(im, 1);
				absdiff(im, prev, diff);
				im.copyTo(prev);
			} while (countNonZero(diff) > 0);

			im *= 255;
		}
	};


};