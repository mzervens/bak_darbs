#pragma once;
#include <opencv2/core.hpp>
#include <opencv2/imgproc.hpp>
#include <iostream>
#include <string>
#include <ppl.h>
#include <stack>
#include <map>
#include "ImgProc.cpp";
#include "Structs.cpp";
#include "Utility.cpp";
#include "vec2.h";

using namespace std;
using namespace cv;


class RidgeEdgeDetect
{
	Mat ridges;
	Mat edges;
	Mat imgRGB;

	Mat pixelTypeMap;
	vector<pair<uint, int>> pixelTypeCnt;
	int maxPixelTypeIndx;


	int maxColorChange;

	Mat lastIterRes;

public:
	RidgeEdgeDetect(Mat ridges, Mat edges, Mat rgbImg)
	{
		this->ridges = ridges;
		this->edges = edges;

		imgRGB = rgbImg;

		maxColorChange = 30 * 3;
		RGBHist(); 
	}

	RidgeEdgeDetect(Mat rgbImg)
	{
		imgRGB = rgbImg;
		maxColorChange = 30 * 3;
		RGBHist();
	}

	pair<bool, int> similarColors(int rgbSum1, int rgbSum2)
	{
		int dif = abs(rgbSum1 - rgbSum2);
		if (dif <= maxColorChange)
		{
			return make_pair(true, dif);
		}

		return make_pair(false, dif);
	}

	double colAvg(uint sum, int count)
	{
		return (double)sum / count;
	}

	void RGBHist()
	{
		auto pixelTypes = vector<pair<uint, int>>();
		Mat typeMap(imgRGB.size(), CV_8UC1, Scalar(0));

		for (int y = 0; y < imgRGB.rows; y++)
		{
			for (int x = 0; x < imgRGB.cols; x++)
			{
				uint sum = (uchar)imgRGB.at<Vec3b>(y, x)[0] + (uchar)imgRGB.at<Vec3b>(y, x)[1] + (uchar)imgRGB.at<Vec3b>(y, x)[2];
				if (pixelTypes.size() == 0)
				{
					pixelTypes.push_back(make_pair(sum, 1));
					typeMap.at<uchar>(y, x) = 1;
				}
				else
				{
					bool added = false;
					int minIdx = -1;
					int minDifVal = maxColorChange + 1;

					for (int i = 0; i < pixelTypes.size(); i++)
					{
						auto similarity = similarColors(colAvg(pixelTypes[i].first, pixelTypes[i].second), sum);
						if (similarity.first)
						{
							added = true;
							if (similarity.second < minDifVal)
							{
								minIdx = i;
								minDifVal = similarity.second;
							}
						}
					}

					if (!added)
					{
						pixelTypes.push_back(make_pair(sum, 1));
						typeMap.at<uchar>(y, x) = pixelTypes.size();
					}
					else
					{
						pixelTypes[minIdx].first += sum;
						pixelTypes[minIdx].second += 1;
						typeMap.at<uchar>(y, x) = (minIdx + 1);
					}
				}

			}

		}

		auto pixelTypesMerg = vector<pair<uint, int>>();
		map<int, bool> processedIndx = map<int, bool>();

		for (int i = 0; i < pixelTypes.size(); i++)
		{
			if (processedIndx.find(i) != processedIndx.end())
			{
				continue;
			}

			auto mergeIndxs = vector<int>();
			mergeIndxs.push_back(i);

			for (int n = 0; n < pixelTypes.size(); n++)
			{
				if (i == n || processedIndx.find(n) != processedIndx.end())
				{
					continue;
				}

				uint sum = 0;
				int cnt = 0;
				for (int s = 0; s < mergeIndxs.size(); s++)
				{
					sum += pixelTypes[mergeIndxs[s]].first;
					cnt += pixelTypes[mergeIndxs[s]].second;
				}

				if (similarColors(colAvg(sum, cnt), colAvg(pixelTypes[n].first, pixelTypes[n].second)).first)
				{
					mergeIndxs.push_back(n);
				}
			}

			for (int s = 0; s < mergeIndxs.size(); s++)
			{
				for (int y = 0; y < typeMap.rows; y++)
				{
					for (int x = 0; x < typeMap.cols; x++)
					{
						if (typeMap.at<uchar>(y, x) == (mergeIndxs[s] + 1))
						{
							typeMap.at<uchar>(y, x) = (pixelTypesMerg.size() + 1);
						}
					}
				}
			}


			pair<uint, int> mPair = make_pair(0, 0);
			for (int s = 0; s < mergeIndxs.size(); s++)
			{
				mPair.first += pixelTypes[mergeIndxs[s]].first;
				mPair.second += pixelTypes[mergeIndxs[s]].second;
				processedIndx.insert(make_pair(mergeIndxs[s], true));
			}

			pixelTypesMerg.push_back(mPair);
			
		}

		pixelTypeMap = typeMap;
		pixelTypeCnt = pixelTypesMerg;
		int maxI = 0;
		int maxCnt = 0;

		for (int i = 0; i < pixelTypesMerg.size(); i++)
		{
			if (pixelTypesMerg[i].second > maxCnt)
			{
				maxCnt = pixelTypesMerg[i].second;
				maxI = i;
			}
		}

		maxPixelTypeIndx = maxI + 1;

		cout << "background indx " << maxPixelTypeIndx << endl;
		for (int i = 0; i < pixelTypesMerg.size(); i++)
		{
			cout << "type col sum " << colAvg(pixelTypesMerg[i].first, pixelTypesMerg[i].second) << " num pixels " << pixelTypesMerg[i].second << endl;
		}
		/*
		for (int i = 0; i < pixelTypes.size(); i++)
		{
			cout << "type col sum " << colAvg(pixelTypes[i].first, pixelTypes[i].second) << " num pixels " << pixelTypes[i].second << endl;
		}

		cout << endl;
		for (int i = 0; i < pixelTypesMerg.size(); i++)
		{
			cout << "type col sum " << colAvg(pixelTypesMerg[i].first, pixelTypesMerg[i].second) << " num pixels " << pixelTypesMerg[i].second << endl;
		}

		int inteVal = (256 / pixelTypesMerg.size()) - 1;
		for (int y = 0; y < typeMap.rows; y++)
		{
			for (int x = 0; x < typeMap.cols; x++)
			{
				typeMap.at<uchar>(y, x) = (typeMap.at<uchar>(y, x) * inteVal);
			}
		}

		Utility::writeImage(typeMap, "C:/Users/Matiss/Desktop/intVals.png");
		*/
	}

	vec2<int> perpVec(vec2<int> vec)
	{
		return vec2<int>(-vec.y, vec.x);
	}

	bool valInHighBucket(vec2<int> pixel)
	{
		uchar indx = pixelTypeMap.at<uchar>(pixel.y, pixel.x);
		
		if (indx == maxPixelTypeIndx)
		{
			return true;
		}

		return false;
	}


	Mat printPixelTypeMap()
	{
		int inteVal = (256 / pixelTypeCnt.size()) - 1;
		for (int y = 0; y < pixelTypeMap.rows; y++)
		{
			for (int x = 0; x < pixelTypeMap.cols; x++)
			{
				pixelTypeMap.at<uchar>(y, x) = (pixelTypeMap.at<uchar>(y, x) * inteVal);
			}
		}

		return pixelTypeMap;

	}

	Mat fillGaps()
	{
		auto closeNeighbs = vector<vec2<int>>({ vec2<int>(1, 0), vec2<int>(0, 1) });
		for (int y = 0; y < lastIterRes.rows; y++)
		{
			for (int x = 0; x < lastIterRes.cols; x++)
			{
				if (lastIterRes.at<uchar>(y, x) == 0)
				{
					vec2<int> pos(x, y);
					bool ok = true;
					for (int i = 0; i < closeNeighbs.size(); i++)
					{
						if (ImgProc::inBoundsWithVal(lastIterRes, (pos + closeNeighbs[i]), 255) &&
							ImgProc::inBoundsWithVal(lastIterRes, (pos - closeNeighbs[i]), 255))
						{
							continue;
						}
						else
						{
							ok = false;
						}
					}

					if (ok)
					{
						lastIterRes.at<uchar>(y, x) = 255;
					}
				}
			}
		}

		Mat visited(lastIterRes.size(), CV_8UC1, Scalar(0));

		for (int y = 0; y < lastIterRes.rows; y++)
		{
			for (int x = 0; x < lastIterRes.cols; x++)
			{
				if (lastIterRes.at<uchar>(y, x) == 255 || valInHighBucket(vec2<int>(x, y)))
				{
					continue;
				}

				auto st = stack<vec2<int>>();
				auto fillPixs = vector<vec2<int>>();
				st.push(vec2<int>(x, y));
				bool canFill = true;


				while (!st.empty())
				{
					auto el = st.top();
					st.pop();
					fillPixs.push_back(el);

					for (int i = 0; i < closeNeighbs.size(); i++)
					{
						vec2<int> neib1 = (el + closeNeighbs[i]);
						vec2<int> neib2 = (el - closeNeighbs[i]);

						

						if (!ImgProc::inBounds(lastIterRes, neib1) ||
							!ImgProc::inBounds(lastIterRes, neib2))
						{
							canFill = false;
							break;
						}

						if (valInHighBucket(neib1) || valInHighBucket(neib2))
						{
							canFill = false;
							break;
						}

						visited.at<uchar>(neib1.y, neib1.x) = 255;
						visited.at<uchar>(neib2.y, neib2.x) = 255;

						if (ImgProc::inBoundsWithVal(lastIterRes, neib1, 0) && !ImgProc::inBoundsWithVal(visited, neib1, 255))
						{
							st.push(neib1);
						}

						if (ImgProc::inBoundsWithVal(lastIterRes, neib2, 0) && !ImgProc::inBoundsWithVal(visited, neib2, 255))
						{
							st.push(neib2);
						}
					}

					if (!canFill)
					{
						break;
					}
				}

				if (canFill)
				{
					for (int i = 0; i < fillPixs.size(); i++)
					{
						lastIterRes.at<uchar>(fillPixs[i].y, fillPixs[i].x) = 255;
					}
				}

			}
		}

		return (lastIterRes - edges);
	}

	Mat doIteration()
	{
		Mat result = edges.clone();
		vec2<int> dirVec(0, 0);
		vec2<int> yOfs(0, 1);
		vec2<int> xOfs(1, 0);
		vec2<int> xyOfs(1, 1);
		vec2<int> xyOfs2(-1, 1);
		auto dirs = vector<vec2<int>>({ yOfs, xOfs, xyOfs, xyOfs2 });

		for (int y = 0; y < ridges.rows; y++)
		{
			for (int x = 0; x < ridges.cols; x++)
			{

				if (ridges.at<uchar>(y, x) == 0)
				{
					continue;
				}


				for (int d = 0; d < dirs.size(); d++)
				{
				vec2<int> curPos(x, y);
				dirVec = dirs[d];
				/*

				if (ImgProc::inBounds(ridges, (curPos + yOfs)) &&						// ridge goes vertical
					ridges.at<uchar>((curPos + yOfs).y, (curPos + yOfs).x) != 0 ||
					ImgProc::inBounds(ridges, (curPos - yOfs)) &&
					ridges.at<uchar>((curPos - yOfs).y, (curPos - yOfs).x) != 0)
				{
					dirVec = perpVec(yOfs);
				}
				else if (ImgProc::inBounds(ridges, (curPos + xOfs)) &&	// ridge goes horizontal
					ridges.at<uchar>((curPos + xOfs).y, (curPos + xOfs).x) != 0 ||
					ImgProc::inBounds(ridges, (curPos - xOfs)) &&
					ridges.at<uchar>((curPos - xOfs).y, (curPos - xOfs).x) != 0)
				{
					dirVec = perpVec(xOfs);
				}
				else if (ImgProc::inBounds(ridges, (curPos + xyOfs)) &&	// ridge goes horizontal
					ridges.at<uchar>((curPos + xyOfs).y, (curPos + xyOfs).x) != 0 ||
					ImgProc::inBounds(ridges, (curPos - xyOfs)) &&
					ridges.at<uchar>((curPos - xyOfs).y, (curPos - xyOfs).x) != 0)
				{
					dirVec = perpVec(xyOfs);
				}
				else if (ImgProc::inBounds(ridges, (curPos + xyOfs2)) &&	// ridge goes horizontal
					ridges.at<uchar>((curPos + xyOfs2).y, (curPos + xyOfs2).x) != 0 ||
					ImgProc::inBounds(ridges, (curPos - xyOfs2)) &&
					ridges.at<uchar>((curPos - xyOfs2).y, (curPos - xyOfs2).x) != 0)
				{
					dirVec = perpVec(xyOfs2);
				}
				else
				{
					dirVec = perpVec(xOfs);
					//continue;
				}
				*/



				vec2<int> lft(x, y);
				vec2<int> rgt(x, y);
				int numOfLftPixsInMaxBucket = 0;
				int numOfRgtPixsInMaxBucket = 0;
				int lftDist = 0;
				int rgtDist = 0;
				
				while (ImgProc::inBounds(edges, lft))
				{
					if (ImgProc::inBoundsWithVal(edges, lft, 255))
					{
						break;
					}
					else
					{
						if (valInHighBucket(lft))
						{
							numOfLftPixsInMaxBucket++;
						}
						
						lft = lft - dirVec;
						lftDist++;
						
					}
				}


				while (ImgProc::inBounds(edges, rgt))
				{
					if (ImgProc::inBoundsWithVal(edges, rgt, 255))
					{
						break;
					}
					else
					{
						if (valInHighBucket(rgt))
						{
							numOfRgtPixsInMaxBucket++;
						}
						
						rgt = rgt + dirVec;
						rgtDist++;
					}
				}

				
				if (ImgProc::inBoundsWithVal(edges, lft, 255) && ImgProc::inBoundsWithVal(edges, rgt, 255))
				{
					int diff = abs(lftDist - rgtDist);
					int totalDist = rgtDist + lftDist;
					int highPixs = numOfLftPixsInMaxBucket + numOfRgtPixsInMaxBucket;

					/*
					if (numOfLftPixsInMaxBucket > 0 || numOfRgtPixsInMaxBucket > 0)
					{
						continue;
					}
					*/

					if (highPixs > 0 && (((double)totalDist / highPixs) > 0.1))
					{
						continue;
					}


				//	if (diff <= 20) // mark the path 
					if (true)
					{
						while (lft != curPos)
						{
							result.at<uchar>(lft.y, lft.x) = 255;
							lft = lft + dirVec;
						}

						while (rgt != curPos)
						{
							result.at<uchar>(rgt.y, rgt.x) = 255;
							rgt = rgt - dirVec;
						}
					}

					result.at<uchar>(y, x) = 255;

				}
				
				}
			}
		}

		lastIterRes = result;
		return result;
	}
};