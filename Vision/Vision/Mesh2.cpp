#pragma once

#include <stack>
#include <map>
#include <iostream>
#include <fstream>
#include "Utility.cpp";
#include "ImgProc.cpp";
#include "ImgHeightMap.cpp";
#include "Spline.h";

using namespace std;
using namespace cv;


class Mesh2
{
	ImgHeightMap hMap;
	Mat bImg; // skeleton
	Mat eImg; // edges

public:
	Mesh2(ImgHeightMap heightMap, Mat skImg, Mat edgeImg)
	{
		hMap = heightMap;
		bImg = skImg;
		eImg = edgeImg;
	}

	pair<int, int> imPos(int x, int y)
	{
		return make_pair(x, y);
	}

	int sideOfLine(pair<int, int> l1, pair<int, int> l2, pair<int, int> t)
	{
		return sideOfLine(Point(l1.first, l1.second), Point(l2.first, l2.second), Point(t.first, t.second));
	}

	int sideOfLine(Point l1, Point l2, Point t)
	{
		return ((l2.x - l1.x) * (t.y - l1.y) - (l2.y - l1.y) * (t.x - l1.x));
	}

	void trigCurveWithEdges(CRSpline * c, CRSpline * e1, CRSpline * e2, float res = 2.0, string key = "")
	{
		fstream f;
		f.open("C:/Users/Matiss/Desktop/ttt/sm_tr" + key + ".obj", ios::out);

		float curVal = 0.0;
		float prevVal = 0.0;
		int indx = 1;
		float iter = 1 / (c->vp.size() * res);
		curVal += iter;

		auto verts = vector<vec3>();
		auto quads = vector<int>(); 

		while (curVal < 1.0)
		{
			auto cV1 = c->GetInterpolatedSplinePoint(prevVal);
			auto cV2 = c->GetInterpolatedSplinePoint(curVal);

			auto e1V1 = e1->GetInterpolatedSplinePoint(prevVal);
			auto e1V2 = e1->GetInterpolatedSplinePoint(curVal);

			auto e2V1 = e2->GetInterpolatedSplinePoint(prevVal);
			auto e2V2 = e2->GetInterpolatedSplinePoint(curVal);

			f << "v " << cV1.x << " " << cV1.z << " " << cV1.y << endl;
			f << "v " << cV2.x << " " << cV2.z << " " << cV2.y << endl;
			f << "v " << e1V2.x << " " << e1V2.z << " " << e1V2.y << endl;
			f << "v " << e1V1.x << " " << e1V1.z << " " << e1V1.y << endl;
			f << "v " << e2V2.x << " " << e2V2.z << " " << e2V2.y << endl;
			f << "v " << e2V1.x << " " << e2V1.z << " " << e2V1.y << endl;

			quads.push_back(indx);
			quads.push_back(indx + 1);
			quads.push_back(indx + 2);
			quads.push_back(indx + 3);

			quads.push_back(indx);
			quads.push_back(indx + 1);
			quads.push_back(indx + 4);
			quads.push_back(indx + 5);

			indx += 6;

			prevVal = curVal;
			curVal += iter;
		}


		for (int i = 0; i < quads.size(); i += 4)
		{
			f << "f " << quads[i] << " " << quads[i + 1] << " " << quads[i + 2] << " " << quads[i + 3] << endl;
		}

		f.close();
	}

	bool pointAccessible(pair<int, int> p1, pair<int, int> p2, int maxDist = 6)
	{
		auto st = stack<pair<pair<int, int>, int>>();
		auto vsted = map<pair<int, int>, bool>();
		st.push(make_pair(p1, 0));
		
		int kern = 1;

		while (!st.empty())
		{
			auto el = st.top();
			vsted.insert(make_pair(el.first, true));
			st.pop();

			if (el.second + 1 >= maxDist)
			{
				continue;
			}

			for (int y = -kern; y <= kern; y++)
			{
				for (int x = -kern; x <= kern; x++)
				{
					auto pPos = make_pair(x + el.first.first, y + el.first.second);
					if (pPos == p2)
					{
						return true;
					}

					if (ImgProc::inBoundsWithVal(eImg, pPos, 255) && vsted.find(pPos) == vsted.end())
					{
						st.push(make_pair(pPos, el.second + 1));
					}
				}
			}
		}

		return false;
	}

	vector<CRSpline*> findEdgeCurvesFromCenter(CRSpline * cnt)
	{
		int maxPrevPntDst = 3;
		int currPrevPntDst = 0;
		auto curves = vector<CRSpline*>();
		CRSpline * side1 = new CRSpline(); // positive side points
		CRSpline * side2 = new CRSpline(); // negative side points
		bool xIncr = true;
		auto prevSPnt = pair<int, int>(-1, -1);

		for (int i = 0; i < cnt->vp.size(); i++)
		{
			int pX = cnt->vp[i].x;
			int pY = cnt->vp[i].y;

			if (prevSPnt.first < 0) // first pnt
			{
				if (cnt->vp[i + 1].x >= pX)
				{
					xIncr = true;
				}
				else
				{
					xIncr = false;
				}

			}
			else
			{
				if (prevSPnt.first <= pX)
				{
					xIncr = true;
				}
				else
				{
					xIncr = false;
				}
			}


			auto curSPnt = make_pair(pX, pY); 

			int curKer = 1;
			auto minXYPnt = pair<int, int>(pX, pY);
			auto maxXYPnt = pair<int, int>(pX, pY);
			auto borderPnts = vector<pair<int, int>>();
			auto pozPnt = Triple<pair<int, int>, int, bool>(make_pair(-1, -1), -1, false);
			auto negPnt = Triple<pair<int, int>, int, bool>(make_pair(-1, -1), -1, false);

			while (true)
			{ 

				for (int yO = -curKer; yO <= curKer; yO++)
				{
					for (int xO = -curKer; xO <= curKer; xO++)
					{
						auto iP = make_pair(pX + xO, pY + yO);

						if (ImgProc::inBoundsWithVal(eImg, iP, 255)) // check if pixel is on the border
						{
							borderPnts.push_back(iP);
						}
					/*	else if (ImgProc::inBoundsWithVal(bImg, iP, 255)) // update current skelet min point / max point
						{
							if (iP.first < minXYPnt.first)
							{
								minXYPnt = iP;
							}
							else if (iP.first == minXYPnt.first && iP.second < minXYPnt.second) // for vertical skelet fragments
							{
								minXYPnt = iP;
							}
							
							else if (iP.first > maxXYPnt.first)
							{
								maxXYPnt = iP;
							}
							
							else if (iP.first == maxXYPnt.first && iP.second > maxXYPnt.second)
							{
								maxXYPnt = iP;
							}
							
						}
						*/
					}
				}


				for (int s = 0; s < borderPnts.size(); s++)
				{
					auto iP = borderPnts[s];
					int side;// = sideOfLine(minXYPnt, maxXYPnt, iP);

					if (prevSPnt.first < 0) // first pnt
					{
						side = sideOfLine(prevSPnt, make_pair(cnt->vp[i + 1].x, cnt->vp[i + 1].y), iP);
					}
					else
					{
						auto prevSide1Pnt = make_pair((int)side1->vp[i - 1].x, (int)side1->vp[i - 1].y);
						if (pointAccessible(prevSide1Pnt, iP))
						{
							side = 1;
						}
						else
						{
							side = -1;
						}
					//	side = sideOfLine(prevSPnt, curSPnt, iP);
					//	side = sideOfLine(make_pair(cnt->vp[0].x, cnt->vp[0].y), curSPnt, iP);
					}
					
					
					/*
					
					if (!xIncr)
					{
						side = -side;
					}
					*/


					int diff = abs(pX - iP.first) + abs(pY - iP.second);

					if (side < 0)
					{
						if (!negPnt.thrd || negPnt.scnd > diff)
						{
							negPnt.fst = iP;
							negPnt.scnd = diff;
							negPnt.thrd = true;
						}

					}
					else if (side > 0)
					{
						if (!pozPnt.thrd || pozPnt.scnd > diff)
						{
							pozPnt.fst = iP;
							pozPnt.scnd = diff;
							pozPnt.thrd = true;
						}
					}
				}

				if (negPnt.thrd && pozPnt.thrd)
				{
					float heightPoz = Utility::pointDist(pozPnt.fst, curSPnt);
					float heightNeg = Utility::pointDist(negPnt.fst, curSPnt);
					
					side1->AddSplinePoint(vec3(pozPnt.fst.first, pozPnt.fst.second, -heightPoz));
					side2->AddSplinePoint(vec3(negPnt.fst.first, negPnt.fst.second, -heightNeg));
					break;
				}
				else
				{
					curKer += 1;
					borderPnts = vector<pair<int, int>>();
				}
			}

			currPrevPntDst += 1;
			if (currPrevPntDst >= maxPrevPntDst)
			{
				currPrevPntDst = 1;
				prevSPnt = curSPnt;
			}

			//prevSPnt = curSPnt;

		}

		curves.push_back(side1);
		curves.push_back(side2);

		return curves;

	}

	vector<CRSpline*> partitionCenterCurves()
	{
		auto curves = vector<CRSpline*>();
		Mat vist(bImg.size(), CV_8UC1, Scalar(0));
		int kern = 1;

		for (int y = 0; y < bImg.rows; y++)
		{
			for (int x = 0; x < bImg.cols; x++)
			{
				if (bImg.at<uchar>(y, x) == 0 || vist.at<uchar>(y, x) == 255)
				{
					continue;
				}

				auto cons = vector<pair<int, int>>();
				for (int yO = -kern; yO <= kern; yO++)
				{
					for (int xO = -kern; xO <= kern; xO++)
					{
						int posX = x + xO,
							posY = y + yO;

						if (posX == x && posY == y)
						{
							continue;
						}

						if (ImgProc::inBoundsWithVal(bImg, imPos(posX, posY), 255) && vist.at<uchar>(posY, posX) != 255)
						{
							cons.push_back(make_pair(posX, posY));
						}
					}
				}

				if (cons.size() != 1)
				{
					continue;
				}

				auto nextStrts = stack<pair<int, int>>();
				CRSpline * spl = new CRSpline();
				spl->AddSplinePoint(vec3(x, y, 0));
				vist.at<uchar>(y, x) = (uchar)255;
				auto st = stack<pair<int, int>>();
				
				st.push(cons[0]);
				cons = vector<pair<int, int>>();

				while (!st.empty())
				{
					auto curP = st.top();
					st.pop();
					vist.at<uchar>(curP.second, curP.first) = (uchar)255;
					spl->AddSplinePoint(vec3(curP.first, curP.second, 0));

					for (int yO = -kern; yO <= kern; yO++)
					{
						for (int xO = -kern; xO <= kern; xO++)
						{
							int posX = curP.first + xO,
								posY = curP.second + yO;

							if (posX == curP.first && posY == curP.second)
							{
								continue;
							}

							if (ImgProc::inBoundsWithVal(bImg, imPos(posX, posY), 255) && vist.at<uchar>(posY, posX) != 255)
							{
								cons.push_back(make_pair(posX, posY));
							}
						}
					}

					if (cons.size() == 1)
					{
						st.push(cons[0]);
					}
					else if (cons.size() > 1)
					{
						//st.push(cons[0]);
						for (int nxt = 0; nxt < cons.size(); nxt++)
						{
							nextStrts.push(cons[nxt]);
						}

						curves.push_back(spl);
					}
					else
					{
						curves.push_back(spl);
						
						if (!nextStrts.empty())
						{
							spl = new CRSpline();
							st.push(nextStrts.top());
							nextStrts.pop();
						}
					}

					cons = vector<pair<int, int>>();

				}
			}
		}


		cout << "found splines " << curves.size() << endl;
	//	cout << "fst len " << curves[0]->vp.size() << endl;
	//	cout << "scnd len " << curves[1]->vp.size() << endl;

		return curves;
	}


	void exportSpline(CRSpline * spl, float prec = 2.0, string key = "")
	{
		float iteration = 1 / (spl->GetNumPoints() * prec);
		float pnt = 0.0;
		auto vals = vector<vec3>();

		while (pnt < 1.0)
		{
			vec3 valAt = spl->GetInterpolatedSplinePoint(pnt);
			vals.push_back(valAt);
			pnt += iteration;
		}

		vals.push_back(spl->GetInterpolatedSplinePoint(1.0));


		// write to .obj
		fstream f;
		f.open("C:/Users/Matiss/Desktop/smcurv" + key + ".obj", ios::out);
		for (int i = 0; i < vals.size(); i++)
		{
			auto val = vals[i];
			f << "v " << val.x << " " << val.z << " " << val.y << endl;
		}

		f << "l ";
		for (int i = 1; i <= vals.size(); i++)
		{
			f << i << " ";
		}

		f << endl;

		f.close();
	}

};