#pragma once;

#include <string>
#include <iostream>
#include <vector>
#include <map>

using namespace std;

class PixelHeightMap
{
public:
	int x;
	int y;
	double height;
	double skelMaxDist;
	vector<PixelHeightMap*> skeletParent; // for childs
	map<string, PixelHeightMap*> skelChilds;

	PixelHeightMap()
	{
		x = y = -1;
		height = -1;
		skelMaxDist = -1;
		skelChilds = map<string, PixelHeightMap*>();
		skeletParent = vector<PixelHeightMap*>();
	}

	PixelHeightMap(double initHeight) : PixelHeightMap()
	{
		height = initHeight;
	}

	void calcSkeletMaxDist()
	{
		for (auto &child : skelChilds)
		{
			if (skelMaxDist < child.second->height)
			{
				skelMaxDist = child.second->height;
			}
		}
	}

	void removeChild(string key)
	{
		skelChilds.erase(key);
	}

	// change back to one parent..
	void addChild(PixelHeightMap * posChild, int posX, int posY, double dist)
	{
		string key = to_string(posX) + " " + to_string(posY);

		if (posChild->skeletParent.size() == 0)
		{
			posChild->skeletParent.push_back(this);// = this;
			pair<string, PixelHeightMap*> child(key, posChild);

			skelChilds.insert(child);
			return;
		}

		if (posChild->height < dist) // the child has a parent with a lower distance than this parent
		{
			return;
		}
/*		else if (posChild->height = dist)
		{
			if (skelChilds.find(key) != skelChilds.end())
			{
				return;
			}

			posChild->skeletParent.push_back(this);
			pair<string, PixelHeightMap*> child(key, posChild);

			skelChilds.insert(child);
			return;
		}
		*/
		else
		{
		//	cout << "here " << endl;
			// unlink this child from the previous parent and add it to this parent
			for (int i = 0; i < posChild->skeletParent.size(); i++)
			{
				posChild->skeletParent[i]->removeChild(key);
			}
			posChild->skeletParent.clear();
			posChild->skeletParent.push_back(this);
			//posChild->skeletParent->removeChild(key);
			//posChild->skeletParent = this;
			pair<string, PixelHeightMap*> child(key, posChild);
			skelChilds.insert(child);
			return;
		}
	}

	/*
	void addChild(PixelHeightMap * posChild, int posX, int posY, double dist)
	{
		string key = to_string(posX) + " " + to_string(posY);

		if (posChild->skeletParent.size() == 0)
		{
			posChild->skeletParent.push_back(this);// = this;
			pair<string, PixelHeightMap*> child(key, posChild);

			skelChilds.insert(child);
			return;
		}

		if (posChild->height < dist) // the child has a parent with a lower distance than this parent
		{
			return;
		}
		else if (posChild->height = dist)
		{
			if (skelChilds.find(key) != skelChilds.end())
			{
				return;
			}

			posChild->skeletParent.push_back(this);
			pair<string, PixelHeightMap*> child(key, posChild);

			skelChilds.insert(child);
			return;
		}
		else
		{

			// unlink this child from the previous parent and add it to this parent
			for (int i = 0; i < posChild->skeletParent.size(); i++)
			{
				posChild->skeletParent[i]->removeChild(key);
			}
			posChild->skeletParent.clear();
			posChild->skeletParent.push_back(this);
			//posChild->skeletParent->removeChild(key);
			//posChild->skeletParent = this;
			pair<string, PixelHeightMap*> child(key, posChild);
			skelChilds.insert(child);
			return;
		}
	}
	*/

	void updateMinHeight(int x, int y, double newHeight)
	{
		if (height > newHeight || height == -1)
		{
			height = newHeight;
		}

		this->x = x;
		this->y = y;
	}
};