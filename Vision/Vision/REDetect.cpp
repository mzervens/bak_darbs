#pragma once;

#include <opencv2/core.hpp>
#include <opencv2/imgproc.hpp>
#include <iostream>
#include <string>
#include <vector>
#include "ImgProc.cpp";

using namespace std;


class REDetect
{
	int maxR;
	Mat ridges;
	Mat edges;

	Mat result;

	void copyRidgesToRes()
	{
		for (int y = 0; y < ridges.rows; y++)
		{
			for (int x = 0; x < ridges.cols; x++)
			{
				if (ridges.at<uchar>(y, x) == 255)
				{
					result.at<uchar>(y, x) = 255;
				}
			}
		}
	}

public:
	REDetect(Mat edges, Mat ridges, int maxVeinDiameterPx) : edges(edges), ridges(ridges)
	{
		maxR = maxVeinDiameterPx / 2;
	}

	Mat getRes()
	{
		return result;
	}

	void detect()
	{
		Mat ridg = ridges.clone();
		Mat nextRidg = ridg.clone();
		int kern = 1;

		for (int i = 0; i < maxR; i++)
		{

			for (int y = 0; y < ridg.rows; y++)
			{
				for (int x = 0; x < ridg.cols; x++)
				{
					if (ridg.at<uchar>(y, x) == 255)
					{
						continue;
					}

					bool nextToRidge = false;
					bool nextToEdge = false;

					for (int yO = -kern; yO <= kern; yO++)
					{
						for (int xO = -kern; xO <= kern; xO++)
						{
							if (ImgProc::inBoundsWithVal(ridg, vec2<int>(x + xO, y + yO), 255))
							{
								nextToRidge = true;
							}

							if (ImgProc::inBoundsWithVal(edges, vec2<int>(x + xO, y + yO), 255))
							{
								nextToEdge = true;
							}
						}
					}

					if (nextToRidge && !nextToEdge)
					{
						nextRidg.at<uchar>(y, x) = 255;
					}

				}
			}

			ridg = nextRidg.clone();

		}

		result = ridg;
	}
};