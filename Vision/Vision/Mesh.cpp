#pragma once

#include <stack>
#include <map>
#include <iostream>
#include <fstream>
#include "Utility.cpp";
#include "ImgProc.cpp";
#include "ImgHeightMap.cpp";
#include "Mesh.h";
#include "GuoHall.cpp";
using namespace std;
using namespace cv;


Mesh::Mesh(ImgHeightMap pixelMap, Mat img)
{
	this->pixelMap = pixelMap;
	image = img;
	generatePointsFromMap2();
//	generateBottomPoints2();

//	generateBotPntsPaper();

	triangulate();
//	findFaces();
//	boxMesh();
}

vector<vector<vector<double>>> Mesh::getVoxelGrid2()
{
	auto grid = vector<vector<vector<double>>>();
	int ySize = image.rows + 1;
	int xSize = image.cols + 1;
	int zSize = maxHeight + 1;

	for (int y = 0; y < ySize; y++)
	{
		auto row = vector<vector<double>>();

		for (int x = 0; x < xSize; x++)
		{
			auto col = vector<double>();
			for (int z = 0; z < zSize; z++)
			{
				col.push_back(0);
			}

			row.push_back(col);
		}

		grid.push_back(row);
	}


	for (int i = 0; i < image.rows; i++) // sets 1 for 8 cube vertices if the pixel is white
	{
		for (int n = 0; n < image.cols; n++)
		{
			if (image.at<uchar>(i, n) == 255)
			{
				int height = points[make_pair(n, i)][1];
				for (int z = 0; z < height; z++)
				{
					grid[i][n][z] = 1;
					grid[i][n + 1][z] = 1;
					grid[i + 1][n][z] = 1;
					grid[i + 1][n + 1][z] = 1;

					grid[i][n][z + 1] = 1;
					grid[i][n + 1][z + 1] = 1;
					grid[i + 1][n][z + 1] = 1;
					grid[i + 1][n + 1][z + 1] = 1;
				}
			}
		}
	}


	for (int i = 0; i < image.rows; i++)
	{
		for (int n = 0; n < image.cols; n++)
		{
			if (image.at<uchar>(i, n) == 255)
			{
				continue;
			}

			int kern = 1;
			bool cont = true;
			for (int yOfs = -kern; yOfs <= kern; yOfs++)
			{
				for (int xOfs = -kern; xOfs <= kern; xOfs++)
				{
					if (!outOfBounds(make_pair(n + xOfs, i + yOfs)) && image.at<uchar>(i + yOfs, n + xOfs) == 255)
					{
						cont = false;
						break;
					}
				}
			}

			if (cont)
			{
				continue;
			}

			for (int y = 0; y < 2; y++)
			{
				for (int x = 0; x < 2; x++)
				{
					int posX = x + n;
					int posY = y + i;

					auto pixs = vector<pair<pair<int, int>, double>>();
					pixs.push_back(make_pair(make_pair(posX, posY), 0));
					pixs.push_back(make_pair(make_pair(posX - 1, posY), 0));
					pixs.push_back(make_pair(make_pair(posX, posY - 1), 0));
					pixs.push_back(make_pair(make_pair(posX - 1, posY - 1), 0));

					for (int j = 0; j < pixs.size(); j++)
					{
						if (!outOfBounds(pixs[j].first))
						{
							if (image.at<uchar>(pixs[j].first.second, pixs[j].first.first) == 255)
							{
								pixs[j].second = 1;
							}
						}
					}

					// x2-x/x2-x1 
					double fxy1 = 0.5 * pixs[1].second + 0.5 * pixs[0].second;
					double fxy2 = 0.5 * pixs[3].second + 0.5 * pixs[2].second;

					double val = 0.5 * fxy1 + 0.5 * fxy2;

				//	grid[posY][posX]
				}
			}


		}

	}


	return grid;
}


vector<vector<vector<double>>> Mesh::getVoxelGrid()
{
	auto grid = vector<vector<vector<double>>>();
	int kern = 1;

	for (int y = 0; y < image.rows; y++)
	{
		auto xVec = vector<vector<double>>();
		for (int x = 0; x < image.cols; x++)
		{
			auto zVec = vector<double>();
			if (image.at<uchar>(y, x) != 0)
			{
				int h = points[make_pair(x, y)][1];

				for (int i = 1; i < h; i++)
				{
					zVec.push_back(0);
				}

				zVec.push_back(1);

				for (int n = 0; n < maxHeight - h; n++)
				{
					zVec.push_back(0);
				}
			}
			else // attempt at smoothing on edges
			{
				/*
				for (int n = 0; n < maxHeight; n++)
				{
					zVec.push_back(0);
				}
				*/

				/* t2
				auto top = make_pair(x, y - 1);
				auto bot = make_pair(x, y + 1);
				auto left = make_pair(x - 1, y);
				auto right = make_pair(x + 1, y);
				double val = 0;
				double h = 0;
				double cons = 0.5;

				if (ImgProc::inBoundsWithVal(image, top, 255) && ImgProc::inBoundsWithVal(image, left, 255))
				{
					h = min(points[top][1], points[left][1]);
					val = cons;
				}
				else if (ImgProc::inBoundsWithVal(image, top, 255) && ImgProc::inBoundsWithVal(image, right, 255))
				{
					h = min(points[top][1], points[right][1]);
					val = cons;
				}
				else if (ImgProc::inBoundsWithVal(image, bot, 255) && ImgProc::inBoundsWithVal(image, left, 255))
				{
					h = min(points[bot][1], points[left][1]);
					val = cons;
				}
				else if (ImgProc::inBoundsWithVal(image, bot, 255) && ImgProc::inBoundsWithVal(image, right, 255))
				{
					h = min(points[bot][1], points[right][1]);
					val = cons;
				}

				if (val > 0)
				{
					for (int i = 1; i < h; i++)
					{
						zVec.push_back(0);
					}
					zVec.push_back(val);
				}
				*/



				/* t1
				double avgH = INFINITY;
				int numN = 0;
				for (int yO = -kern; yO <= kern; yO++)
				{
					for (int xO = -kern; xO <= kern; xO++)
					{
						auto pnt = make_pair(xO + x, yO + y);
						if (ImgProc::inBoundsWithVal(image, pnt, 255))
						{
							if (avgH > points[pnt][1])
							{
								avgH = points[pnt][1];
							}

							numN++;
						}
					}
				}

				if (numN > 0)
				{
				//	avgH /= numN;
				//	avgH = floor(avgH);
				}

				if (numN > 0)
				{
					for (int i = 1; i < avgH; i++)
					{
						zVec.push_back(0);
					}
					zVec.push_back(numN * 0.01);
				}
				*/

			}
			

			xVec.push_back(zVec);
		}
		grid.push_back(xVec);
	}

	// test 
	/*
	for (int y = 0; y < grid.size(); y++)
	{
		for (int x = 0; x < grid[y].size(); x++)
		{
			for (int z = 0; z < grid[y][x].size(); z++)
			{
				if (grid[y][x][z] == 1)
				{
					continue;
				}

				if (ImgProc::inBoundsOfVoxelGridWithVal(x - 1, y, z, 1, grid) &&
					ImgProc::inBoundsOfVoxelGridWithVal(x, y, z - 1, 1, grid) || 
					ImgProc::inBoundsOfVoxelGridWithVal(x + 1, y, z, 1, grid) &&
					ImgProc::inBoundsOfVoxelGridWithVal(x, y, z - 1, 1, grid) ||
					
					ImgProc::inBoundsOfVoxelGridWithVal(x, y - 1, z, 1, grid) &&
					ImgProc::inBoundsOfVoxelGridWithVal(x, y, z - 1, 1, grid) ||
					ImgProc::inBoundsOfVoxelGridWithVal(x, y + 1, z, 1, grid) &&
					ImgProc::inBoundsOfVoxelGridWithVal(x, y, z - 1, 1, grid))
				{
				//	cout << "did something " << endl;
					grid[y][x][z] = 0.0001;
				}
			}
		}
	}
	*/

	return grid;
}

void Mesh::boxMesh()
{
	/*
		v 0 -1 0
		v 1 -1 0
		v 1 -1 1
		v 0 -1 1
		v 0 0 0
		v 1 0 0
		v 1 0 1
		v 0 0 1

		f 1 2 3 4
		f 5 6 7 8
		f 1 2 6 5
		f 2 3 7 6
		f 3 4 8 7
		f 4 1 5 8

	*/
	fstream f;

	f.open("C:/Users/Matiss/Desktop/cubes.obj", ios::out);
	int pos = 1;
	auto quads = vector<vector<int>>();

	for (auto &pnts : points)
	{
		int x = pnts.first.first;
		int y = pnts.first.second;

		auto data = pnts.second;

		double botVal = data[1];
		double topVal = botVal + 1;

		f << "v " << data[0] - 0.5 << " " << botVal << " " << data[2] - 0.5 << endl;
		f << "v " << data[0] + 0.5 << " " << botVal << " " << data[2] - 0.5 << endl;
		f << "v " << data[0] + 0.5 << " " << botVal << " " << data[2] + 0.5 << endl;
		f << "v " << data[0] - 0.5 << " " << botVal << " " << data[2] + 0.5 << endl;

		f << "v " << data[0] - 0.5 << " " << topVal << " " << data[2] - 0.5 << endl;
		f << "v " << data[0] + 0.5 << " " << topVal << " " << data[2] - 0.5 << endl;
		f << "v " << data[0] + 0.5 << " " << topVal << " " << data[2] + 0.5 << endl;
		f << "v " << data[0] - 0.5 << " " << topVal << " " << data[2] + 0.5 << endl;


		quads.push_back({pos, pos + 1, pos + 2, pos + 3});
		quads.push_back({pos + 4, pos + 5, pos + 6, pos + 7});
		quads.push_back({pos, pos + 1, pos + 5, pos + 4});
		quads.push_back({pos + 1, pos + 2, pos + 6, pos + 5});
		
		quads.push_back({pos + 2, pos + 3, pos + 7, pos + 6});
		quads.push_back({pos + 3, pos, pos + 4, pos + 7});

		pos += 8;
	}


	for (int i = 0; i < quads.size(); i++)
	{
		f << "f " << quads[i][0] << " " << quads[i][1] << " " << quads[i][2] << " " << quads[i][3] << endl;
	}
}

double Mesh::roundTo2Places(double num)
{
	return floor(num * 100 + 0.5) / 100;
}

bool Mesh::outOfBounds(pair<int, int> pnt)
{
	if (pnt.first < 0 || pnt.second < 0 || pnt.first >= image.cols || pnt.second >= image.rows)
	{
		return true;
	}

	return false;
}

void Mesh::findFaces2()
{
	auto levels = map<double, vector<pair<int, int>>>();

	for (int i = 0; i < pixelMap.map.size(); i++)
	{
		for (int n = 0; n < pixelMap.map[i].size(); n++)
		{
			if (pixelMap.map[i][n].height < 0)
			{
				continue;
			}

			PixelHeightMap pix = pixelMap.map[i][n];
			double key = roundTo2Places(pix.height);

			if (levels.find(key) == levels.end())
			{
				auto cont = vector<pair<int, int>>();
				cont.push_back(make_pair(pix.x, pix.y));

				levels.insert(make_pair(key, cont));
			}
			else
			{
				levels[key].push_back(make_pair(pix.x, pix.y));
			}
		}
	}

	for (auto const& x : levels)
	{
		double key = x.first;
		vector<pair<int, int>> pixels = x.second;

		Mat img(image.size(), CV_8UC1, Scalar(0));
		for (int i = 0; i < pixels.size(); i++)
		{
			img.at<uchar>(pixels[i].second, pixels[i].first) = (uchar)255;
		}

		Utility::writeImage(img, to_string(key) + "im.png");
	}
}

void Mesh::findFaces()
{
	faces = vector<vector<pair<int, int>>>();
	Mat visited(image.size(), CV_8UC1, Scalar(0));
	stack<pair<int, int>> st = stack<pair<int, int>>();
	int maxDistance = 10;
	pair<int, int> originPoint;

	for (int i = 0; i < image.rows; i++)
	{
		for (int n = 0; n < image.cols; n++)
		{
			if (image.at<uchar>(i, n) == 0 || visited.at<uchar>(i, n) != 0)
			{
				continue;
			}

			st.push(make_pair(n, i));
			visited.at<uchar>(i, n) = (uchar)255;
			originPoint = make_pair(n, i);

			vector<pair<int, int>> face = vector<pair<int, int>>();

			while (!st.empty())
			{
				pair<int, int> pos = st.top();
				st.pop();

				face.push_back(pos);

				for (int j = -1; j < 2; j++)
				{
					for (int k = -1; k < 2; k++)
					{
						if ((j + pos.second) < 0 || (j + pos.second) >= image.rows || // skip if outside bounds or the neighbour is empty
							(k + pos.first) < 0 || (k + pos.first) >= image.cols ||
							visited.at<uchar>(j + pos.second, k + pos.first) == 255 ||
							image.at<uchar>(j + pos.second, k + pos.first) == 0)
						{
							continue;
						}

						if (j == 0 && k == 0) // this is current pixel
						{
							continue;
						}

						if (abs((pos.first + k) - originPoint.first) > maxDistance ||
							abs((pos.second + j) - originPoint.second) > maxDistance)
						{
							continue;
						}

						st.push(make_pair(pos.first + k, pos.second + j));
						visited.at<uchar>(pos.second + j, pos.first + k) = (uchar)255;
					}
				}

			}

			faces.push_back(face);
		}

	}

}

void Mesh::triangulate()
{
	triangles = vector<vector<pair<int, int>>>();

	Mat border = ImgProc::getBinaryBorder(image);



	for (int i = 0; i < image.rows - 1; i++)
	{
		for (int n = 0; n < image.cols - 1; n++)
		{
			if (image.at<uchar>(i, n) == 0)
			{
				continue;
			}

			pair<int, int> currPoint = make_pair(n, i);


			// -->
			// |
			if (image.at<uchar>(i, n + 1) != 0 && image.at<uchar>(i + 1, n) != 0)
			{

					triangles.push_back(vector<pair<int, int>>({ currPoint, make_pair(n + 1, i), make_pair(n, i + 1) }));

			}


			// -->
			// \ 
			if (image.at<uchar>(i, n + 1) != 0 && image.at<uchar>(i + 1, n + 1) != 0)
			{

					triangles.push_back(vector<pair<int, int>>({ currPoint, make_pair(n + 1, i), make_pair(n + 1, i + 1) }));

			}


			// |\ 
			if (image.at<uchar>(i + 1, n) != 0 && image.at<uchar>(i + 1, n + 1) != 0)
			{

					triangles.push_back(vector<pair<int, int>>({ currPoint, make_pair(n, i + 1), make_pair(n + 1, i + 1) }));

			}
		}
	}
	

	/*

	auto checkedPixels = map<pair<int, int>, bool>();
	smoothPoints = vector<pair<pair<int, int>, vector<double>>>();
	borderTris = vector<vector<pair<int, int>>>();

	//Utility::writeImage(border, "C:/Users/Matiss/Desktop/testborder.png");
	for (int i = 0; i < border.rows; i++)
	{
		for (int n = 0; n < border.cols; n++)
		{
			if (border.at<uchar>(i, n) == 0)
			{
				continue;
			}
			auto currPnt = make_pair(n, i);
			auto topLeft = make_pair(n - 1, i - 1);
			auto topRight = make_pair(n + 1, i - 1);
			auto botLeft = make_pair(n - 1, i + 1);
			auto botRight = make_pair(n + 1, i + 1);

			if (!outOfBounds(topLeft) && border.at<uchar>(topLeft.second, topLeft.first) != 0)
			{
				auto side1 = make_pair(topLeft.first + 1, topLeft.second);
				auto side2 = make_pair(topLeft.first, topLeft.second + 1);
				double avgHeight = double(points[topLeft][1] + points[currPnt][1]) / 2;
//				double avgHeight = 0.2;

				if (!outOfBounds(side1) && checkedPixels.find(side1) == checkedPixels.end() && image.at<uchar>(side1.second, side1.first) == 0)
				{
					auto pointCoord = vector<double>({ side1.first - 0.5, avgHeight, side1.second - 0.5 });
					auto pointDta = make_pair(side1, pointCoord);
					auto triangle = vector<pair<int, int>>({currPnt, topLeft, side1});
					borderTris.push_back(triangle);
					smoothPoints.push_back(pointDta);
					checkedPixels.insert(make_pair(side1, true));
				}

				if (!outOfBounds(side2) && checkedPixels.find(side2) == checkedPixels.end() && image.at<uchar>(side2.second, side2.first) == 0)
				{
					auto pointCoord = vector<double>({ side2.first - 0.5, avgHeight, side2.second - 0.5 });
					auto pointDta = make_pair(side2, pointCoord);
					auto triangle = vector<pair<int, int>>({ currPnt, topLeft, side2 });
					borderTris.push_back(triangle);
					smoothPoints.push_back(pointDta);
					checkedPixels.insert(make_pair(side2, true));
				}

			}
			
			if (!outOfBounds(topRight) && border.at<uchar>(topRight.second, topRight.first) != 0)
			{
				auto side1 = make_pair(topRight.first - 1, topRight.second);
				auto side2 = make_pair(topRight.first, topRight.second + 1);
				double avgHeight = double(points[topRight][1] + points[currPnt][1]) / 2;

				if (!outOfBounds(side1) && checkedPixels.find(side1) == checkedPixels.end() && image.at<uchar>(side1.second, side1.first) == 0)
				{
					auto pointCoord = vector<double>({ side1.first - 0.5, avgHeight, side1.second - 0.5 });
					auto pointDta = make_pair(side1, pointCoord);
					auto triangle = vector<pair<int, int>>({ currPnt, topRight, side1 });
					borderTris.push_back(triangle);
					smoothPoints.push_back(pointDta);
					checkedPixels.insert(make_pair(side1, true));
				}

				if (!outOfBounds(side2) && checkedPixels.find(side2) == checkedPixels.end() && image.at<uchar>(side2.second, side2.first) == 0)
				{
					auto pointCoord = vector<double>({ side2.first - 0.5, avgHeight, side2.second - 0.5 });
					auto pointDta = make_pair(side2, pointCoord);
					auto triangle = vector<pair<int, int>>({ currPnt, topRight, side2 });
					borderTris.push_back(triangle);
					smoothPoints.push_back(pointDta);
					checkedPixels.insert(make_pair(side2, true));
				}
			}

			if (!outOfBounds(botLeft) && border.at<uchar>(botLeft.second, botLeft.first) != 0)
			{
				auto side1 = make_pair(botLeft.first, botLeft.second - 1);
				auto side2 = make_pair(botLeft.first + 1, botLeft.second);
				double avgHeight = double(points[botLeft][1] + points[currPnt][1]) / 2;

				if (!outOfBounds(side1) && checkedPixels.find(side1) == checkedPixels.end() && image.at<uchar>(side1.second, side1.first) == 0)
				{
					auto pointCoord = vector<double>({ side1.first - 0.5, avgHeight, side1.second - 0.5 });
					auto pointDta = make_pair(side1, pointCoord);
					auto triangle = vector<pair<int, int>>({ currPnt, botLeft, side1 });
					borderTris.push_back(triangle);
					smoothPoints.push_back(pointDta);
					checkedPixels.insert(make_pair(side1, true));
				}

				if (!outOfBounds(side2) && checkedPixels.find(side2) == checkedPixels.end() && image.at<uchar>(side2.second, side2.first) == 0)
				{
					auto pointCoord = vector<double>({ side2.first - 0.5, avgHeight, side2.second - 0.5 });
					auto pointDta = make_pair(side2, pointCoord);
					auto triangle = vector<pair<int, int>>({ currPnt, botLeft, side2 });
					borderTris.push_back(triangle);
					smoothPoints.push_back(pointDta);
					checkedPixels.insert(make_pair(side2, true));
				}
			}

			if (!outOfBounds(botRight) && border.at<uchar>(botRight.second, botRight.first) != 0)
			{
				auto side1 = make_pair(botRight.first - 1, botRight.second);
				auto side2 = make_pair(botRight.first, botRight.second - 1);
				double avgHeight = double(points[botRight][1] + points[currPnt][1]) / 2;

				if (!outOfBounds(side1) && checkedPixels.find(side1) == checkedPixels.end() && image.at<uchar>(side1.second, side1.first) == 0)
				{
					auto pointCoord = vector<double>({ side1.first - 0.5, avgHeight, side1.second - 0.5 });
					auto pointDta = make_pair(side1, pointCoord);
					auto triangle = vector<pair<int, int>>({ currPnt, botRight, side1 });
					borderTris.push_back(triangle);
					smoothPoints.push_back(pointDta);
					checkedPixels.insert(make_pair(side1, true));
				}

				if (!outOfBounds(side2) && checkedPixels.find(side2) == checkedPixels.end() && image.at<uchar>(side2.second, side2.first) == 0)
				{
					auto pointCoord = vector<double>({ side2.first - 0.5, avgHeight, side2.second - 0.5 });
					auto pointDta = make_pair(side2, pointCoord);
					auto triangle = vector<pair<int, int>>({ currPnt, botRight, side2 });
					borderTris.push_back(triangle);
					smoothPoints.push_back(pointDta);
					checkedPixels.insert(make_pair(side2, true));
				}
			}
			

		}
	}
	*/
	/*
	for (int i = 0; i < border.rows; i++)
	{
		for (int n = 0; n < border.cols; n++)
		{
			if (border.at<uchar>(i, n) )
		}
	}
	*/


}

vector<pair<int, int>> Mesh::pixelBorderParents(Mat &border, pair<int, int> pnt)
{
	int kern = 1;
	auto res = vector<pair<int, int>>();

	for (int y = -kern; y <= kern; y++)
	{
		for (int x = -kern; x <= kern; x++)
		{
			auto curPnt = make_pair(x + pnt.first, y + pnt.second);
			if (ImgProc::inBoundsWithVal(border, curPnt, 255) &&
				curPnt != pnt)
			{
				res.push_back(curPnt);
			}
			
		}
	}

	return res;
}

vector<pair<int, int>> Mesh::pixelNeighbours(Mat &border, pair<int, int> pnt)
{
	int kern = 1;
	auto res = vector<pair<int, int>>();

	for (int y = -kern; y <= kern; y++)
	{
		for (int x = -kern; x <= kern; x++)
		{
			auto curPnt = make_pair(x + pnt.first, y + pnt.second);
			if (ImgProc::inBoundsWithVal(image, curPnt, 255) &&
				curPnt != pnt &&
				border.at<uchar>(curPnt.second, curPnt.first) != 255)
			{
				res.push_back(curPnt);
			}

		}
	}

	return res;
}

vector<pair<int, int>> Mesh::pixelNeighboursWithNoBordPnts(Mat &border, pair<int, int> pnt)
{
	auto res = vector<pair<int, int>>();
	auto neighbs = pixelNeighbours(border, pnt);

	for (int i = 0; i < neighbs.size(); i++)
	{
		auto neighbBord = pixelBorderParents(border, neighbs[i]);
		if (neighbBord.size() == 0)
		{
			res.push_back(neighbs[i]);
		}
	}

	return res;
}

void Mesh::setBorderPixelDists(vector<vector<double>> &dists, Mat &border, pair<int, int> pnt)
{
	auto st = stack<pair<int, int>>();
	
	if (points.find(pnt) == points.end()) // this is caused by the border since it can create new pixels that arent in the original image
	{
		int kern = 1;
		double avg = 0;
		int num = 0;
		
		for (int y = -kern; y <= kern; y++)
		{
			for (int x = -kern; x <= kern; x++)
			{
				auto nb = make_pair(pnt.first + x, pnt.second + y);
				if (ImgProc::inBoundsWithVal(image, nb, 255) &&
					points.find(nb) != points.end())
				{
					num++;
					avg += points[nb][1];
				}
			}
		}

		if (num != 0)
		{
			avg = avg / num;
		}

		points.insert(make_pair(pnt, vector<double>({ (double)pnt.first, avg, (double)pnt.second })));
	}


	double pntHeight = points[pnt][1];
	st.push(pnt);

	while (true)
	{
		auto currPnt = st.top();
		st.pop();

		if (currPnt == pnt)
		{
			auto nonBordNeigh = pixelNeighbours(border, currPnt);
			for (int i = 0; i < nonBordNeigh.size(); i++)
			{
				double d = pntHeight - Utility::pointDist(currPnt, nonBordNeigh[i]);
				if (dists[nonBordNeigh[i].second][nonBordNeigh[i].first] > d)
				{
					dists[nonBordNeigh[i].second][nonBordNeigh[i].first] = d;
				}

				auto neighbs = pixelBorderParents(border, nonBordNeigh[i]);
				if (neighbs.size() <= 1)
				{
					st.push(nonBordNeigh[i]);
				}
			}
		}
		else
		{
			auto neighbsWithNoBord = pixelNeighboursWithNoBordPnts(border, currPnt);
			for (int i = 0; i < neighbsWithNoBord.size(); i++)
			{
				double neighbD = dists[neighbsWithNoBord[i].second][neighbsWithNoBord[i].first];
				double d = pntHeight - Utility::pointDist(currPnt, neighbsWithNoBord[i]);
				if (neighbD > d)
				{
					dists[neighbsWithNoBord[i].second][neighbsWithNoBord[i].first] = d;
					st.push(neighbsWithNoBord[i]);
				}
			}
		}

		if (st.empty())
		{
			break;
		}
	}
}

void Mesh::generateBotPntsPaper()
{
	this->bottomPoints = map<pair<int, int>, vector<double>>();
	Mat border = ImgProc::getBinaryBorder(image);
	Mat skel = image.clone();
	Thinning::GuoHall::thinn(skel);
	int kern = 1;

	auto st1 = stack<vec2<int>>();
	auto st2 = stack<vec2<int>>();


	auto hghts = vector<vector<int>>(border.rows, vector<int>());
	for (int y = 0; y < hghts.size(); y++)
	{
		for (int x = 0; x < border.cols; x++)
		{
			hghts[y].push_back(0);
		}
	}


	Mat hasVal(border.size(), CV_8UC1, Scalar(0));

	for (int y = 0; y < border.rows; y++)
	{
		for (int x = 0; x < border.rows; x++)
		{
			if (border.at<uchar>(y, x) == 255 && points.find(make_pair(x, y)) != points.end() &&
				skel.at<uchar>(y, x) == 0)
			{
				st1.push(vec2<int>(x, y));
				hghts[y][x] = points[make_pair(x, y)][1];
				hasVal.at<uchar>(y, x) = 1;
			}
		}
	}


	while (!st1.empty())
	{
		vec2<int> el = st1.top();
		st1.pop();

		while (!st1.empty())
		{
			for (int yO = -kern; yO <= kern; yO++)
			{
				for (int xO = -kern; xO <= kern; xO++)
				{
					vec2<int> neib = vec2<int>(el.x + xO, el.y + yO);
					
					if (neib == el)
					{
						continue;
					}

					if (ImgProc::inBoundsWithVal(image, neib, 255) &&
						hasVal.at<uchar>(neib.y, neib.x) == 0 && skel.at<uchar>(neib.y, neib.x) == 0
						)
					{
						st2.push(neib);
						hghts[neib.y][neib.x] = hghts[el.y][el.x] - 1;
						hasVal.at<uchar>(neib.y, neib.x) = 1;
					}
				}
			}

			el = st1.top();
			st1.pop();
		}

		if (!st2.empty())
		{
			el = st2.top();
			st2.pop();
		}

		while (!st2.empty())
		{
			for (int yO = -kern; yO <= kern; yO++)
			{
				for (int xO = -kern; xO <= kern; xO++)
				{
					vec2<int> neib = vec2<int>(el.x + xO, el.y + yO);

					if (neib == el)
					{
						continue;
					}

					if (ImgProc::inBoundsWithVal(image, neib, 255) &&
						hasVal.at<uchar>(neib.y, neib.x) == 0 && skel.at<uchar>(neib.y, neib.x) == 0
						)
					{
						st1.push(neib);
						hghts[neib.y][neib.x] = hghts[el.y][el.x] - 1;
						hasVal.at<uchar>(neib.y, neib.x) = 1;
					}
				}
			}

			el = st2.top();
			st2.pop();
		}
	}



	for (int y = 0; y < image.rows; y++)
	{
		for (int x = 0; x < image.cols; x++)
		{
			if (image.at<uchar>(y, x) == 0 || border.at<uchar>(y, x) == 255)
			{
				continue;
			}

			auto dta = vector<double>({ (double)x, (double)hghts[y][x], (double)y });
			//auto dta = vector<double>({(double)n, maxHeight - points[key][1], (double)i });
			bottomPoints.insert(make_pair(make_pair(x, y), dta));
		}
	}

}

void Mesh::generateBottomPoints2()
{
	this->bottomPoints = map<pair<int, int>, vector<double>>();
	Mat border = ImgProc::getBinaryBorder(image);
	int curKern = 1;
	int maxKern = 10;


	for (int i = 0; i < border.rows; i++)
	{
		for (int n = 0; n < border.cols; n++)
		{
			if (image.at<uchar>(i, n) == 0 || border.at<uchar>(i, n) == 255)
			{
				continue;
			}

			auto key = make_pair(n, i);

			if (points.find(key) == points.end())
			{
				continue;
			}

			double avg = 0;
			bool found = false;

			while (true)
			{ 

				for (int y = -curKern; y <= curKern; y++)
				{
					for (int x = -curKern; x <= curKern; x++)
					{
						auto pos = make_pair(x + n, y + i);
						if (ImgProc::inBoundsWithVal(border, pos, 255) &&
							points.find(pos) != points.end())
						{
							avg = points[pos][1] - curKern;
							found = true;
							break;
						}
					}

					if (found)
						break;
				}

				curKern++;

				if (found)
				{ 
					curKern = 1;
					break;
				}

			}



			auto dta = vector<double>({ (double)n,avg, (double)i });
			//auto dta = vector<double>({(double)n, maxHeight - points[key][1], (double)i });
			bottomPoints.insert(make_pair(key, dta));
		}
	}
}

void Mesh::generateBottomPoints()
{
	this->bottomPoints = map<pair<int, int>, vector<double>>();
	Mat border = ImgProc::getBinaryBorder(image);
	
	auto dists = vector<vector<double>>();
	for (int i = 0; i < image.rows; i++)
	{
		auto dRow = vector<double>();
		for (int n = 0; n < image.cols; n++)
		{
			dRow.push_back(INFINITY);
		}
		dists.push_back(dRow);
	}


	for (int y = 0; y < border.rows; y++)
	{
		for (int x = 0; x < border.cols; x++)
		{
			if (border.at<uchar>(y, x) == 255)
			{
				setBorderPixelDists(dists, border, make_pair(x, y));
			}

		}
	}


	for (int y = 0; y < border.rows; y++)
	{
		for (int x = 0; x < border.cols; x++)
		{
			if (border.at<uchar>(y, x) == 255 || image.at<uchar>(y, x) == 0 || dists[y][x] == INFINITY)
			{
				continue;
			}

			auto pnt = make_pair(x, y);
			auto pntDta = vector<double>({(double)x, dists[y][x], (double)y});
			bottomPoints.insert(make_pair(pnt, pntDta));
		}
	}

}

void Mesh::generatePointsFromSimpMap(vector<vector<int>> simpMap)
{
	this->points = map<pair<int, int>, vector<double>>();

	double lowestPnt = INFINITY;

	for (int i = 0; i < simpMap.size(); i++)
	{
		for (int n = 0; n < simpMap[i].size(); n++)
		{

			if (simpMap[i][n] == -1)
			{
				continue;
			}

			double x = n;
			double y;
			double z = i;

			if (simpMap[i][n] > 0)
			{
				y = -simpMap[i][n];
			}
			else
			{
				y = 0;
			}

			if (y < lowestPnt)
			{
				lowestPnt = y;
			}


			points[make_pair(n, i)] = vector<double>({ x, y, z });

		}

	}


	lowestPnt = abs(lowestPnt);
	this->lowestPnt = lowestPnt;

	for (auto &pnts : points)
	{
		points[pnts.first][1] += lowestPnt;
	}

	maxHeight = lowestPnt; // since center is 0 and everything else is smaller
}

void Mesh::generatePointsFromMap2()
{
	this->points = map<pair<int, int>, vector<double>>();

	double lowestPnt = INFINITY;

	for (int i = 0; i < pixelMap.map.size(); i++)
	{
		for (int n = 0; n < pixelMap.map[i].size(); n++)
		{
			PixelHeightMap element = pixelMap.map[i][n];

			if (element.height < 0)
			{
				continue;
			}

			double x = n;
			double y;
			double z = i;

			if (element.height > 0)
			{
				y = -element.height;
			}
			else
			{
				y = 0;
			}

			if (y < lowestPnt)
			{
				lowestPnt = y;
			}

			points[make_pair(n, i)] = vector<double>({ x, y, z });
		}
	}

	lowestPnt = abs(lowestPnt);
	this->lowestPnt = lowestPnt;

	for (auto &pnts : points)
	{
		points[pnts.first][1] += lowestPnt;
	}

	maxHeight = lowestPnt; // since center is 0 and everything else is smaller

	/*
	// added some border pixels
	Mat border = ImgProc::getBinaryBorder(image);
	for (int i = 0; i < border.rows; i++)
	{
		for(int n = 0; n < border.cols; n++)
		{
			if (border.at<uchar>(i, n) == 0 || image.at<uchar>(i, n) == border.at<uchar>(i, n))
			{
				continue;
			}

			int kern = 1;
			double avgH = 0;
			int numNeib = 0;

			for (int yOfs = -kern; yOfs <= kern; yOfs++)
			{
				for (int xOfs = -kern; xOfs <= kern; xOfs++)
				{
					int x = n + xOfs;
					int y = i + yOfs;
					if (!outOfBounds(make_pair(x, y)) && image.at<uchar>(y, x) == border.at<uchar>(y, x) && border.at<uchar>(y, x) == 255)
					{
						numNeib++;
						avgH += points[make_pair(x, y)][1];
					}
				}
			}

			avgH /= numNeib;
			points[make_pair(n, i)] = vector<double>({ (double)n, avgH, (double)i });
		}
	}

	for (int i = 0; i < border.rows; i++)
	{
		for (int n = 0; n < border.cols; n++)
		{
			if (border.at<uchar>(i, n) == 255 && image.at<uchar>(i, n) == 0)
			{
				image.at<uchar>(i, n) = (uchar)255;
			}
		}
	}
	*/
}

void Mesh::generatePointsFromMap()
{
	this->points = map<pair<int, int>, vector<double>>();

	for (int i = 0; i < pixelMap.map.size(); i++)
	{
		for (int n = 0; n < pixelMap.map[i].size(); n++)
		{
			PixelHeightMap element = pixelMap.map[i][n];

			if (element.height < 0)
			{
				continue;
			}

			double x = n;
			double y;
			double z = i;


			if (element.skeletParent.size() == 0)
			{
				y = element.skelMaxDist + 1;
			}
			else
			{
				double parentMaxDist;
				double maxY = INFINITY;

				for (int j = 0; j < element.skeletParent.size(); j++)
				{
					parentMaxDist = element.skeletParent[j]->skelMaxDist + 1;
					y = parentMaxDist - element.height;
					if (y < maxY)
					{
						maxY = y;
					}
					/*
					if (y >= 1)
					{
						break;
					}
					*/
				}

				y = maxY; 
			}

			points[make_pair(n, i)] = vector<double>({ x, y, z });
			//points.push_back(vector<double>({x, y, z}));
		//	points.push_back(vector<double>({ x, -y, z }));
		}
	}
}

void Mesh::exportFrontBottomPointsToWavefront(string objectName, string filePath)
{
	fstream f;
	f.open(filePath + "/" + objectName + ".obj", ios::out);
	f << "# OBJ file containing leaf veins in 3D" << endl;

	for (auto &el : points)
	{
		f << "v " << el.second[0] << " " << el.second[1] << " " << el.second[2] << endl;
	}
	double maxPntDiff = -INFINITY;
	for (auto &el : bottomPoints)
	{
		if (points.find(el.first) != points.end() && points[el.first][1] != el.second[1])
		{
			
			double pntDif = abs(points[el.first][1] - el.second[1]);
			if (pntDif > maxPntDiff)
			{
				maxPntDiff = pntDif;
			}

			if (pntDif < 1)
			{
				//cout << "small pnt diff " << pntDif << endl;
			}
			else
			{
				f << "v " << el.second[0] << " " << el.second[1] << " " << el.second[2] << endl;
			}
		}
		else
		{
		//	cout << "duplicate found" << endl;
		}
	}
	cout << "max diff " << maxPntDiff << endl;
	int w;
	cin >> w;
	f.close();
}

void Mesh::exportBottomPointsToWavefront(string objectName, string filePath)
{
	fstream f;
	f.open(filePath + "/" + objectName + ".obj", ios::out);
	f << "# OBJ file containing leaf veins in 3D" << endl;

	for (auto &el : bottomPoints)
	{
		f << "v " << el.second[0] << " " << el.second[1] << " " << el.second[2] << endl;
	}

	f.close();
}

void Mesh::exportPointsToWavefront(string objectName, string filePath)
{
	fstream f;
	int indx = 1;
	map<pair<int, int>, int> pointOrder = map<pair<int, int>, int>();
	auto botPntOrder = map<pair<int, int>, int>();

	f.open(filePath + "/" + objectName + ".obj", ios::out);
	f << "# OBJ file containing leaf veins in 3D" << endl;

	for (auto &el : points)
	{
		if (image.at<uchar>(el.first.second, el.first.first) == 0)
		{
			continue;
		}

		f << "v " << el.second[0] << " " << el.second[1] << " " << el.second[2] << endl;
//		f << "v " << el.second[0] << " " << el.second[1] - 1 << " " << el.second[2] << endl;
		pointOrder[el.first] = indx;
		indx++;
	//	indx += 2;
	}

	/*
	for (auto &el : bottomPoints)
	{
		f << "v " << el.second[0] << " " << el.second[1] << " " << el.second[2] << endl;
		botPntOrder[el.first] = indx;
		indx++;
	}
	*/
	/*
	for (auto &face : faces)
	{
		f << "f ";

		for (int i = 0; i < face.size(); i++)
		{
			f << pointOrder[face[i]];
			
			if (i + 1 == face.size())
			{
				f << endl;
			}
			else
			{
				f << " ";
			}
		}
	}
	*/


	/*

	// border smooth
	map<pair<int, int>, int> borderPntOrd = map<pair<int, int>, int>();
	for (auto &pnt : smoothPoints)
	{
		auto dta = pnt.second;
		borderPntOrd[pnt.first] = indx;
		indx++;

		f << "v " << dta[0] << " " << dta[1] << " " << dta[2] << endl;
		
	}

	for (auto &el : borderTris)
	{
		f << "f " << pointOrder[el[0]] << " " << pointOrder[el[1]] << " " << borderPntOrd[el[2]] << endl;
	}
	*/
	//




	for (auto &el : triangles)
	{
		int tri1 = pointOrder[el[0]];
		int tri2 = pointOrder[el[1]];
		int tri3 = pointOrder[el[2]];

		int tri4;
		int tri5;
		int tri6;

		if (botPntOrder.find(el[0]) == botPntOrder.end())
		{
			tri4 = tri1;
		}
		else
		{
			tri4 = botPntOrder[el[0]];
		}

		if (botPntOrder.find(el[1]) == botPntOrder.end())
		{
			tri5 = tri2;
		}
		else
		{
			tri5 = botPntOrder[el[1]];
		}


		if (botPntOrder.find(el[2]) == botPntOrder.end())
		{
			tri6 = tri3;
		}
		else
		{
			tri6 = botPntOrder[el[2]];
		}

		double t1Angl1 = Utility::angle3Dvecs(
			Utility::vec3DSub(points[el[0]], points[el[1]]),
			Utility::vec3DSub(points[el[0]], points[el[2]]));

		double t1Angl2 = Utility::angle3Dvecs(
			Utility::vec3DSub(points[el[1]], points[el[0]]),
			Utility::vec3DSub(points[el[1]], points[el[2]]));

		double t1Angl3 = 180 - (t1Angl1 + t1Angl2);

//		double t1Angl1 = (Utility::vec3DSub(points[el[0]],(points[el[1]])
		pair<double, int> anglK[3] = { make_pair(t1Angl1, tri1), make_pair(t1Angl2, tri2), make_pair(t1Angl3, tri3) };

		if (anglK[0].first > anglK[1].first)
			swap(anglK[0], anglK[1]);
		if (anglK[0].first > anglK[2].first)
			swap(anglK[0], anglK[2]);
		if (anglK[1].first > anglK[2].first)
			swap(anglK[1], anglK[2]);


		f << "f " << anglK[2].second << " " << anglK[1].second << " " << anglK[0].second << endl;
	//	f << "f " << tri4 << " " << tri5 << " " << tri6 << endl;
	//	f << "f " << tri1 + 1 << " " << tri2 + 1 << " " << tri3 + 1 << endl;
	}
	


	f.close();
	return;
}