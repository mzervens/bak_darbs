#pragma once;

#include <iostream>
#include <vector>
#include <map>
#include "PixelHeightMap.cpp";

using namespace std;


class ImgHeightMap
{
public:
	vector<vector<PixelHeightMap>> map;
	
	ImgHeightMap()
	{

	}

	ImgHeightMap(int rows, int cols)
	{
		map = vector<vector<PixelHeightMap>>(rows, vector<PixelHeightMap>(cols));
	}

	void updatePixelHeight(int x, int y, int parX, int parY, double height)
	{
		if (y < 0 || y >= map.size() || x < 0 || x >= map[y].size())
		{
			return;
		}

		if (!(x == parX && y == parY)) // skelet pixel, cant be child for other pixels
		{
			map[parY][parX].addChild(&map[y][x], x, y, height);
		}

		map[y][x].updateMinHeight(x, y, height);



		
	}
};

