#pragma once;

#include "Mesh.h";
#include "opencv2/imgcodecs.hpp"
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include "Utility.cpp";
#include "ImgProc.cpp";
#include "Structs.cpp";
#include "Filter.cpp";
#include "ThreeDMap.cpp";
#include "ZhangSuen.cpp";
#include "GuoHall.cpp";
#include "Interpolator.cpp";
#include "MarchingCubes.cpp";
#include "Spline.h";
#include "Mesh2.cpp";
#include "BranchPartition.cpp";
#include "RidgeEdgeDetect.cpp";
#include "RegionGrowing.cpp";
#include "TaubinSmooth.cpp";
#include "DoGDetect.cpp";
#include "VeinHeights.cpp";
#include "REDetect.cpp";
#include <ctime>

using namespace cv;
using namespace std;
 


class Testing
{
public:
	void cannyTest(Mat src, int thresh, string resultPaths[])
	{
		Mat canny_output = Filter::CannyEdge(src, thresh, thresh * 2);
		Utility::writeImage(canny_output, resultPaths[0]);
		Utility::displayImage(canny_output);

		Triple<Mat, vector<Vec4i>, vector<vector<Point>>> conts = ImgProc::curvesFromBin(canny_output, true);

		Utility::displayImage(conts.fst);
	}

	void cannyTrippleChan(Mat src)
	{
		Mat hsv;
		cvtColor(src, hsv, CV_BGR2HSV);

		vector<Mat> channels;
		split(hsv, channels);

		Mat H = channels[0];
		Mat S = channels[1];
		Mat V = channels[2];

		//Utility::displayImage(H);

		Mat Hshift = H.clone();
		int shiftDegs = 155;
		for (int i = 0; i < Hshift.rows; i++)
		{
			for (int n = 0; n < Hshift.cols; n++)
			{
				Hshift.at<uchar>(i, n) = (Hshift.at<uchar>(i, n) + shiftDegs) % 180;
			}
		}


		//Utility::displayImage(Hshift);
		Mat Hinv = ImgProc::doThreshold(H, 20, 255, THRESH_BINARY_INV);
		Utility::displayImage(Hinv);
		Utility::writeImage(Hinv, "C:/Users/Matiss/Desktop/hue_inv.png");
		return;

		Mat Hcanny = Filter::CannyEdge(Hinv, 100, 200, Size(3, 3), true);
		Utility::displayImage(Hcanny);

		//Utility::displayImage(S);
		//Utility::displayImage(V);
	}

};

uchar enlargeI(int minI, int maxI, int i, int t = 3)
{
	int val = (255 * pow(((float)(i - minI) / (maxI - minI)), t));
	return val;
}


Mat testGrey(Mat im)
{
	Mat hsv;
	Mat res(im.size(), CV_8UC1, Scalar(0));
	hsv = ImgProc::GBRToHSI(im);

	vector<Mat> channels;
	split(hsv, channels);

	Mat H = channels[0];
	Mat S = channels[1];
	Mat V = channels[2];

	for (int y = 0; y < res.rows; y++)
	{
		for (int x = 0; x < res.cols; x++)
		{
			double newVal = (double)((double)((H.at<uchar>(y, x) + 90) % 180) / 180 + (1 - V.at<uchar>(y, x))) / 2;
			res.at<uchar>(y, x) = std::min((int)newVal, 255);
		}
	}

	return res;
}

Mat topHat(Mat im, int morph_size = 1)
{
	Mat res;
	Mat element = getStructuringElement(MORPH_RECT, Size(2 * morph_size + 1, 2 * morph_size + 1), Point(morph_size, morph_size));

	morphologyEx(im, res, MORPH_TOPHAT, element);

	return res;
}

Mat blackHat(Mat im, int morph_size = 1)
{
	Mat res;
	Mat element = getStructuringElement(MORPH_RECT, Size(2 * morph_size + 1, 2 * morph_size + 1), Point(morph_size, morph_size));

	morphologyEx(im, res, MORPH_BLACKHAT, element);

	return res;
}

Mat simpInv(Mat im)
{
	Mat allW(im.size(), CV_8UC1, Scalar(255));
	return allW - im;
}

Mat doSmth(Mat im)
{
	Mat blackH = blackHat(im, 10);
	Mat topH = topHat(im, 10);

	//return blackH;
	//return topH;
	//return blackH;
	return (blackH - topH);
}


Mat changeIntSmth(Mat im)
{
	uchar minVal = 300;
	uchar maxVal = 0;
	Mat res = im.clone();

	for (int y = 0; y < im.rows; y++)
	{
		for (int x = 0; x < im.cols; x++)
		{
			if (im.at<uchar>(y, x) > maxVal)
			{
				maxVal = im.at<uchar>(y, x);
			}

			if (im.at<uchar>(y, x) < minVal)
			{
				minVal = im.at<uchar>(y, x);
			}
		}
	}


	for (int y = 0; y < im.rows; y++)
	{
		for (int x = 0; x < im.cols; x++)
		{
			res.at<uchar>(y, x) = 255 * (((double)(im.at<uchar>(y, x) - minVal)) / (maxVal - minVal));
		}
	}
	return res;
}

Mat otsusThresh(Mat im)
{
	Mat res;
	threshold(im, res, 0, 255, CV_THRESH_BINARY | CV_THRESH_OTSU);
	return res;
}

double otsusThreshVal(Mat im)
{
	return threshold(im, im, 0, 255, CV_THRESH_BINARY | CV_THRESH_OTSU);
}

Mat testLapl(Mat im)
{
	/// Remove noise by blurring with a Gaussian filter
	GaussianBlur(im, im, Size(3, 3), 0, 0, BORDER_DEFAULT);
	cvtColor(im, im, CV_BGR2GRAY);

	/// Apply Laplace function
	Mat abs_dst;

	Laplacian(im, im, CV_16S, 3, 1, 0, BORDER_DEFAULT);
	convertScaleAbs(im, abs_dst);

	return abs_dst;
}



Mat mySobel(Mat greyIm, bool returnAngles = false)
{
	Mat res(greyIm.size(), CV_8UC1, Scalar(0));
	Mat angles(greyIm.size(), CV_8UC1, Scalar(0));

	for (int y = 1; y < greyIm.rows - 1; y++)
	{
		for (int x = 1; x < greyIm.cols - 1; x++)
		{
			/*
			int xResp = - (3 * greyIm.at<uchar>(y - 1, x - 1)) + (3 * greyIm.at<uchar>(y - 1, x + 1))
						- (10 * greyIm.at<uchar>(y, x - 1)) + (10 * greyIm.at<uchar>(y, x + 1))
						- (3 * greyIm.at<uchar>(y + 1, x - 1)) + (3 * greyIm.at<uchar>(y + 1, x + 1));

			int yResp = - (3 * greyIm.at<uchar>(y - 1, x - 1)) + (3 * greyIm.at<uchar>(y + 1, x - 1))
				- (10 * greyIm.at<uchar>(y - 1, x)) + (10 * greyIm.at<uchar>(y + 1, x))
				- (3 * greyIm.at<uchar>(y - 1, x + 1)) + (3 * greyIm.at<uchar>(y + 1, x + 1));
				*/

			
			
				
			double xResp = -(greyIm.at<uchar>(y - 1, x - 1)) + (greyIm.at<uchar>(y - 1, x + 1))
				- (2 * greyIm.at<uchar>(y, x - 1)) + (2 * greyIm.at<uchar>(y, x + 1))
				- (greyIm.at<uchar>(y + 1, x - 1)) + (greyIm.at<uchar>(y + 1, x + 1));

			double yResp = -(greyIm.at<uchar>(y - 1, x - 1)) + (greyIm.at<uchar>(y + 1, x - 1))
				- (2 * greyIm.at<uchar>(y - 1, x)) + (2 * greyIm.at<uchar>(y + 1, x))
				- (greyIm.at<uchar>(y - 1, x + 1)) + (greyIm.at<uchar>(y + 1, x + 1));
				

			double resp = sqrt((xResp * xResp) + (yResp * yResp));
			res.at<uchar>(y, x) = std::min(resp, (double)255);

			double edgedirection = (atan2(yResp, xResp)) * (180 / 3.14159265);

			while (edgedirection < 0)
			{
				edgedirection += 360;
			}

			while (edgedirection > 360)
			{
				edgedirection -= 360;
			}

			if (edgedirection > 337.5 || edgedirection <= 22.5)
			{
				edgedirection = 0;
			}
			else if (edgedirection > 22.5 && edgedirection <= 67.5)
			{
				edgedirection = 45;
			}
			else if (edgedirection > 67.5 && edgedirection <= 112.5)
			{
				edgedirection = 90;
			}
			else if (edgedirection > 112.5 && edgedirection <= 157.5)
			{
				edgedirection = 135;
			}
			else if (edgedirection > 157.5 && edgedirection <= 202.5)
			{
				edgedirection = 0;
			}
			else if (edgedirection > 202.5 && edgedirection <= 247.5)
			{
				edgedirection = 45;
			}
			else if (edgedirection > 247.5 && edgedirection <= 292.5)
			{
				edgedirection = 90;
			}
			else if (edgedirection > 292.5 && edgedirection <= 337.5)
			{
				edgedirection = 135;
			}


			angles.at<uchar>(y, x) = edgedirection;
		}
	}

	if (returnAngles)
	{
		return angles;
	}

	return res;
}

// my attempt at canny
Mat NonMaxSupr(Mat SobelIn, bool doDoubleThresh = false)
{
	Mat sobelRes = mySobel(SobelIn);
	Mat angles = mySobel(SobelIn, true);
	Mat thinnedRes = sobelRes.clone();
	//return thinnedRes;
	for (int y = 0; y < thinnedRes.rows; y++)
	{
		for (int x = 0; x < thinnedRes.cols; x++)
		{
			if (thinnedRes.at<uchar>(y, x) > 0)
			{
				thinnedRes.at<uchar>(y, x) = 255;
			}
		}
	}
	
	double maxThresh = Filter::Otsus(SobelIn);
	double minThresh = maxThresh * 0.5;

	for (int y = 1; y < sobelRes.rows - 1; y++)
	{
		for (int x = 1; x < sobelRes.cols - 1; x++)
		{
			/*
			if the rounded gradient angle is 0� (i.e. the edge is in the north�south direction) the point will be considered to be on the edge if its gradient magnitude is greater than the magnitudes at pixels in the east and west directions,
			if the rounded gradient angle is 90� (i.e. the edge is in the east�west direction) the point will be considered to be on the edge if its gradient magnitude is greater than the magnitudes at pixels in the north and south directions,
			if the rounded gradient angle is 135� (i.e. the edge is in the northeast�southwest direction) the point will be considered to be on the edge if its gradient magnitude is greater than the magnitudes at pixels in the north west and south east directions,
			if the rounded gradient angle is 45� (i.e. the edge is in the north west�south east direction) the point will be considered to be on the edge if its gradient magnitude is greater than the magnitudes at pixels in the north east and south west directions.
			*/
			double angle = angles.at<uchar>(y, x);
			if (angle == 0)
			{
				auto grad1 = sobelRes.at<uchar>(y, x - 1);
				auto grad = sobelRes.at<uchar>(y, x);
				auto grad2 = sobelRes.at<uchar>(y, x + 1);

				if (grad < grad1 || grad < grad2)
				{
					thinnedRes.at<uchar>(y, x) = 0;
				}

			}
			
			if (angle == 90)
			{
				auto grad1 = sobelRes.at<uchar>(y - 1, x);
				auto grad = sobelRes.at<uchar>(y, x);
				auto grad2 = sobelRes.at<uchar>(y + 1, x);

				if (grad < grad1 || grad < grad2)
				{
					thinnedRes.at<uchar>(y, x) = 0;
				}

			}
			
			
			if (angle == 135)
			{
				auto grad1 = sobelRes.at<uchar>(y - 1, x + 1);
				auto grad = sobelRes.at<uchar>(y, x);
				auto grad2 = sobelRes.at<uchar>(y + 1, x - 1);

				if (grad < grad1 || grad < grad2)
				{
					thinnedRes.at<uchar>(y, x) = 0;
				}
			}
			
			
			
			if (angle == 45)
			{
				auto grad1 = sobelRes.at<uchar>(y - 1, x - 1);
				auto grad = sobelRes.at<uchar>(y, x);
				auto grad2 = sobelRes.at<uchar>(y + 1, x + 1);

				if (grad < grad1 || grad < grad2)
				{
					thinnedRes.at<uchar>(y, x) = 0;
				}



			}

			/*
			if (doDoubleThresh)
			{
				if (sobelRes.at<uchar>(y, x) < minThresh)
				{
					thinnedRes.at<uchar>(y, x) = 0;
				}
			}
			*/
		}
	}

	// drop pixels below minimum threshold

	for (int y = 1; y < sobelRes.rows - 1; y++)
	{
		for (int x = 1; x < sobelRes.cols - 1; x++)
		{
			if (doDoubleThresh)
			{
				if (sobelRes.at<uchar>(y, x) < minThresh)
				{
					thinnedRes.at<uchar>(y, x) = 0;
				}
			}
		}
	}
	
	
	Mat visited(thinnedRes.size(), CV_8UC1, Scalar(0));
	/*
	if (doDoubleThresh)
	{
		for (int y = 1; y < sobelRes.rows - 1; y++)
		{
			for (int x = 1; x < sobelRes.cols - 1; x++)
			{
				auto grad = sobelRes.at<uchar>(y, x);
				if (grad >= maxThresh)
				{
					continue;
				}

				if (grad < maxThresh && grad >= minThresh && thinnedRes.at<uchar>(y, x) == 255)
				{
					auto st = stack<pair<vec2<int>, int>>();
					st.push(make_pair(vec2<int>(x, y), 0));
					int kern = 1;
					bool connected = false;
					
					visited = 0;
					
					int maxDist = 8;

					while (!st.empty())
					{
						auto el = st.top();
						st.pop();

						if (sobelRes.at<uchar>(el.first.y, el.first.x) >= maxThresh)
						{
							connected = true;
							break;
						}

						for (int yO = -kern; yO <= kern; yO++)
						{
							for (int xO = -kern; xO <= kern; xO++)
							{
								if (el.second < maxDist &&
									ImgProc::inBoundsWithVal(thinnedRes, vec2<int>(el.first.x + xO, el.first.y + yO), 255) &&
									visited.at<uchar>(el.first.y + yO, el.first.x + xO) == 0)
								{
									visited.at<uchar>(el.first.y + yO, el.first.x + xO) = 255;
									st.push(make_pair(vec2<int>(el.first.x + xO, el.first.y + yO), 1 + el.second));
								}
							}
						}

					}

					if (!connected)
					{
						thinnedRes.at<uchar>(y, x) = 0;
					}
				}
			}
		}
		
	}
	*/
	return thinnedRes;
}

int main()
{
	/*
	Mat t_zb = imread("C:/Users/Matiss/Desktop/hsv_v.png");
	Mat t_zb_g; 
	
	cvtColor(t_zb, t_zb_g, CV_BGR2GRAY);

	Utility::writeImage(Filter::FreivaldRidgeImg(t_zb_g), "C:/Users/Matiss/Desktop/t_zebr.png");
	return 0;
	*/

	/*
	Mat t_ap = imread("C:/Users/Matiss/Desktop/tRes/at_apst_t.png", 0);

	Mat t_ap_dr = ImgProc::dropRegionsBelowAvg(t_ap);


		t_ap_dr = ImgProc::elipseDilate(t_ap_dr, 5);



		t_ap_dr = ImgProc::elipseErode(t_ap_dr, 5);

	


	Utility::writeImage(ImgProc::dropRegionsBelowAvg(t_ap), "C:/Users/Matiss/Desktop/tRes/at_apst_r.png");
	Utility::writeImage(t_ap_dr, "C:/Users/Matiss/Desktop/tRes/at_apst_r_ed.png");
	return 0;
	
	*/

	/*
	string pths;
	cin >> pths;
	double low;
	double high;

	cin >> low >> high;

	//Mat dIm = imread("C:/Users/Matiss/Desktop/zwq3_t2.png");
	Mat dIm = imread(pths);
	DoGDetect dogD(dIm);
	auto res = dogD.detect(low, high);

	for (int i = 0; i < res.size(); i++)
	{
		Utility::writeImage(res[i], "C:/Users/Matiss/Desktop/tRes/v"+to_string(i)+".png");
	}
	return 0;
	*/

	/*
	Mat berVein = imread("C:/Users/Matiss/Desktop/bak_darbs/lapas/berzs_sakums.jpg");
	


	DoGDetect doed(berVein);
	
	Utility::writeImage(doed.detect(9, 15)[0], "C:/Users/Matiss/Desktop/berzs_sak_l_veins.png");
	return 0;
	*/
	/*
	Mat klava = imread("C:/Users/Matiss/Desktop/q3_t2.png", 1);
	DoGDetect eogEd(klava);
	Mat klava_v = eogEd.detect()[0]; 
	klava_v = ImgProc::dropRegionsBelowAvg(klava_v);
	klava_v = ImgProc::elipseDilate(klava_v, 5);
	klava_v = ImgProc::elipseErode(klava_v, 5);

	Utility::writeImage(klava_v, "C:/Users/Matiss/Desktop/klava_veins.png");
	return 0;
	*/


	/*

	Mat berVein = imread("C:/Users/Matiss/Desktop/q3_t2-HSV.png", 0);
	Mat berVeinCol = imread("C:/Users/Matiss/Desktop/q3_t2.png");
	Mat berVeinOr = berVein.clone();
	GaussianBlur(berVein, berVein, Size(15, 15), 0);

	double threshVal = Filter::Otsus(berVein);
	double minThreshVal = threshVal * 0.5;

	Mat veinRe = Filter::FreivaldRidgeImg(berVein);
	Mat veinReTh;

	threshold(veinRe, veinReTh, minThreshVal, 255, CV_THRESH_BINARY);


	GaussianBlur(berVeinOr, berVeinOr, Size(7, 7), 0);
	Mat berVeinEdg = Filter::CannyWithOtsus(berVeinOr, false);

	veinReTh = ImgProc::dropRegionsBelowAvg(ImgProc::dropRegionsBelowAvg(veinReTh));
	Thinning::GuoHall::thinn(veinReTh);

	Utility::writeImage(veinReTh, "C:/Users/Matiss/Desktop/testRidg.png");
	Utility::writeImage(berVeinEdg, "C:/Users/Matiss/Desktop/testEdg.png");
	Utility::writeImage(ImgProc::mergeBinImgToColor(veinReTh, Vec3b(0, 0, 255), berVeinEdg, Vec3b(255, 255, 255)), "C:/Users/Matiss/Desktop/testEandR.png");

	RidgeEdgeDetect ridgEdgD(veinReTh, berVeinEdg, berVeinCol);
	Utility::writeImage(ridgEdgD.doIteration(), "C:/Users/Matiss/Desktop/testreJoin.png");
	return 0;

	REDetect REDet(berVeinEdg, veinReTh, 20);
	REDet.detect();

	Utility::writeImage(REDet.getRes(), "C:/Users/Matiss/Desktop/testRidgD.png");
	return 0;



	Mat guo = berVein.clone();


	Thinning::GuoHall::thinn(guo);

	cout << "thinned img" << endl;


	VeinHeights vhts(berVein, guo);
	vhts.calculate();

	cout << "done mapping " << endl;

	vector<vector<int>> hgtsC = vhts.getMap();

	ImgHeightMap result;
	Mesh mesh(result, berVein);
	mesh.generatePointsFromSimpMap(hgtsC);


	cout << "generated points " << endl;
	cout << "max height " << mesh.maxHeight << endl;

	auto voxs = mesh.getVoxelGrid();
	cout << "got voxels " << endl;
	*/

	Mat compIm = imread("C:/Users/Matiss/Desktop/sm_l.png", 0);
	Mat skel22 = compIm.clone();
	Thinning::GuoHall::thinn(skel22);

	VeinHeights vhts(compIm, skel22);
	vhts.calculate();

	cout << "done mapping " << endl;

	vector<vector<int>> hgtsC = vhts.getMap();

	ImgHeightMap result;
	ThreeDMap thdM;
	thdM.calcDistUsingKernels(compIm, skel22);
	result = *thdM.getResultMap();

	int start_s = clock();
	Mesh mesh(result, compIm);
	int stop_s = clock();
	cout << "time: " << (stop_s - start_s) / double(CLOCKS_PER_SEC) * 1000 << endl;
	mesh.exportPointsToWavefront("myobj", "C:/Users/Matiss/Desktop/");
	return 0;

	//mesh.generatePointsFromSimpMap(hgtsC);


	cout << "generated points " << endl;
	cout << "max height " << mesh.maxHeight << endl;

	auto voxs = mesh.getVoxelGrid();
	cout << "got voxels " << endl;

	double thresh = 0;
	double step = 1;

	MarchingCubes marchUnit(voxs, step, thresh);
	
	marchUnit.march();
	
	//marchUnit.printMesh();
	marchUnit.printMeshUpd();

	cout << "got mesh" << endl;
	int waiter;
	cin >> waiter;
	return 0;

//	mesh.exportPointsToWavefront("testKlava", "C:/Users/Matiss/Desktop");
	//mesh.exportFrontBottomPointsToWavefront("testKlava", "C:/Users/Matiss/Desktop");

	TaubinSmooth tsmth("C:/Users/Matiss/Desktop/march.obj", 0.4, -0.45, 17);
	tsmth.smooth();
	tsmth.writeData("C:/Users/Matiss/Desktop/testKlava_sm.obj");

	return 0;

	//Utility::writeImage(ImgProc::mergeBinImgToColor(guo, Vec3b(0, 0, 255), berVein, Vec3b(255, 255, 255)), "C:/Users/Matiss/Desktop/ber_guo.png");
	//Utility::writeImage(ImgProc::mergeBinImgToColor(zhan, Vec3b(0, 0, 255), berVein, Vec3b(255, 255, 255)), "C:/Users/Matiss/Desktop/ber_zhan.png");
	
	/*
	ThreeDMap mapper;
	ImgHeightMap result;

	mapper.calcDistUsingKernels(berVein, guo);



	result = *mapper.getResultMap();

	cout << "done mapping " << endl;

	Mesh mesh(result, berVein);

	return 0;


	auto voxs = mesh.getVoxelGrid();
	cout << "got voxels " << endl;

	double thresh = 0;
	double step = 1;

	MarchingCubes marchUnit(voxs, step, thresh);
	marchUnit.march();
	marchUnit.printMesh();

	cout << "got mesh" << endl;
	
	mesh.exportPointsToWavefront("testKlava", "C:/Users/Matiss/Desktop");
	//mesh.exportFrontBottomPointsToWavefront("testKlava", "C:/Users/Matiss/Desktop");

	TaubinSmooth tsmth("C:/Users/Matiss/Desktop/march.obj", 0.4, -0.45, 17);
	tsmth.smooth();
	tsmth.writeData("C:/Users/Matiss/Desktop/testKlava_sm.obj");
//	tsmth.writeDataUniq("C:/Users/Matiss/Desktop/testKlava_sm.obj");

//	Utility::writeImage(ImgProc::getBinaryBorder(berVein), "C:/Users/Matiss/Desktop/ber_bord.png");
//	Utility::writeImage(guo, "C:/Users/Matiss/Desktop/ber_guo.png");
	return 0;

	*/
	
	/* thinning
	Mat testE = imread("C:/Users/Matiss/Desktop/sm_l.png", 0);
	Mat tR = Thinning::ZhangSuen2::runAlgo(testE);

	

	Utility::writeImage(ImgProc::mergeBinImgToColor(tR, Vec3b(0, 0, 255), testE, Vec3b(255, 255, 255)), "C:/Users/Matiss/Desktop/sm_l_my_zhan.png");
	Thinning::ZhangSuen::thinn(testE);
	Utility::writeImage(testE, "C:/Users/Matiss/Desktop/sm_l_his_zhan.png");
	return 0;

	Mat HSI;

	cvtColor(testE, HSI, CV_BGR2HSV);

	vector<Mat> chans;
	split(HSI, chans);

	blur(chans[2], chans[2], Size(3, 3));

	Utility::writeImage(mySobel(chans[2]), "C:/Users/Matiss/Desktop/3_t2_sobel.png");
	Utility::writeImage(NonMaxSupr(chans[2], true), "C:/Users/Matiss/Desktop/3_t2_sobel_supr.png");
	Utility::writeImage(Filter::CannyWithOtsus(chans[2]), "C:/Users/Matiss/Desktop/3_t2_canny_ots.png");
	Utility::writeImage(Filter::FreivaldRidgeImg(chans[2], true), "C:/Users/Matiss/Desktop/3_t2_ridges.png");
//	Scharr(chans[2], chans[2], CV_8U, 1, 1,);


	Utility::writeImage(chans[2], "C:/Users/Matiss/Desktop/cv_sob.png");
	//Utility::writeImage(testE, "C:/Users/Matiss/Desktop/3_t2_main_reg_gray.png");
	//Utility::writeImage( ImgProc::otsusThresh(ImgProc::inv(Filter::DoG(chans[2], Size(9, 9), Size(3, 3)))), "C:/Users/Matiss/Desktop/3_t2_Ilum.png");
	return 0;
	*/

	/*
	Mat testE = imread("C:/Users/Matiss/Desktop/3_t2.png");
	Mat HSI = ImgProc::GBRToHSI(testE);
	
	Utility::writeImage(HSI, "C:/Users/Matiss/Desktop/hsi.png");
	cvtColor(testE, testE, CV_BGR2HSV);
	Utility::writeImage(testE, "C:/Users/Matiss/Desktop/hsv.png");

	vector<Mat> channels1;
	split(testE, channels1);

	Utility::writeImage(channels1[0], "C:/Users/Matiss/Desktop/hsv_h.png");
	Utility::writeImage(channels1[2], "C:/Users/Matiss/Desktop/hsv_v.png");

	vector<Mat> channels2;
	split(HSI, channels2);

	Utility::writeImage(channels2[0], "C:/Users/Matiss/Desktop/hsi_h.png");
	Utility::writeImage(channels2[2], "C:/Users/Matiss/Desktop/hsi_i.png");
	*/

	/*
	Mat hT = imread("C:/Users/Matiss/Desktop/3_t2.png");
	blur(hT, hT, Size(3, 3));
	Mat orhT = hT.clone();
	Mat orhT2 = hT.clone();
	
	hT = ImgProc::GBRToHSI(hT);
	auto buckets = vector<pair<uchar, int>>({

		make_pair(14, 0),
		make_pair(29, 0),
		make_pair(44, 0),

		make_pair(59, 0),
		make_pair(74, 0),
		make_pair(89, 0),

		make_pair(104, 0),
		make_pair(119, 0),
		make_pair(134, 0),

		make_pair(149, 0),
		make_pair(164, 0),
		make_pair(179, 0)
	});

	uchar maxI = 0;
	uchar minI = 500;

	Vec3b pixel = hT.at<Vec3b>(0, 0); // read pixel (0,0) (make copy)
	cout << pixel[0] << endl;

	for (int y = 0; y < hT.rows; y++)
	{
		for (int x = 0; x < hT.cols; x++)
		{
			Vec3b pixel = hT.at<Vec3b>(y, x);
			if (pixel[2] > maxI)
				maxI = pixel[2];
			if (pixel[2] < minI)
				minI = pixel[2];

			for (int b = 0; b < buckets.size(); b++)
			{
				if (pixel[0] <= buckets[b].first)
				{
					buckets[b].second += 1;
					break;
				}
			}
		}
	}

	cout << "min, max (" + to_string(minI) + ", " + to_string(maxI) + ")" << endl;
	for (int b = 0; b < buckets.size(); b++)
	{
		cout << "bucket " << to_string(buckets[b].first) << " " << buckets[b].second << endl;
	}

	int maxBucket = 0;
	int maxBucketI = 0;
	int maxBucketlI = 0;
	uchar maxBucketH = 0;
	bool highIZero = false;

	for (int b = 0; b < buckets.size(); b++)
	{
		if (buckets[b].second > maxBucket)
		{
			maxBucket = buckets[b].second;
			maxBucketH = buckets[b].first;
			maxBucketI = b;
		}
	}

	if (maxBucketI > 0)
	{
		maxBucketlI = maxBucketI - 1;
	}
	else
	{
		highIZero = true;
	}

	for (int y = 0; y < hT.rows; y++)
	{
		for (int x = 0; x < hT.cols; x++)
		{
			Vec3b pixel = hT.at<Vec3b>(y, x);
			Vec3b unset(0, 0, 0);

			if (highIZero)
			{
				if (pixel[0] <= buckets[maxBucketI].first)
				{
					orhT.at<Vec3b>(y, x) = unset;
				}
			}
			else
			{
				if (pixel[0] <= buckets[maxBucketI].first && pixel[0] > buckets[maxBucketlI].first) //||
					//	pixel[0] <= buckets[0].first)
				{
					orhT.at<Vec3b>(y, x) = unset;
				}
			}
		}
	}


	for (int y = 0; y < hT.rows; y++)
	{
		for (int x = 0; x < hT.cols; x++)
		{
			Vec3b pixel = hT.at<Vec3b>(y, x);
			pixel[2] = enlargeI(minI, maxI, pixel[2], 1);

			hT.at<Vec3b>(y, x) = pixel;
		}
	}
	Mat testE = imread("C:/Users/Matiss/Desktop/3_t2.png");

	cvtColor(testE, testE, CV_BGR2GRAY);
	vector<Mat> channels;
	split(hT, channels);


	Mat V = channels[2];
	Utility::writeImage(V, "C:/Users/Matiss/Desktop/valEnl.png");
	Utility::writeImage(ImgProc::otsusThresh(V), "C:/Users/Matiss/Desktop/valEnl_b.png");

	Utility::writeImage(orhT, "C:/Users/Matiss/Desktop/noMez.png");

	Utility::writeImage(ImgProc::otsusThresh(changeIntSmth(doSmth(V))), "C:/Users/Matiss/Desktop/morphRes.png");

	//Utility::writeImage(cH, "C:/Users/Matiss/Desktop/hue.png");
	int www;
	cin >> www;
	return 0;

	*/




	/*
	Mat testE = imread("C:/Users/Matiss/Desktop/bak_darbs/lapas/3_to.png");
	RidgeEdgeDetect red(testE);

	Utility::writeImage(ImgProc::otsusThresh(red.printPixelTypeMap()), "C:/Users/Matiss/Desktop/smrterJoin.png");

	return 0;
	*/

	/*
	Mat testE = imread("C:/Users/Matiss/Desktop/bak_darbs/lapas/b_vl.png");
//	Mat testE = imread("C:/Users/Matiss/Desktop/ffffs.jpg");
	Mat testE2 = testE.clone();
	blur(testE2, testE2, Size(3, 3));


	Mat testE3 = testE.clone();

	cvtColor(testE, testE, CV_BGR2GRAY);
	cvtColor(testE3, testE3, CV_BGR2GRAY);
	Mat otsusCan = Filter::CannyWithOtsus(testE);

	blur(testE3, testE3, Size(3, 3));
	Mat ridgesE3 = Filter::FreivaldRidgeImg(testE3);


	RidgeEdgeDetect red(ridgesE3, otsusCan, testE2);
	red.doIteration();
	Mat rezzz = red.fillGaps();
	rezzz = ImgProc::dropRegionsBelowAvg(rezzz);

	for (int i = 0; i < 5; i++)
	{
		rezzz = ImgProc::elipseDilate(rezzz);
	}

	for (int i = 0; i < 5; i++)
	{
		rezzz = ImgProc::elipseErode(rezzz);
	}

	Utility::writeImage(rezzz, "C:/Users/Matiss/Desktop/smrterJoin.png");
	Utility::writeImage(otsusCan, "C:/Users/Matiss/Desktop/otsus.png");
	Utility::writeImage(ridgesE3, "C:/Users/Matiss/Desktop/curRidg.png");
	red.printPixelTypeMap();


	int wwwww;
	cin >> wwwww;
	return 0;
	*/

	/* test raksts 1
	Mat testE = imread("C:/Users/Matiss/Desktop/bak_darbs/lapas/3_t2.png");
	Mat testESg = testGrey(testE); 

	Utility::writeImage(otsusThresh(changeIntSmth(doSmth(testESg))), "C:/Users/Matiss/Desktop/smsh.png");


	Utility::writeImage(testESg, "C:/Users/Matiss/Desktop/bestGrey.png");
	return 0;
	*/

	/*
	Mat enl = imread("C:/Users/Matiss/Desktop/valenl5.png", 0);
	blur(enl, enl, Size(3, 3));
	Mat enlRidges = Filter::FreivaldRidgeImg(enl);
	Utility::writeImage(enlRidges, "C:/Users/Matiss/Desktop/curRidgEnl.png");
	return 0;



	Mat testE = imread("C:/Users/Matiss/Desktop/bak_darbs/lapas/3_t2.png");
	Mat testE2 = testE.clone();
	Mat testE3 = testE.clone();

	cvtColor(testE, testE, CV_BGR2GRAY);
	cvtColor(testE3, testE3, CV_BGR2GRAY);
	Mat otsusCan = Filter::CannyWithOtsus(testE);
	Mat curCan = Filter::CannyEdge(testE2, 100, 200);

	blur(testE3, testE3, Size(3, 3));
	Mat ridgesE3 = Filter::FreivaldRidgeImg(testE3);
	ridgesE3 = imread("C:/Users/Matiss/Desktop/curRidg.png", 0);
	ridgesE3  = ImgProc::dropRegionsBelowAvg(ridgesE3);


	RidgeEdgeDetect red(ridgesE3, otsusCan);
	Mat tJn = red.doIteration(7);

	Utility::writeImage(tJn, "C:/Users/Matiss/Desktop/tJoin.png");
	Utility::writeImage(otsusCan, "C:/Users/Matiss/Desktop/otsus.png");
	Utility::writeImage(curCan, "C:/Users/Matiss/Desktop/curCan.png");
	Utility::writeImage(ridgesE3, "C:/Users/Matiss/Desktop/curRidg.png");
	return 0;
	*/

/*
	double thresh;
	double step;


	//return 0;
	///

	Mat kl_b = imread("C:/Users/Matiss/Desktop/sm_l.png", 0);
	Mat kl_b_s = kl_b.clone();
	Mat kl_b_e = ImgProc::getBinaryBorder(kl_b);
	

	Thinning::GuoHall::thinn(kl_b_s);

//	Utility::writeImage(kl_b_s, "C:/Users/Matiss/Desktop/sm_l_skl.png");
//	Utility::writeImage(kl_b_e, "C:/Users/Matiss/Desktop/sm_l_e.png");

	ThreeDMap mapper;
	ImgHeightMap result;

	mapper.calcDistUsingKernels(kl_b, kl_b_s);

	result = mapper.getResultMap();

	Mesh mesh(result, kl_b);

	auto voxs = mesh.getVoxelGrid();
	cout << voxs.size() << " " << voxs[0].size() << endl;

	thresh = 0;
	step = 0.3;

	MarchingCubes marchUnit(voxs, step, thresh);
	marchUnit.march();
	marchUnit.printMesh();


	mesh.exportPointsToWavefront("testKlava", "C:/Users/Matiss/Desktop");
	mesh.exportBottomPointsToWavefront("testKlavaOnlyBot", "C:/Users/Matiss/Desktop");
	mesh.exportFrontBottomPointsToWavefront("testKlavaBot", "C:/Users/Matiss/Desktop");

	*/

	TaubinSmooth tsm("C:/Users/Matiss/Desktop/march.obj", 0.7, -0.95, 10);
	tsm.smooth();
	tsm.writeData("C:/Users/Matiss/Desktop/march2.obj");
	

	return 0;








	Mat ai = imread("C:/Users/Matiss/Desktop/sm_tt.png", 0);
	Mat ai_sk = ai.clone();

	Thinning::GuoHall::thinn(ai_sk);

	BranchPartition bp(ai, ai_sk);
	bp.partitionCenter();

	vector<Mat> rez = bp.printCenterPartitions();
	for (int i = 0; i < rez.size(); i++)
	{
		Utility::writeImage(rez[i], "C:/Users/Matiss/Desktop/ttt/sm_tt_part"+to_string(i)+".png");
	}
	
	return 0;









	Mat t = imread("C:/Users/Matiss/Desktop/sm.png", 0);
	Mat skel = t.clone();

	Thinning::GuoHall::thinn(skel);

	Mat edg = Filter::CannyEdge(t, 255, 255, Size(3, 3), true, true);
//	Utility::writeImage(edg, "C:/Users/Matiss/Desktop/sm_t_e.png");
//	return 0;

//	Utility::writeImage(skel, "C:/Users/Matiss/Desktop/smsk.png");
	Mesh2 m(ImgHeightMap(), skel, edg);
	auto splns = m.partitionCenterCurves();

	m.exportSpline(splns[0], 2.0, "1");
	auto eSplns = m.findEdgeCurvesFromCenter(splns[0]);
	m.exportSpline(eSplns[0], 2.0, "2");
	m.exportSpline(eSplns[1], 2.0, "3");

	m.trigCurveWithEdges(splns[0], eSplns[0], eSplns[1]);
	return 0;

	for (int i = 0; i < splns.size(); i++)
	{
		auto eSplns = m.findEdgeCurvesFromCenter(splns[i]);
		m.trigCurveWithEdges(splns[i], eSplns[0], eSplns[1], 2.0, to_string(i));
	}
	


	

	int wz;
	cin >> wz;

	return 0;

	/*
	while (true)
	{
		float pos;
		cin >> pos;
		if (pos < 0)
		{
			break;
		}
		vec3 pnt = splns[0]->GetInterpolatedSplinePoint(pos);

		cout << "val at " << pos << " " << pnt.x << " " << pnt.y << " " << pnt.z << endl;;

	}
	*/

	int w;
	cin >> w;
	return 0;
}

int main_o()
{
	Mat tt = imread("C:/Users/Matiss/Desktop/sm.png", 0);
	Mat klavaBinSkelInt = tt.clone();

	Thinning::GuoHall::thinn(klavaBinSkelInt);
	Mat klavaSkelett = klavaBinSkelInt;





	ThreeDMap mappert;
	ImgHeightMap resultt;
	//mapper.gen(klavaBin, klavaSkelet);
	mappert.calcDistUsingKernels(tt, klavaSkelett);

	resultt = *mappert.getResultMap();
	Utility::writeImage(mappert.printResult(), "C:/Users/Matiss/Desktop/smm.png");


	Mesh mesht(resultt, tt);
	mesht.exportPointsToWavefront("testKlava", "C:/Users/Matiss/Desktop");

	auto vo = mesht.getVoxelGrid2();
	for (int z = 0; z < vo[0][0].size(); z++)
	{ 
		for (int i = 0; i < vo.size(); i++)
		{
			for (int n = 0; n < vo[i].size(); n++)
			{
				cout << vo[i][n][z] << " ";
			}
			cout << endl;
		}

		cout << endl << endl << endl;
	}

	int zw;
	cin >> zw;
	return 0;

	auto voxs = vector<vector<vector<double>>>();

	for (int y = 0; y < 7; y++)
	{
		auto xVec = vector<vector<double>>();
		for (int x = 0; x < 15; x++)
		{
			auto zVec = vector<double>();
			for (int z = 0; z < 5; z++)
			{
				zVec.push_back(0.0);
			}
			xVec.push_back(zVec);
		}
		voxs.push_back(xVec);
	}
	/*
	voxs[2][2][1] = 1;
	voxs[2][2][2] = 1;
	voxs[2][2][3] = 1;
	voxs[2][3][3] = 1;
	*/

	voxs[1][7][1] = 1;
	voxs[1][8][2] = 1;
	voxs[1][9][2] = 1;
	voxs[1][10][2] = 1;

	voxs[2][7][2] = 1;
	voxs[2][8][2] = 1;
	voxs[2][9][3] = 1;
	voxs[2][10][2] = 1;

	voxs[3][6][2] = 1;
	voxs[3][7][2] = 1;
	voxs[3][8][3] = 1;
	voxs[3][9][2] = 1;
	voxs[3][10][2] = 1;

	voxs[4][5][2] = 1;
	voxs[4][6][3] = 1;
	voxs[4][7][3] = 1;
	voxs[4][8][2] = 1;
	voxs[4][9][2] = 1;
	voxs[4][10][1] = 1;
	

	voxs[5][4][3] = 1;
	voxs[5][5][3] = 1;
	voxs[5][6][2] = 1;
	voxs[5][7][2] = 1;
	voxs[5][8][2] = 1;
	voxs[5][9][1] = 1;
	

	MarchingCubes mc(voxs, 0.17, 0.45);
	mc.march();
	mc.printMesh();

	int w;
	//cin >> w;
	return 0;
	
	//Mat berzsmrbin = imread("C:/Users/Matiss/Desktop/berz_xs_dzisl.png", 0);
	//Utility::writeImage(ImgProc::dropRegionsBelowAvg(berzsmrbin), "C:/Users/Matiss/Desktop/berz_xs_dzisl_dropped.png");
	//return 0;

	/*
	Mat berzsmrbin = imread("C:/Users/Matiss/Desktop/berz_xs_ridge_bin.png", 0);

	for (int i = 0; i < 1; i++)
	{
		berzsmrbin = ImgProc::dropRegionsBelowAvg(berzsmrbin);
	}

	berzsmrbin = ImgProc::removeIsolatedRegions(berzsmrbin, 3);

	Utility::writeImage(berzsmrbin, "C:/Users/Matiss/Desktop/berz_xs_ridge_bin_drop.png");



	return 0;

	Mat berzsmr = imread("C:/Users/Matiss/Desktop/berz_xs.png");

	GaussianBlur(berzsmr, berzsmr, Size(5, 5), 0);
	cvtColor(berzsmr, berzsmr, CV_BGR2GRAY);
	Utility::writeImage(Filter::FreivaldRidgeImg(berzsmr), "C:/Users/Matiss/Desktop/berz_xs_ridge.png");
	return 0;





	Mat berzsm = imread("C:/Users/Matiss/Desktop/berz_xs.png");
	
	GaussianBlur(berzsm, berzsm, Size(5, 5), 0);

	//Utility::displayImage(berzsm);
	Mat diff = Filter::DoG(berzsm, Size(1, 1), Size(9, 9));
	Utility::writeImage(diff, "C:/Users/Matiss/Desktop/berz_xs_df.png");
	return 0;


	*/


	


	Mat klavaBin = imread("C:/Users/Matiss/Desktop/sample3dxxs.png", 0);
	Mat klavaBinSkelIn = klavaBin.clone();
	Mat klavaBinSkel2In = klavaBin.clone();
	Mat klavaBinSkel3In = klavaBin.clone();
	//Utility::displayImage(klavaBin);

	//Mat klavaSkelet = ImgProc::skeleton(klavaBinSkelIn);
	Thinning::GuoHall::thinn(klavaBinSkelIn);
	Mat klavaSkelet = klavaBinSkelIn;

//	Utility::writeImage(klavaSkelet, "C:/Users/Matiss/Desktop/test3dskel.png");

//	Utility::displayImage(klavaBinSkel2In);
//	Thinning::ZhangSuen::thinn(klavaBinSkel2In);
//	Thinning::GuoHall::thinn(klavaBinSkel3In);
//	Utility::writeImage(klavaBinSkel2In, "C:/Users/Matiss/Desktop/compskel1.png");
//	Utility::writeImage(klavaBinSkel3In, "C:/Users/Matiss/Desktop/compskel2.png");
//	return 0;

	Mat black(klavaBin.size(), CV_8UC1, Scalar(0));

	ThreeDMap mapper;
	ImgHeightMap result;
	//mapper.gen(klavaBin, klavaSkelet);
	mapper.calcDistUsingKernels(klavaBin, klavaSkelet);

	result = *mapper.getResultMap();

	Mesh mesh(result, klavaBin);
	mesh.exportPointsToWavefront("testKlava", "C:/Users/Matiss/Desktop");
	/*
	auto voxs = mesh.getVoxelGrid();
	
	for (int y = 0; y < voxs.size(); y++)
	{
		for (int x = 0; x < voxs[y].size(); x++)
		{
			string res = "";
			for (int z = 0; z < voxs[y][x].size(); z++)
			{
				res += to_string(voxs[y][x][z]);
			}
			cout << res << " ";
		}
		cout << endl;
	}

	

	MarchingCubes mc(voxs, 0.04);
	*/
	mc.march();
	mc.printMesh();
//	Utility::writeImage(mapper.printResult(), "C:/Users/Matiss/Desktop/test3dmap_2.png");

//	mesh.findFaces2();
//	Utility::displayImage(black);
//	mapper.printLinkedSkel(black);
//	Utility::displayImage(black);
//	Utility::writeImage(black, "C:/Users/Matiss/Desktop/test3dskellink.png");

	int t;
	cin >> t;
	return 0;
}


