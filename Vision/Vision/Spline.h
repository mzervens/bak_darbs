#ifndef overhauserHPP
#define overhauserHPP

#include "vec3.h"
#include <vector>

class CRSpline
{
public:

	// Constructors and destructor
	CRSpline();
	CRSpline(const CRSpline&);
	~CRSpline();

	// Operations
	void AddSplinePoint(const vec3& v);
	vec3 GetInterpolatedSplinePoint(float t);   // t = 0...1; 0=vp[0] ... 1=vp[max]
	int GetNumPoints();
	vec3& GetNthPoint(int n);

	// Static method for computing the Catmull-Rom parametric equation
	// given a time (t) and a vector quadruple (p1,p2,p3,p4).
	static vec3 Eq(double t, vec3& p1, vec3& p2, vec3& p3, vec3& p4);

	std::vector<vec3> vp;
private:
	
	float delta_t;
};

#endif
