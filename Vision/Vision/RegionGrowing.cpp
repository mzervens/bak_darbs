#pragma once;

#include "opencv2/imgcodecs.hpp"
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <stack>
#include "Utility.cpp";
#include "ImgProc.cpp";
#include "Structs.cpp";


using namespace cv;
using namespace std;


class RegionGrowing
{
	Mat rgbIm;

public:
	RegionGrowing(Mat img)
	{
		rgbIm = img;
	}

	Mat findRegions(double thresh)
	{
		auto regions = vector<vector<vec2<int>>>();
		Mat visited(rgbIm.size(), CV_8UC1, Scalar(0));
		Mat reslt = rgbIm.clone();

		for (int y = 0; y < rgbIm.rows; y++)
		{
			for (int x = 0; x < rgbIm.rows; x++)
			{
				if (visited.at<uchar>(y, x) == 255)
				{
					continue;
				}

				auto st = stack<vec2<int>>();
				auto reg = vector<vec2<int>>();
				st.push(vec2<int>(x, y));
				reg.push_back(vec2<int>(x, y));
				visited.at<uchar>(y, x) = 255;
				int kern = 1;

				while (!st.empty())
				{

					auto curPos = st.top();
					st.pop();
					auto curPosVal = rgbIm.at<Vec3b>(y, x);
				
					for (int i = -kern; i <= kern; i++)
					{
						for (int n = -kern; n <= kern; n++)
						{
							auto pos = vec2<int>(n, i) + curPos;
							if (ImgProc::inBounds(rgbIm, pos) && pos != curPos && ImgProc::inBoundsWithVal(visited, pos, 0))
							{
								auto thisPix = rgbIm.at<Vec3b>(pos.y, pos.x);
								double dist = Utility::pointDist3D(thisPix[0], thisPix[1], thisPix[2],
																   curPosVal[0], curPosVal[1], curPosVal[2]);

								if (dist <= thresh)
								{
									visited.at<uchar>(pos.y, pos.x) = 255;
									reg.push_back(pos);
									st.push(pos);
								}
							}
						}
					}

				}

				regions.push_back(reg);

			}
		}


		for (int i = 0; i < regions.size(); i++)
		{
			Vec3b color = Vec3b(Utility::random(0, 256), Utility::random(0, 256), Utility::random(0, 256));
			for (int n = 0; n < regions[i].size(); n++)
			{
				reslt.at<Vec3b>(regions[i][n].y, regions[i][n].x) = color;
			}
		}

		return reslt;

	}
};