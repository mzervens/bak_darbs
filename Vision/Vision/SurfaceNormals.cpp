#pragma once

#include <stack>
#include <map>
#include <iostream>
#include <fstream>
#include "Utility.cpp";
#include "ImgProc.cpp";
#include "ImgHeightMap.cpp";
#include "Mesh.h";
using namespace cv;
using namespace std;

class SurfaceNormals
{
	Mat img;
	Mat skel;

public:
	SurfaceNormals(Mat binImg, Mat skelet)
	{
		img = binImg;
		skel = skelet;
	}
};