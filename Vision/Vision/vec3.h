
#pragma once;
#include <iostream>
using namespace std;

/// Minimal 3-dimensional vector abstraction
class vec3
{
	bool empty;
public:

	// Constructors
	vec3() : x(0), y(0), z(0)
	{
		empty = true;
	}

	vec3(double vx, double vy, double vz)
	{
		empty = false;

		x = vx;
		y = vy;
		z = vz;
	}

	bool isEmpty()
	{
		return empty;
	}

	vec3(const vec3& v)
	{
		empty = v.empty;

		x = v.x;
		y = v.y;
		z = v.z;
	}

	// Destructor
	~vec3() {}

	// A minimal set of vector operations
	vec3 operator * (double mult) // result = this * arg
	{
		return vec3(x * mult, y * mult, z * mult);
	}

	vec3 operator + (const vec3& v) // result = this + arg
	{
		return vec3(x + v.x, y + v.y, z + v.z);
	}

	vec3 operator - (const vec3& v) // result = this - arg
	{
		return vec3(x - v.x, y - v.y, z - v.z);
	}

	void print()
	{
		cout << "vec3 " << x << " " << y << " " << z << endl;
	}

	double x, y, z;
};
