#pragma once;

#include <opencv2/core.hpp>
#include <opencv2/imgproc.hpp>
#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <stack>
#include <map>
#include "Structs.cpp";
#include "Utility.cpp";
#include "ImgProc.cpp";

using namespace std;
using namespace cv;


class VeinHeights
{
	Mat veins;
	Mat centers;

	vector<vector<int>> result;

public:
	VeinHeights(Mat veins, Mat veinCenters) : veins(veins), centers(veinCenters)
	{

	}

	vector<vector<int>> getMap()
	{
		return result;
	}

	void calculate()
	{
		result = vector<vector<int>>(veins.rows, vector<int>());
		auto st1 = stack<pair<int, int>>();
		auto st2 = stack<pair<int, int>>();
		int kern = 1;

		for (int y = 0; y < result.size(); y++)
		{
			for (int x = 0; x < veins.cols; x++)
			{
				result[y].push_back(-1); 
			}
		}

		for (int y = 0; y < centers.rows; y++)
		{
			for (int x = 0; x < centers.cols; x++)
			{
				if (centers.at<uchar>(y, x) == 255)
				{
					result[y][x] = 0;
					st1.push(make_pair(x, y));
				}
			}
		}

		while (!st1.empty())
		{

			while (!st1.empty())
			{
				auto el = st1.top();
				st1.pop();

				for (int yO = -kern; yO <= kern; yO++)
				{
					for (int xO = -kern; xO <= kern; xO++)
					{
						if (xO == 0 && yO == 0)
						{
							continue;
						}

						auto neib = make_pair(el.first + xO, el.second + yO);

						if (ImgProc::inBoundsWithVal(veins, neib, 255) &&
							result[neib.second][neib.first] == -1)
						{
							result[neib.second][neib.first] = 1 + result[el.second][el.first];
							st2.push(neib);
						}
					}
				}

			}

			while (!st2.empty())
			{
				auto el = st2.top();
				st2.pop();

				for (int yO = -kern; yO <= kern; yO++)
				{
					for (int xO = -kern; xO <= kern; xO++)
					{
						if (xO == 0 && yO == 0)
						{
							continue;
						}

						auto neib = make_pair(el.first + xO, el.second + yO);

						if (ImgProc::inBoundsWithVal(veins, neib, 255) &&
							result[neib.second][neib.first] == -1)
						{
							result[neib.second][neib.first] = 1 + result[el.second][el.first];
							st1.push(neib);
						}
					}
				}

			}

		}

	}
};