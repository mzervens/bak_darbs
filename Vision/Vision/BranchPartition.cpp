#pragma once;
#include <opencv2/core.hpp>
#include <opencv2/imgproc.hpp>

#include <iostream>
#include <vector>
#include <set>

#include "vec2.h";
#include "ImgProc.cpp";

using namespace std;
using namespace cv;

class BranchPartition
{
	Mat veins;
	Mat veinC;
	int veinPixelVal;

	vector<vector<vec2<int>>> centerParts;

	void initCenterParts()
	{
		centerParts = vector<vector<vec2<int>>>();
	}

	vector<vec2<int>> unconstrainedNeighbs(Mat &img, vec2<int> point, int kernel = 1)
	{
		vector<vec2<int>> res = vector<vec2<int>>();
		for (int y = -kernel; y <= kernel; y++)
		{
			for (int x = -kernel; x <= kernel; x++)
			{
				vec2<int> pnt = vec2<int>(x, y) + point;
				if (ImgProc::inBoundsWithVal(img, pnt, veinPixelVal))
				{
					res.push_back(pnt);
				}
			}
		}

		return res;
	}

	vector<vec2<int>> neighbours(Mat &img, Mat &visited, vec2<int> point, set<pair<int, int>> ignore = set<pair<int, int>>(), int kernel = 1)
	{
		vector<vec2<int>> res = vector<vec2<int>>();


		for (int y = -kernel; y <= kernel; y++)
		{
			for (int x = -kernel; x <= kernel; x++)
			{
				vec2<int> pnt = vec2<int>(x, y) + point;
				if (ImgProc::inBoundsWithVal(img, pnt, veinPixelVal) && 
					visited.at<uchar>(pnt.y, pnt.x) == 0 &&
					pnt != point &&
					ignore.find(make_pair(pnt.x, pnt.y)) == ignore.end())
				{
					res.push_back(pnt);
				}
			}
		}

		return res;
	}

	vec2<int> iterateTillNewVeinCenterPixel(Mat &visited, vec2<int> origin)
	{
		vec2<int> res(-1, -1);
		bool brk = false;

		for (int y = origin.y; y < veinC.rows; y++)
		{
			for (int x = origin.x; x < veinC.cols; x++)
			{
				if (veinC.at<uchar>(y, x) == 0 || visited.at<uchar>(y, x) == 255)
				{
					continue;
				}

				res.x = x;
				res.y = y;
				visited.at<uchar>(y, x) = (uchar)255;

				brk = true;
				break;
			}

			if (brk)
				break;
		}

		return res;
	}

	bool pointInNeighbours(vec2<int> pnt, vector<vec2<int>> vec)
	{
		for (int i = 0; i < vec.size(); i++)
		{
			if (vec[i] == pnt)
			{
				return true;
			}
		}
		return false;
	}

public:
	/// Mat veins - binary leaf veins
	/// Mat veinCenter - center of veins 
	BranchPartition(Mat veins, Mat veinCenter) : veins(veins), veinC(veinCenter)
	{
		initCenterParts();
		veinPixelVal = 255;
	}

	void partitionCenter()
	{
		initCenterParts();
		Mat visited(veins.size(), CV_8UC1, Scalar(0));
		vector<vec2<int>> branchStart = vector<vec2<int>>(); // point starts for branches found when creating a branch
		vector<vec2<int>> branch = vector<vec2<int>>(); // current branch
		vec2<int> currDir(0, 0);
		vec2<int> iterationOrig(0, 0);
		auto ignorePixels = set<pair<int, int>>();

		vec2<int> brStart = iterateTillNewVeinCenterPixel(visited, iterationOrig);
		iterationOrig = brStart;

		if (brStart.x < 0)
		{
			return;
		}

		branch.push_back(brStart);

		while (true)
		{
			vector<vec2<int>> curNeigh = neighbours(veinC, visited, brStart, ignorePixels);// neighbours(Mat &img, Mat &visited, vec2<int> point, int kernel = 1)

			if (curNeigh.size() == 0) // branch has ended
			{
				centerParts.push_back(branch);
				branch = vector<vec2<int>>();
				ignorePixels = set<pair<int, int>>();
				if (branchStart.size() > 0)
				{
					brStart = branchStart[branchStart.size() - 1];
					currDir = vec2<int>(0, 0);

					branch.push_back(brStart);
					branchStart.pop_back();
				}
				else
				{
					brStart = iterateTillNewVeinCenterPixel(visited, iterationOrig);
					
					if (brStart.x < 0)
					{
						break;
					}

					currDir = vec2<int>(0, 0);
					visited.at<uchar>(brStart.y, brStart.x) = (uchar)255;
					branch.push_back(brStart);
				}
			}
			else if (curNeigh.size() == 1) // continue our branch 
			{
				currDir = curNeigh[0] - brStart;
				brStart = curNeigh[0];
				visited.at<uchar>(brStart.y, brStart.x) = (uchar)255;
				branch.push_back(brStart);
			}
			else // save current point as start for branches going from this point and find the appropriate point to continue this branch
			{
				vec2<int> apprNextPnt = brStart + currDir;
				branchStart.push_back(brStart);

				if (pointInNeighbours(apprNextPnt, curNeigh))
				{
					brStart = apprNextPnt;
					visited.at<uchar>(brStart.y, brStart.x) = (uchar)255;
					branch.push_back(brStart);
				}
				else // if the branch doesnt continue on this direction then just take first neighbour
				{
					currDir = curNeigh[0] - brStart;
					brStart = curNeigh[0];
					visited.at<uchar>(brStart.y, brStart.x) = (uchar)255;
					branch.push_back(brStart);
				}

				// ignore rest and their neighbours
				for (int i = 0; i < curNeigh.size(); i++)
				{
					if (curNeigh[i] == brStart)
					{
						continue;
					}

					vector<vec2<int>> nNeighbs = neighbours(veinC, visited, curNeigh[i]);
					nNeighbs.push_back(curNeigh[i]);

					for (int n = 0; n < nNeighbs.size(); n++)
					{
						pair<int, int> neighPr = make_pair(nNeighbs[n].x, nNeighbs[n].y);
						if (ignorePixels.find(neighPr) == ignorePixels.end())
						{
							ignorePixels.insert(neighPr);
						}
					}
				}
			}

		}

	}

	vector<Mat> printCenterPartitions()
	{
		auto res = vector<Mat>();
		

		for (int i = 0; i < centerParts.size(); i++)
		{
			Mat partImg = Mat::zeros(veinC.size(), CV_8UC3);
			auto color = vector<int>({ Utility::random(0, 256), Utility::random(0, 256), Utility::random(0, 256) });
			auto part = centerParts[i];
			for (int n = 0; n < part.size(); n++)
			{
				partImg.at<Vec3b>(part[n].y, part[n].x) = Vec3b(color[0], color[1], color[2]);
			}

			res.push_back(partImg);
		}

		return res;
	}


};
