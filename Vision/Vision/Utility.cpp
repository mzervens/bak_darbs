#pragma once;

#include <opencv2/core.hpp>
#include <opencv2/imgcodecs.hpp>
#include <opencv2/highgui.hpp>
#include <iostream>
#include <string>
#include <time.h>

using namespace std;
using namespace cv;

class Utility
{
public:
	static void displayImage(string path)
	{
		Mat img = imread(path.c_str(), IMREAD_COLOR);

		if (img.empty())
		{
			cout << "couldnt read the image " << path << endl;
		}

		namedWindow("Image", WINDOW_AUTOSIZE);
		imshow("Image", img);
		waitKey(0);
	}

	static void displayImage(Mat img)
	{
		namedWindow("Image", WINDOW_AUTOSIZE);
		imshow("Image", img);
		waitKey(0);
	}

	static void writeImage(Mat img, string path)
	{
		imwrite(path, img);
	}

	static int random(int min, int max) //range : [min, max)
	{
		static bool first = true;
		if (first)
		{
			srand(time(NULL)); //seeding for the first time only!
			first = false;
		}
		return min + rand() % (max - min);
	}


	static float pointDist(pair<int, int> p1, pair<int, int> p2)
	{
		float xDelta = p2.first - p1.first;
		float yDelta = p2.second - p1.second;

		return sqrtf((xDelta * xDelta) + (yDelta * yDelta));
	}

	static double pointDist3D(double x1, double y1, double z1,
		double x2, double y2, double z2)
	{
		double xDelta = x2 - x1;
		double yDelta = y2 - y1;
		double zDelta = z2 - z1;

		return sqrt((xDelta * xDelta) + (yDelta * yDelta) + (zDelta * zDelta));
	}

	static double dot3D(double x1, double y1, double z1,
		double x2, double y2, double z2)
	{
		double res;
		res = (x1 * x2) + (y1 * y2) + (z1 * z2);

		return res;
	}

	static double len3D(double x1, double y1, double z1)
	{
		return sqrt(((x1 * x1) + (y1 * y1) + (z1 * z1)));
	}

	static double angle3Dvecs(vector<double> vec1,
		vector<double> vec2)
	{
		return angle3Dvecs(vec1[0], vec1[1], vec1[2],
			vec2[0], vec2[1], vec2[2]);
	}

	static double angle3Dvecs(double x1, double y1, double z1,
		double x2, double y2, double z2)
	{
		double an = acos(dot3D(x1, y1, z1, x2, y2, z2) / (len3D(x1, y1, z1) * len3D(x2, y2, z2)));

		return  an * 180 / 3.14159265359;
	}


	static vector<double> vec3DSub(vector<double> vec1, vector<double> vec2)
	{
		return vector<double>({ vec2[0] - vec1[0], vec2[1] - vec1[1], vec2[2] - vec1[2] });
	}

};