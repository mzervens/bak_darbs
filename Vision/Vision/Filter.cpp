#pragma once;

#include <opencv2/core.hpp>
#include <opencv2/imgproc.hpp>
#include <iostream>
#include <string>
#include "ImgProc.cpp";

using namespace std;
using namespace cv;

class Filter
{
public:

	static double Otsus(Mat img)
	{
		Mat imC = img.clone();
		return threshold(img, imC, 0, 255, CV_THRESH_BINARY | CV_THRESH_OTSU);
	}

	static Mat DoG(Mat img, Size s1 = Size(1, 1), Size s2 = Size(3, 3))
	{
		Mat r1, r2;
		GaussianBlur(img, r1, s1, 0);
		GaussianBlur(img, r2, s2, 0);

		return (r1 - r2);
	}

	static Mat DoG_exp(Mat img, double smallSigma, double largeSigma, Size s1 = Size(251, 119), Size s2 = Size(251, 119))
	{
		Mat r1, r2;
		GaussianBlur(img, r1, s1, smallSigma, smallSigma);
		GaussianBlur(img, r2, s2, largeSigma, largeSigma);

		return (r1 - r2);
	}

	static Mat CannyWithOtsus(Mat greyImg, bool doBlur = true)
	{
		double maxThresh = Otsus(greyImg);
		double minThresh = maxThresh * 0.5;
		Mat res;

		if (doBlur)
			blur(greyImg, res, Size(3, 3));
		else
			res = greyImg;


		Canny(res, res, minThresh, maxThresh);

		return res;
	}

	static Mat CannyEdge(Mat img, int minTresh, int maxTresh, Size blurSize = Size(3, 3), bool isGray = false, bool skipBluring = false)
	{
		Mat gray, res;

		if (!isGray)
		{
			cvtColor(img, gray, COLOR_BGR2GRAY);
		}
		else
		{
			gray = img;
		}
		if (!skipBluring)
		{ 
			blur(gray, gray, blurSize);
		}

		Canny(gray, res, minTresh, maxTresh);

		return res;
	}

	static Mat mySobel(Mat greyIm, bool returnAngles = false)
	{
		Mat res(greyIm.size(), CV_8UC1, Scalar(0));
		Mat angles(greyIm.size(), CV_8UC1, Scalar(0));

		for (int y = 1; y < greyIm.rows - 1; y++)
		{
			for (int x = 1; x < greyIm.cols - 1; x++)
			{
				/*
				int xResp = - (3 * greyIm.at<uchar>(y - 1, x - 1)) + (3 * greyIm.at<uchar>(y - 1, x + 1))
				- (10 * greyIm.at<uchar>(y, x - 1)) + (10 * greyIm.at<uchar>(y, x + 1))
				- (3 * greyIm.at<uchar>(y + 1, x - 1)) + (3 * greyIm.at<uchar>(y + 1, x + 1));

				int yResp = - (3 * greyIm.at<uchar>(y - 1, x - 1)) + (3 * greyIm.at<uchar>(y + 1, x - 1))
				- (10 * greyIm.at<uchar>(y - 1, x)) + (10 * greyIm.at<uchar>(y + 1, x))
				- (3 * greyIm.at<uchar>(y - 1, x + 1)) + (3 * greyIm.at<uchar>(y + 1, x + 1));
				*/




				double xResp = -(greyIm.at<uchar>(y - 1, x - 1)) + (greyIm.at<uchar>(y - 1, x + 1))
					- (2 * greyIm.at<uchar>(y, x - 1)) + (2 * greyIm.at<uchar>(y, x + 1))
					- (greyIm.at<uchar>(y + 1, x - 1)) + (greyIm.at<uchar>(y + 1, x + 1));

				double yResp = -(greyIm.at<uchar>(y - 1, x - 1)) + (greyIm.at<uchar>(y + 1, x - 1))
					- (2 * greyIm.at<uchar>(y - 1, x)) + (2 * greyIm.at<uchar>(y + 1, x))
					- (greyIm.at<uchar>(y - 1, x + 1)) + (greyIm.at<uchar>(y + 1, x + 1));


				double resp = sqrt((xResp * xResp) + (yResp * yResp));
				res.at<uchar>(y, x) = std::min(resp, (double)255);

				double edgedirection = (atan2(yResp, xResp)) * (180 / 3.14159265);

				while (edgedirection < 0)
				{
					edgedirection += 360;
				}

				while (edgedirection > 360)
				{
					edgedirection -= 360;
				}

				if (edgedirection > 337.5 || edgedirection <= 22.5)
				{
					edgedirection = 0;
				}
				else if (edgedirection > 22.5 && edgedirection <= 67.5)
				{
					edgedirection = 45;
				}
				else if (edgedirection > 67.5 && edgedirection <= 112.5)
				{
					edgedirection = 90;
				}
				else if (edgedirection > 112.5 && edgedirection <= 157.5)
				{
					edgedirection = 135;
				}
				else if (edgedirection > 157.5 && edgedirection <= 202.5)
				{
					edgedirection = 0;
				}
				else if (edgedirection > 202.5 && edgedirection <= 247.5)
				{
					edgedirection = 45;
				}
				else if (edgedirection > 247.5 && edgedirection <= 292.5)
				{
					edgedirection = 90;
				}
				else if (edgedirection > 292.5 && edgedirection <= 337.5)
				{
					edgedirection = 135;
				}


				angles.at<uchar>(y, x) = edgedirection;
			}
		}

		if (returnAngles)
		{
			return angles;
		}

		return res;
	}

	static Mat NonMaxSupr(Mat SobelIn, bool doDoubleThresh = false)
	{
		Mat sobelRes = mySobel(SobelIn);
		Mat angles = mySobel(SobelIn, true);
		Mat thinnedRes = sobelRes.clone();
		//return thinnedRes;
		for (int y = 0; y < thinnedRes.rows; y++)
		{
			for (int x = 0; x < thinnedRes.cols; x++)
			{
				if (thinnedRes.at<uchar>(y, x) > 0)
				{
					thinnedRes.at<uchar>(y, x) = 255;
				}
			}
		}

		double maxThresh = Otsus(SobelIn);
		double minThresh = maxThresh * 0.5;

		for (int y = 1; y < sobelRes.rows - 1; y++)
		{
			for (int x = 1; x < sobelRes.cols - 1; x++)
			{
				/*
				if the rounded gradient angle is 0� (i.e. the edge is in the north�south direction) the point will be considered to be on the edge if its gradient magnitude is greater than the magnitudes at pixels in the east and west directions,
				if the rounded gradient angle is 90� (i.e. the edge is in the east�west direction) the point will be considered to be on the edge if its gradient magnitude is greater than the magnitudes at pixels in the north and south directions,
				if the rounded gradient angle is 135� (i.e. the edge is in the northeast�southwest direction) the point will be considered to be on the edge if its gradient magnitude is greater than the magnitudes at pixels in the north west and south east directions,
				if the rounded gradient angle is 45� (i.e. the edge is in the north west�south east direction) the point will be considered to be on the edge if its gradient magnitude is greater than the magnitudes at pixels in the north east and south west directions.
				*/
				double angle = angles.at<uchar>(y, x);
				if (angle == 0)
				{
					auto grad1 = sobelRes.at<uchar>(y, x - 1);
					auto grad = sobelRes.at<uchar>(y, x);
					auto grad2 = sobelRes.at<uchar>(y, x + 1);

					if (grad < grad1 || grad < grad2)
					{
						thinnedRes.at<uchar>(y, x) = 0;
					}

				}

				if (angle == 90)
				{
					auto grad1 = sobelRes.at<uchar>(y - 1, x);
					auto grad = sobelRes.at<uchar>(y, x);
					auto grad2 = sobelRes.at<uchar>(y + 1, x);

					if (grad < grad1 || grad < grad2)
					{
						thinnedRes.at<uchar>(y, x) = 0;
					}

				}


				if (angle == 135)
				{
					auto grad1 = sobelRes.at<uchar>(y - 1, x + 1);
					auto grad = sobelRes.at<uchar>(y, x);
					auto grad2 = sobelRes.at<uchar>(y + 1, x - 1);

					if (grad < grad1 || grad < grad2)
					{
						thinnedRes.at<uchar>(y, x) = 0;
					}
				}



				if (angle == 45)
				{
					auto grad1 = sobelRes.at<uchar>(y - 1, x - 1);
					auto grad = sobelRes.at<uchar>(y, x);
					auto grad2 = sobelRes.at<uchar>(y + 1, x + 1);

					if (grad < grad1 || grad < grad2)
					{
						thinnedRes.at<uchar>(y, x) = 0;
					}



				}

				/*
				if (doDoubleThresh)
				{
				if (sobelRes.at<uchar>(y, x) < minThresh)
				{
				thinnedRes.at<uchar>(y, x) = 0;
				}
				}
				*/
			}
		}

		// drop pixels below minimum threshold

		for (int y = 1; y < sobelRes.rows - 1; y++)
		{
			for (int x = 1; x < sobelRes.cols - 1; x++)
			{
				if (doDoubleThresh)
				{
					if (sobelRes.at<uchar>(y, x) < minThresh)
					{
						thinnedRes.at<uchar>(y, x) = 0;
					}
				}
			}
		}


		Mat visited(thinnedRes.size(), CV_8UC1, Scalar(0));
		/*
		if (doDoubleThresh)
		{
		for (int y = 1; y < sobelRes.rows - 1; y++)
		{
		for (int x = 1; x < sobelRes.cols - 1; x++)
		{
		auto grad = sobelRes.at<uchar>(y, x);
		if (grad >= maxThresh)
		{
		continue;
		}

		if (grad < maxThresh && grad >= minThresh && thinnedRes.at<uchar>(y, x) == 255)
		{
		auto st = stack<pair<vec2<int>, int>>();
		st.push(make_pair(vec2<int>(x, y), 0));
		int kern = 1;
		bool connected = false;

		visited = 0;

		int maxDist = 8;

		while (!st.empty())
		{
		auto el = st.top();
		st.pop();

		if (sobelRes.at<uchar>(el.first.y, el.first.x) >= maxThresh)
		{
		connected = true;
		break;
		}

		for (int yO = -kern; yO <= kern; yO++)
		{
		for (int xO = -kern; xO <= kern; xO++)
		{
		if (el.second < maxDist &&
		ImgProc::inBoundsWithVal(thinnedRes, vec2<int>(el.first.x + xO, el.first.y + yO), 255) &&
		visited.at<uchar>(el.first.y + yO, el.first.x + xO) == 0)
		{
		visited.at<uchar>(el.first.y + yO, el.first.x + xO) = 255;
		st.push(make_pair(vec2<int>(el.first.x + xO, el.first.y + yO), 1 + el.second));
		}
		}
		}

		}

		if (!connected)
		{
		thinnedRes.at<uchar>(y, x) = 0;
		}
		}
		}
		}

		}
		*/
		return thinnedRes;
	}

	static Mat FreivaldRidgeImg(Mat imgGrey, bool doThresh = false)
	{
		Mat result = imgGrey.clone();
		Mat degs(result.size(), CV_8UC1, Scalar(0));

		for (int i = 1; i < imgGrey.rows - 1; i++) // skipping image borders since the ridge detection works if the pixel is enclodes by 8 other pixels
		{
			for (int n = 1; n < imgGrey.cols - 1; n++)
			{
				int value = FreivaldRidgePixel(n, i, imgGrey);
				result.at<uchar>(i, n) = std::min(value, 255);
			}
		}

		for (int i = 0; i < imgGrey.cols; i++)
		{
			result.at<uchar>(0, i) = 0;
			result.at<uchar>(imgGrey.rows - 1, i) = 0;
		}

		for (int i = 0; i < imgGrey.rows; i++)
		{
			result.at<uchar>(i, 0) = 0;
			result.at<uchar>(i, imgGrey.cols - 1) = 0;
		}

		if (doThresh)
		{
			for (int i = 1; i < imgGrey.rows - 1; i++) // skipping image borders since the ridge detection works if the pixel is enclodes by 8 other pixels
			{
				for (int n = 1; n < imgGrey.cols - 1; n++)
				{
					int de = FreivaldRidgePixel(n, i, imgGrey, true);
					degs.at<uchar>(i, n) = de;
				}
			}

			Mat edgeAngl = mySobel(imgGrey, true);
			Mat edges = NonMaxSupr(imgGrey, true);

			Utility::displayImage(edges);

			int curKer = 1;
			int maxKer = 8;
			bool drop = true;
			bool end = false;

			for (int y = 0; y < result.rows; y++)
			{
				for (int x = 0; x < result.cols; x++)
				{
					if (result.at<uchar>(y, x) == 0)
					{
						continue;
					}


					while (curKer <= maxKer)
					{
						for (int yO = -curKer; yO <= curKer; yO++)
						{
							for (int xO = -curKer; xO <= curKer; xO++)
							{
								auto pos = vec2<int>(x + xO, y + yO);
								if (ImgProc::inBoundsWithVal(edges, pos, 255))
								{
									if (edgeAngl.at<uchar>(pos.y, pos.x) == degs.at<uchar>(y, x))
									{
										drop = false;
										end = true;
										break;
									}
									
									else
									{
										drop = true;
										end = true;
										break;
									}
									
									
								}
							}

							if (end)
							{
								break;
							}
						}

						if (end)
						{
							break;
						}

						curKer++;
					}


					if (drop)
					{
						result.at<uchar>(y, x) = 0;
					}

					curKer = 1;
					drop = true;
					end = false;
				}
			}
		}

		return result;
	}

	static int FreivaldRidgePixel(int x, int y, Mat img, bool getDeg = false)
	{
		int f = img.at<uchar>(y, x); // value of the central pixel
		int fpp[4]; // array for second derivatives for each direction
		int fpp2[4];
		int fhh, fhhI, fhh_tmp, fhhI_tmp;
		int fhh2, fhh2_tmp;
		// claculate magnitudes of directional second derivatives
		// find the maximum one

		//horizontal
		
		fpp[0] = (f - img.at<uchar>(y, x + 1) - img.at<uchar>(y, x - 1));
		// diagonal \ 
		fpp[1] = (f -img.at<uchar>(y + 1, x + 1) - img.at<uchar>(y - 1, x - 1)) / 2;
		// vertical
		fpp[2] = (f - img.at<uchar>(y + 1, x) - img.at<uchar>(y - 1, x));
		// diagonal /
		fpp[3] = (f -img.at<uchar>(y + 1, x - 1) - img.at<uchar>(y - 1, x + 1)) / 2;
		

		fpp2[0] = 2*(f - img.at<uchar>(y, x + 1) - img.at<uchar>(y, x - 1));
		// diagonal \ 
		fpp2[1] = ( - img.at<uchar>(y + 1, x + 1) - img.at<uchar>(y - 1, x - 1));
		// vertical
		fpp2[2] = 2*(f - img.at<uchar>(y + 1, x) - img.at<uchar>(y - 1, x));
		// diagonal /
		fpp2[3] = (- img.at<uchar>(y + 1, x - 1) - img.at<uchar>(y - 1, x + 1));

		if (fpp[1]>fpp[0])
		{
			fhh = fpp[1];
			fhh2 = fpp2[1];
			fhhI = 1;
		}
		else
		{
			fhh = fpp[0];
			fhh2 = fpp2[0];
			fhhI = 0;
		}

		if (fpp[2]>fpp[3])
		{
			fhh_tmp = fpp[2];
			fhh2_tmp = fpp2[2];
			fhhI_tmp = 2;
		}
		else
		{
			fhh_tmp = fpp[3];
			fhh2_tmp = fpp2[3];
			fhhI_tmp = 3;
		}


		if (fhh_tmp > fhh)
		{
			fhh = fhh_tmp;
			fhh2 = fhh2_tmp;
			fhhI = fhhI_tmp;
		}
		fhh += 2 * f; // now this is the maximum second derivative
		fhh2 += 2 * f;
		// second derivative in the orthogonal direction
		int fgg = fpp[(fhhI + 2) & 3] + 2 * f;

		pair<int, int> f1Ofs[4] = { pair<int, int>(0, 1), pair<int, int>(1, 1), pair<int, int>(1, 0), pair<int, int>(1, -1) }; // relative offsets of reighbor pixels
		//get the pixels in the direction of maximum second derivative
		pair<int, int> ofs = f1Ofs[fhhI];

		if (getDeg)
		{
			if (fhhI == 0)
			{
				return 0;
			}
			
			if (fhhI == 1)
			{
				return 135;
			}

			if (fhhI == 2)
			{
				return 90;
			}

			if (fhhI == 3)
			{
				return 45;
			}
		}


		// ridge is when both neighbors in this direction is smaller than the central pixel
		// curvarute in the found direction should be smaller than in orhogonal direction
		if (img.at<uchar>(y + ofs.first, x + ofs.second) < f &&
			img.at<uchar>(y - ofs.first, x - ofs.second) < f && abs(fgg) <= fhh)
		{
			return fhh;// the ridge strength is the magnitude of the second derivative in the direction perpendicular to ridge
		}

		return 0; // no ridge at this point
	}
};