#pragma once;

#include <opencv2/core.hpp>
#include <opencv2/imgproc.hpp>
#include <iostream>
#include <string>

using namespace std;
using namespace cv;

namespace Thinning
{
	class ZhangSuen2
	{
	public:

		static bool HasOne01Pat(Mat &img, int x, int y)
		{
			int count = 0;
			int one = 255;

			if (img.at<uchar>(y - 1, x) == 0 && img.at<uchar>(y - 1, x + 1) == one)
			{
				count++;
			}
			if (img.at<uchar>(y - 1, x + 1) == 0 && img.at<uchar>(y, x + 1) == one)
			{
				count++;
			}
			if (img.at<uchar>(y, x + 1) == 0 && img.at<uchar>(y + 1, x + 1) == one)
			{
				count++;
			}
			if (img.at<uchar>(y + 1, x + 1) == 0 && img.at<uchar>(y + 1, x) == one)
			{
				count++;
			}
			 if (img.at<uchar>(y + 1, x) == 0 && img.at<uchar>(y + 1, x - 1) == one)
			{
				count++;
			}
			 if (img.at<uchar>(y + 1, x - 1) == 0 && img.at<uchar>(y, x - 1) == one)
			{
				count++;
			}
			if (img.at<uchar>(y, x - 1) == 0 && img.at<uchar>(y - 1, x - 1) == one)
			{
				count++;
			}
			 if (img.at<uchar>(y - 1, x - 1) == 0 && img.at<uchar>(y - 1, x) == one)
			{
				count++;
			}


			if (count == 1)
			{
				return true;
			}

			return false;

		}

		static int nonZeroNeibs(Mat &img, int x, int y)
		{
			int kern = 1;
			int neibs = 0;

			for (int yO = -kern; yO <= kern; yO++)
			{
				for (int xO = -kern; xO <= kern; xO++)
				{
					if (yO == 0 && xO == 0)
					{
						continue;
					}

					if (img.at<uchar>(y + yO, x + xO) == 255)
					{
						neibs++;
					}
				}
			}

			return neibs;
		}

		static int P246(Mat &img, int x, int y)
		{
			if (img.at<uchar>(y - 1, x) == 0 ||
				img.at<uchar>(y, x + 1) == 0 ||
				img.at<uchar>(y + 1, x) == 0)
			{
				return 0;
			}

			return 1;
		}

		static int P248(Mat &img, int x, int y)
		{
			if (img.at<uchar>(y - 1, x) == 0 ||
				img.at<uchar>(y, x + 1) == 0 ||
				img.at<uchar>(y, x - 1) == 0)
			{
				return 0;
			}

			return 1;
		}

		static int P268(Mat &img, int x, int y)
		{
			if (img.at<uchar>(y - 1, x) == 0 ||
				img.at<uchar>(y + 1, x) == 0 ||
				img.at<uchar>(y, x - 1) == 0)
			{
				return 0;
			}

			return 1;
		}

		static int P468(Mat &img, int x, int y)
		{
			if (img.at<uchar>(y, x + 1) == 0 ||
				img.at<uchar>(y + 1, x) == 0 ||
				img.at<uchar>(y, x - 1) == 0)
			{
				return 0;
			}

			return 1;
		}

		static Mat iter1(Mat &img)
		{
			Mat res = img.clone();

			for (int y = 1; y < img.rows - 1; y++)
			{
				for (int x = 1; x < img.rows - 1; x++)
				{
					if (img.at<uchar>(y, x) == 0)
					{
						continue;
					}

					int neibs = nonZeroNeibs(img, x, y);
					bool hasOne = HasOne01Pat(img, x, y);
					int p246 = P246(img, x, y);
					int p468 = P468(img, x, y);

					if (neibs >= 2 && neibs <= 6 && hasOne && p246 == 0 && p468 == 0)
					{
						res.at<uchar>(y, x) = 0;
					}
				}
			}

			return res;
		}

		static Mat iter2(Mat &img)
		{
			Mat res = img.clone();

			for (int y = 1; y < img.rows - 1; y++)
			{
				for (int x = 1; x < img.rows - 1; x++)
				{
					if (img.at<uchar>(y, x) == 0)
					{
						continue;
					}

					int neibs = nonZeroNeibs(img, x, y);
					bool hasOne = HasOne01Pat(img, x, y);
					int p248 = P248(img, x, y);
					int p268 = P268(img, x, y);

					if (neibs >= 2 && neibs <= 6 && hasOne && p248 == 0 && p268 == 0)
					{
						res.at<uchar>(y, x) = 0;
					}
				}
			}

			return res;
		}


		static Mat runAlgo(Mat img)
		{
			Mat prev = img.clone();
			Mat cur = img.clone();
			Mat dif;

			while (true)
			{
				cur = iter1(cur);
				cur = iter2(cur);

				absdiff(cur, prev, dif);
				cur.copyTo(prev);

				if (countNonZero(dif) == 0)
				{
					break;
				}
				
			}

			return cur;
		}


	};


}