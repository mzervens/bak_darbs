#pragma once;
#include <fstream>
#include <iostream>
#include <string>
#include <map>
#include <vector>
#include "vec3.h"

using namespace std;


class TaubinSmooth
{
	vector<vec3> verts;
	vector<vec3> allVerts;
	vector<int> faces;
	map<int, vector<int>> vertNeibs;
	vector<int> mapToUniqVert;

	map<string, int> uniqVerts;

	vector<int> initialF;


	double lambda;
	double mu;
	int N;

public:
	TaubinSmooth(string objF, double lambda, double mu, int N) : lambda(lambda), mu(mu), N(N)
	{
		readData(objF);
	}


	void smooth()
	{
		for (int i = 0; i < N; i++)
		{
			smoothIter(true);
			smoothIter(false);

			cout << "finished iter " << i << endl;
		}
	}

	void smoothIter(bool iterType)
	{
	//	cout << "smooth iter" << endl;
		auto vertAvgs = vector<vec3>();


		for (int i = 0; i < verts.size(); i++)
		{
			if (verts[i].isEmpty())
			{
				vertAvgs.push_back(vec3()); // pad
				continue;
			}

			auto curVert = verts[i];
			auto neibs = vertNeibs[i];
			vec3 vecAvg(0, 0, 0);
			double w = 1.0 / neibs.size();

			for (int n = 0; n < neibs.size(); n++)
			{
				vecAvg = vecAvg + ((verts[neibs[n]] - curVert) * w);
			}

			vertAvgs.push_back(vecAvg);
		}

	//	cout << "here " << vertAvgs.size() << endl;
		double mulConst;
		if (iterType)
		{
			mulConst = lambda;
		}
		else
		{
			mulConst = mu;
		}

		for (int i = 0; i < verts.size(); i++)
		{
			if (verts[i].isEmpty())
			{
				continue;
			}

			verts[i] = verts[i] + (vertAvgs[i] * mulConst);
		}
	}


	void readData(string objF)
	{
		fstream f;
		f.open(objF, ios::in);
		string line;
		char c;
		int vertI = 1;


		verts = vector<vec3>(); // unique
		allVerts = vector<vec3>();

		faces = vector<int>();
		initialF = vector<int>();
		mapToUniqVert = vector<int>();
		vertNeibs = map<int, vector<int>>();

		uniqVerts = map<string, int>();

		f >> c;

		while (!f.eof())
		{

			if (c == 'v')
			{
				double x, y, z;
				string key;

				f >> x;
				f >> y;
				f >> z;
				key = to_string(x) + " " + to_string(y) + " " + to_string(z);

				allVerts.push_back(vec3(x, y, z));

				if (uniqVerts.find(key) == uniqVerts.end())
				{
					verts.push_back(vec3(x, y, z));
					vertNeibs.insert(make_pair(verts.size() - 1, vector<int>()));
					uniqVerts.insert(make_pair(key, verts.size() - 1));
					mapToUniqVert.push_back(-1);
				}
				else
				{
					verts.push_back(vec3()); // pad with empty data
					mapToUniqVert.push_back(uniqVerts[key]);

				}

				vertI++;
			}

			else if (c == 'f') // asume triangles and face list after verts
			{
				int v1, v2, v3;
				
				f >> v1;
				f >> v2;
				f >> v3;

				v1 -= 1;
				v2 -= 1;
				v3 -= 1;

				initialF.push_back(v1);
				initialF.push_back(v2);
				initialF.push_back(v3);

				if (mapToUniqVert[v1] != -1)
				{
					v1 = mapToUniqVert[v1];
				}

				if (mapToUniqVert[v2] != -1)
				{
					v2 = mapToUniqVert[v2];
				}

				if (mapToUniqVert[v3] != -1)
				{
					v3 = mapToUniqVert[v3];
				}

				faces.push_back(v1);
				faces.push_back(v2);
				faces.push_back(v3);

			}
			else
			{
				std::getline(f, line);
			}

			f >> c;
			//cout << line << " tt" << endl;
		}

		for (int n = 0; n < faces.size(); n += 3)
		{
			int v1, v2, v3;
			v1 = faces[n];
			v2 = faces[n + 1];
			v3 = faces[n + 2];

				int curV;
				auto vert = vector<int>({ v1, v2, v3 });

				for (int i = 0; i < 3; i++)
				{
					curV = vert[i];
					for (int n = 0; n < vert.size(); n++)
					{
						if (vert[n] == curV)
						{
							continue;
						}
						bool noDupe = true;

						for (int j = 0; j < vertNeibs[curV].size(); j++)
						{
							if (vertNeibs[curV][j] == vert[n])
							{
								noDupe = false;
							}
						}

						if (noDupe)
						{
							vertNeibs[curV].push_back(vert[n]);
						}
					}

				}
		}

		/*
		for (int i = 0; i < vertNeibs.size(); i++)
		{
			cout << (i + 1) << " ";
			for (int n = 0; n < vertNeibs[i].size(); n++)
			{
				cout << (vertNeibs[i][n] + 1) << " ";
			}
			cout << endl;
		}
		*/
		cout << "uniq verts size " << verts.size() << endl;
	//	int rrrr;
	//	cin >> rrrr;
		f.close();
	}

	void writeDataUniq(string objF)
	{
		cout << "writing data " << endl;
		fstream f;
		f.open(objF, ios::out);

		for (int i = 0; i < verts.size(); i++)
		{
			if (verts[i].isEmpty())
			{
				continue;
			}
			f << "v " << verts[i].x << " " << verts[i].y << " " << verts[i].z << endl;
		}

		for (int i = 0; i < faces.size(); i += 3)
		{
			f << "f ";

			int ofs = 1;
			while (verts[faces[i] - ofs].isEmpty())
			{
				ofs++;
			}

			f << (faces[i] + 1 - (ofs - 1)) << " ";


			ofs = 1;
			while (verts[faces[i + 1] - ofs].isEmpty())
			{
				ofs++;
			}

			f << (faces[i + 1] + 1 - (ofs - 1)) << " ";


			ofs = 1;
			while (verts[faces[i + 2] - ofs].isEmpty())
			{
				ofs++;
			}

			f << (faces[i + 2] + 1 - (ofs - 1)) << endl;

			//<< (faces[i] + 1) << " " << (faces[i + 1] + 1) << " " << (faces[i + 2] + 1) << endl;
		}


		f.close();
	}

	void writeData(string objF)
	{
		cout << "writing data " << endl;
		fstream f;
		f.open(objF, ios::out);
	
		for (int i = 0; i < allVerts.size(); i++)
		{
			auto vert = allVerts[i];
			string key = to_string(vert.x) + " " + to_string(vert.y) + " " + to_string(vert.z);

			f << "v " << verts[uniqVerts[key]].x << " " << verts[uniqVerts[key]].y << " " << verts[uniqVerts[key]].z << endl;
		}


		for (int i = 0; i < initialF.size(); i += 3)
		{

			f << "f " << (initialF[i] + 1) << " " << (initialF[i + 1] + 1) << " " << (initialF[i + 2] + 1) << endl;
		}

		f.close();
	}
};