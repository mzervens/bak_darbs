#pragma once
#include <opencv2/core.hpp>
#include <opencv2/imgproc.hpp>
#include <iostream>
#include <vector>
using namespace std;
using namespace cv;

class SkeletStruct
{
public:
	uint x;
	uint y;
	bool marked;

	uint hasNeighbours;
	vector<SkeletStruct*> neighbours;

	int topLeft, top, topRight,
		left, right,
		botLeft, bot, botRight;

	SkeletStruct()
	{
		x = y = -1;
		marked = false;
		hasNeighbours = 0;
		neighbours = vector<SkeletStruct*>(8);
		for (int i = 0; i < neighbours.size(); i++)
		{
			neighbours[i] = NULL;
		}

		topLeft = 0;
		top = 1;
		topRight = 2;
		left = 3;
		right = 4;
		botLeft = 5;
		bot = 6;
		botRight = 7;
	}

	SkeletStruct(int x, int y) : SkeletStruct()
	{
		this->x = x;
		this->y = y;
	}

	SkeletStruct * getNeighbour(int offset)
	{
		if (offset >= hasNeighbours)
		{
			return NULL;
		}

		for (int i = 0; i < neighbours.size(); i++)
		{
			if (neighbours[i] != NULL)
			{
				if (offset <= 0)
				{
					return neighbours[i];
				}
				else
				{
					offset--;
				}
			}
		}

		return NULL;
	}

	void addNeighbour(SkeletStruct * neighbour)
	{

		if (neighbour->x < x) // left colum
		{
			if (neighbour->y < y) // top left
			{
				neighbours[topLeft] = neighbour;
			}
			else if (neighbour->y == y) // left
			{
				neighbours[left] = neighbour;
			}
			else // bot left
			{
				neighbours[botLeft] = neighbour;
			}
		}
		else if (neighbour->x > x) // right colum
		{
			if (neighbour->y < y) // top right
			{
				neighbours[topRight] = neighbour;
			}
			else if (neighbour->y == y) // right
			{
				neighbours[right] = neighbour;
			}
			else // bot right
			{
				neighbours[botRight] = neighbour;
			}

		}
		else // middle colum
		{
			if (neighbour->y < y) // top
			{
				neighbours[top] = neighbour;
			}
			else // bot
			{
				neighbours[bot] = neighbour;
			}
		}

		hasNeighbours++;
	}
};
