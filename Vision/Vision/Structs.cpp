#pragma once;
#include <iostream>
using namespace std;

template<typename T1, typename T2, typename T3>
class Triple
{
	bool empty;

public:
	Triple(T1 fst, T2 scnd, T3 thrd) 
	  :	fst(fst),
		scnd(scnd),
		thrd(thrd)
	{
		empty = false;
	}

	Triple()
	{
		empty = true;
	}

	bool isEmpty()
	{
		return !empty;
	}

	T1 fst;
	T2 scnd;
	T3 thrd;
};