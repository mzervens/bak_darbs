#pragma once;

#include <opencv2/core.hpp>
#include <opencv2/imgproc.hpp>
#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <stack>
#include <map>
#include "Structs.cpp";
#include "Utility.cpp";
#include "SkeletStruct.cpp";
#include "ImgHeightMap.cpp";


using namespace std;
using namespace cv;

enum PIXELDIRECTIONS
{
	LEFT,
	RIGHT,
	TOP,
	BOT,
	TOPLEFT,
	TOPRIGHT,
	BOTLEFT,
	BOTRIGHT
};

class ThreeDMap
{
	Mat inputBin;
	ImgHeightMap res;
	

	double diagonalConst = sqrt(2);
	double normalConst = 1;

	vector<vector<SkeletStruct*>> linkedSkelet = vector<vector<SkeletStruct*>>();

	bool validPosition(int x, int y)
	{
		if ((y < linkedSkelet.size() && y >= 0) && (x < linkedSkelet[y].size() && x >= 0))
		{
			return true;
		}
		return false;
	}

	bool updHeightIfValid(int x, int y, int parX, int parY, double height)
	{
	//	Utility::displayImage(inputBin);
		if (validPosition(x, y) && !belongsToSkelet(x, y) && inputBin.at<uchar>(y, x) != 0)
		{
			res.updatePixelHeight(x, y, parX, parY, height);
			return true;
		}
		return false;
	}

	void resetLinkSkelMarks()
	{
		for (int i = 0; i < linkedSkelet.size(); i++)
		{
			for (int n = 0; n < linkedSkelet[i].size(); n++)
			{
				if (linkedSkelet[i][n] != NULL)
				{
					linkedSkelet[i][n]->marked = false;
				}
			}
		}
	}

	SkeletStruct * getFirstUnmarked()
	{
		for (int i = 0; i < linkedSkelet.size(); i++)
		{
			for (int n = 0; n < linkedSkelet[i].size(); n++)
			{
				if (linkedSkelet[i][n] != NULL && !linkedSkelet[i][n]->marked)
				{
					return linkedSkelet[i][n];
				}
			}
		}

		return NULL;
	}

	/**
	 *	Not safe function, provide valid parameters
	 */
	bool belongsToSkelet(int x, int y)
	{
		if (linkedSkelet[y][x] != NULL)
		{
			return true;
		}
		return false;
	}

	SkeletStruct * skeletStructBuilder(int x, int y)
	{
		SkeletStruct * str;
		if (linkedSkelet[y][x] == NULL)
		{
			str = new SkeletStruct(x, y);
			linkedSkelet[y][x] = str;
		}

		return linkedSkelet[y][x];
	}

	void initSkeletLinkContainer(int rows, int cols)
	{
		linkedSkelet = vector<vector<SkeletStruct*>>(rows, vector<SkeletStruct*>(cols));
		for (int i = 0; i < rows; i++)
		{
			for (int n = 0; n < cols; n++)
			{
				linkedSkelet[i][n] = NULL;
			}
		}
	}

	void linkSkelet(Mat skeleton)
	{
		initSkeletLinkContainer(skeleton.rows, skeleton.cols);

		for (int i = 0; i < skeleton.rows; i++)
		{
			for (int n = 0; n < skeleton.cols; n++)
			{
				if (skeleton.at<uchar>(i, n) == 0)
				{
					continue;
				}

				SkeletStruct * skelStrct = skeletStructBuilder(n, i);

				for (int j = -1; j < 2; j++)
				{
					for (int k = -1; k < 2; k++)
					{
						if ((j + i) < 0 || (j + i) >= skeleton.rows || // skip if outside bounds or the neighbour is empty
							(k + n) < 0 || (k + n) >= skeleton.cols ||
							skeleton.at<uchar>(j + i, k + n) == 0)
						{
							continue;
						}

						if (j == 0 && k == 0) // this is current pixel
						{
							continue;
						}

						SkeletStruct * neighbour = skeletStructBuilder(n + k, i + j);
						skelStrct->addNeighbour(neighbour);
					}
				}
			}
		}
	}

public:
	ThreeDMap()
	{

	}

	ImgHeightMap * getResultMap()
	{
		return &res;
	}

	void calcDistUsingKernels(Mat binImg, Mat skeleton)
	{
		res = ImgHeightMap(binImg.rows, binImg.cols);
		inputBin = binImg;
		linkSkelet(skeleton);

		SkeletStruct * el = getFirstUnmarked();
		stack<SkeletStruct*> st;

		if (el == NULL)
		{
			return;
		}

		st.push(el);

		while (!st.empty())
		{
			el = st.top();
			st.pop();

			res.updatePixelHeight(el->x, el->y, el->x, el->y, 0.0);// zero for skeleton pixels
			el->marked = true;


			int kernSize = 1;
			bool firstCondMet = false;

			while (true)
			{
				int hitCount = 0;
				int pixelsProcessed = 0;

				for (int i = -kernSize; i <= kernSize; i++)
				{
					for (int n = -kernSize; n <= kernSize; n++)
					{
						int y = i + el->y;
						int x = n + el->x;

						if (updHeightIfValid(x, y, el->x, el->y, kernSize))
						{
							hitCount++;
						}

						pixelsProcessed++;

					}

				}

				kernSize++;
				double perc = (double)hitCount / pixelsProcessed;
				
				if (perc <= 0.5)
				{
					if (firstCondMet)
					{
						break;
					}
					else
					{
						firstCondMet = true;
					}
				}
				else
				{
					firstCondMet = false;
				}

			}

			int gotNeighbours = 0;
			SkeletStruct * neighbour;
			while ((neighbour = el->getNeighbour(gotNeighbours)) != NULL)
			{
				if (!neighbour->marked)
				{
					st.push(neighbour);
				}
				gotNeighbours++;
			}

			if (st.empty())
			{
				neighbour = getFirstUnmarked();
				if (neighbour != NULL)
				{
					st.push(neighbour);
				}
			}


		}
	}

	void gen(Mat binImg, Mat skeleton)
	{
		res = ImgHeightMap(binImg.rows, binImg.cols);
		inputBin = binImg;
		linkSkelet(skeleton);

		SkeletStruct * el = getFirstUnmarked();
		stack<SkeletStruct*> st;

		if (el == NULL)
		{
			return;
		}

		st.push(el);

		while (!st.empty())
		{
			el = st.top();
			st.pop();

			res.updatePixelHeight(el->x, el->y, el->x, el->y, 0.0);// zero for skeleton pixels
			el->marked = true;

			int offsHor = 1;
			int offsVer = 1;
			int xHor = el->x;
			int yHor = el->y;

			//horizontal
			while (updHeightIfValid(xHor + offsHor, yHor, el->x, el->y, offsHor))
			{
				offsHor++;
			}
			offsHor = 1;

			while (updHeightIfValid(xHor - offsHor, yHor, el->x, el->y, offsHor))
			{
				offsHor++;
			}
			offsHor = 1;

			//vertical
			while (updHeightIfValid(xHor, yHor + offsVer, el->x, el->y, offsVer))
			{
				offsVer++;
			}
			offsVer = 1;

			while (updHeightIfValid(xHor, yHor - offsVer, el->x, el->y, offsVer))
			{
				offsVer++;
			}
			offsVer = 1;

			//diagonal \ 
			while (updHeightIfValid(xHor + offsHor, yHor + offsHor, el->x, el->y, offsHor * diagonalConst))
			{
				offsHor++;
				offsVer++;
			}
			offsHor = 1;
			offsVer = 1;

			while (updHeightIfValid(xHor - offsHor, yHor - offsVer, el->x, el->y, offsHor * diagonalConst))
			{
				offsHor++;
				offsVer++;
			}
			offsHor = 1;
			offsVer = 1;

			//diagonal /
			while (updHeightIfValid(xHor + offsHor, yHor - offsHor, el->x, el->y, offsHor * diagonalConst))
			{
				offsHor++;
				offsVer++;
			}
			offsHor = 1;
			offsVer = 1;

			while (updHeightIfValid(xHor - offsHor, yHor + offsVer, el->x, el->y, offsHor * diagonalConst))
			{
				offsHor++;
				offsVer++;
			}
			offsHor = 1;
			offsVer = 1;

			int gotNeighbours = 0;
			SkeletStruct * neighbour;
			while ((neighbour = el->getNeighbour(gotNeighbours)) != NULL)
			{
				if (!neighbour->marked)
				{
					st.push(neighbour);
				}
				gotNeighbours++;
			}

			if (st.empty())
			{
				neighbour = getFirstUnmarked();
				if (neighbour != NULL)
				{
					st.push(neighbour);
				}
			}

		}


		for (int i = 0; i < linkedSkelet.size(); i++) // calculates the skeleton pixel max distance once all childs are known
		{
			for (int n = 0; n < linkedSkelet[i].size(); n++)
			{
				if (linkedSkelet[i][n] != NULL)
				{
					res.map[i][n].calcSkeletMaxDist();
				}
			}
		}


	}

	Mat printResult()
	{
		Mat result = Mat::zeros(inputBin.size(), CV_8UC3);
		map<double, vector<int>> uniqueVals;
		map<double, vector<int>>::iterator it;

		for (int i = 0; i < res.map.size(); i++)
		{
			for (int n = 0; n < res.map[i].size(); n++)
			{
				double rndH = round(res.map[i][n].height);

				if (uniqueVals.find(rndH) == uniqueVals.end())
				{

					vector<int> color;
					if (rndH < 0)
					{
						color = { 0, 0, 0 };
					}
					else
					{
						color = { Utility::random(0, 256), Utility::random(0, 256), Utility::random(0, 256) };
					}
					cout << "height " << rndH << endl;
					
					uniqueVals.insert(pair<double, vector<int>>(rndH, color));
				}
			}
		}

		for (int i = 0; i < res.map.size(); i++)
		{
			for (int n = 0; n < res.map[i].size(); n++)
			{
				double rndH = round(res.map[i][n].height);

				it = uniqueVals.find(rndH);
				result.at<Vec3b>(i, n)[0] = it->second[0];
				result.at<Vec3b>(i, n)[1] = it->second[1];
				result.at<Vec3b>(i, n)[2] = it->second[2];
			}
		}

		return result;
	}

	void printResMatrix()
	{
		ofstream out("test.txt");

		for (int i = 0; i < res.map.size(); i++)
		{
			for (int n = 0; n < res.map.size(); n++)
			{
				out << res.map[i][n].height << " ";
			}
			out << "\n";
		}
	}

	void printLinkedSkel(Mat res)
	{
		resetLinkSkelMarks();

		int calledSlow = 0;
		SkeletStruct * el = getFirstUnmarked();
		while(el != NULL)
		{
			calledSlow++;
			el->marked = true;
			res.at<uchar>(el->y, el->x) = (uchar)255;
		
			int walkedNeighbours = 0;
			SkeletStruct * neighbour;
			while ((neighbour = el->getNeighbour(walkedNeighbours)) != NULL)
			{
				neighbour->marked = true;
				res.at<uchar>(neighbour->y, neighbour->x) = (uchar)255;
				walkedNeighbours++;
			}

			el = getFirstUnmarked();

		}

		
		int totalPix = 0;
		for (int i = 0; i < linkedSkelet.size(); i++)
		{
			for (int n = 0; n < linkedSkelet[i].size(); n++)
			{
				if (linkedSkelet[i][n] != NULL)
				{
					totalPix++; 
				}
			}
		}

		cout << "slow " << calledSlow << " total " << totalPix << endl;
	}
};