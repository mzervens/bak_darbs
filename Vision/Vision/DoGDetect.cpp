#pragma once;

#include <opencv2/core.hpp>
#include <opencv2/imgproc.hpp>
#include <iostream>
#include <string>
#include <vector>
#include "ImgProc.cpp";
#include "Filter.cpp";

class DoGDetect
{
	Mat colorImg;
	Mat valueImg;

	void getValImg()
	{
		Mat hsv;
		cvtColor(colorImg, hsv, CV_BGR2HSV);

		vector<Mat> channels;
		split(hsv, channels);


		valueImg = channels[2];
		return;
	}

public:
	DoGDetect(Mat img)
	{
		colorImg = img;
	}

	vector<Mat> detect(double low = 1, double high = 3)
	{
		getValImg();

		Mat smallVeins = valueImg.clone();

		GaussianBlur(smallVeins, smallVeins, Size(5, 5), 0);

		int horSize = smallVeins.cols * 0.1;
		int verSize = smallVeins.rows * 0.1;

		if (horSize % 2 == 0)
		{
			horSize++;
		}

		if (verSize % 2 == 0)
		{
			verSize++;
		}

		cout << "hor ver size " << horSize << " " << verSize << endl;


		return vector<Mat>({ImgProc::otsusThresh(Filter::DoG_exp(smallVeins, low, high, Size(horSize, verSize), Size(horSize, verSize))) });


	}


};