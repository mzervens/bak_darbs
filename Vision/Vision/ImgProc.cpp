#pragma once;

#include <opencv2/core.hpp>
#include <opencv2/imgproc.hpp>
#include <iostream>
#include <string>
#include <ppl.h>
#include <stack>
#include "Structs.cpp";
#include "Utility.cpp";
#include "vec2.h";

using namespace std;
using namespace cv;
using namespace concurrency;

class ImgProc
{
public:
	static Mat invertBin(Mat img)
	{
		for (int i = 0; i < img.rows; i++)
		{
			for (int n = 0; n < img.cols; n++)
			{
				int val = img.at<uchar>(i, n);
				if (val > 0)
				{
					img.at<uchar>(i, n) = (uchar)0;
				}
				else
				{
					img.at<uchar>(i,n) = (uchar)255;
				}
			}
		}

		return img;
	}

	


	static Mat veinIncreseGrey(Mat im)
	{
		Mat res(im.size(), CV_8UC1, Scalar(0));
		for (int y = 0; y < im.rows; y++)
		{
			for (int x = 0; x < im.cols; x++)
			{
				auto pix = im.at<Vec3b>(y, x);
				res.at<uchar>(y, x) = std::min((int)(1 * pix[0] + 1 * pix[2] + 0.05 * pix[1]), 255);
			}
		}

		return res;
	}

	static Mat properGreyscale(Mat im)
	{
		Mat grey(im.size(), CV_8UC1, Scalar(0));
		double pak1 = 2.2;
		double pak2 = 1 / 2.2;

		for (int y = 0; y < im.rows; y++)
		{
			for (int x = 0; x < im.cols; x++)
			{
				auto pix = im.at<Vec3b>(y, x);
				grey.at<uchar>(y, x) = pow(0.2126 * pow(pix[2], pak1) + 0.7152 * pow(pix[1], pak1) + 0.0722 * pow(pix[0], pak1), pak2);
			}
		}

		return grey;
	}

	static Mat otsusThresh(Mat im)
	{
		Mat res;
		threshold(im, res, 0, 255, CV_THRESH_BINARY | CV_THRESH_OTSU);
		return res;
	}


	static Mat inv(Mat img)
	{
		return Scalar::all(255) - img;
	}

	static Mat elipseErode(Mat binImg, int dilation_size = 1)
	{
		Mat res;
		Mat strEl = getStructuringElement(MORPH_ELLIPSE, Size(2 * dilation_size + 1, 2 * dilation_size + 1),
			Point(dilation_size, dilation_size));

		erode(binImg, res, strEl);

		return res;
	}


	static Mat elipseDilate(Mat binImg, int dilation_size = 1)
	{
		Mat res;
		Mat strEl = getStructuringElement(MORPH_ELLIPSE, Size(2 * dilation_size + 1, 2 * dilation_size + 1),
			Point(dilation_size, dilation_size));

		dilate(binImg, res, strEl);

		return res;
	}

	static Mat getBinaryBorder(Mat img)
	{
		Mat black(img.size(), CV_8UC1, Scalar(0));
		for (int i = 0; i < img.rows; i++)
		{
			for (int n = 0; n < img.cols; n++)
			{
				if (img.at<uchar>(i, n) == 0)
				{
					continue;
				}

				int kern = 1;
				int foundTresh = 2;
				int foundEmpty = 0;

				for (int yOfs = -kern; yOfs <= kern; yOfs++)
				{
					bool found = false;
					for (int xOfs = -kern; xOfs <= kern; xOfs++)
					{
						int x = yOfs + n;
						int y = xOfs + i;

						if (x < 0 || x >= img.cols || y < 0 || y >= img.rows)
						{
							foundEmpty++;

							if (foundEmpty >= foundTresh)
							{
								black.at<uchar>(i, n) = (uchar)255;
								found = true;
								break;
							}
							else
							{
								continue;
							}
						}

						if (img.at<uchar>(y, x) == 0) // pixel is a part of border
						{
							foundEmpty++;

							if (foundEmpty >= foundTresh)
							{
								black.at<uchar>(i, n) = (uchar)255;
								found = true;
								break;
							}
							else
							{
								continue;
							}
						}

					}

					if (found)
					{
						break;
					}
				}

			}
		}

		for (int i = 1; i < img.rows - 1; i++)
		{
			for (int n = 1; n < img.cols - 1; n++)
			{
				if (img.at<uchar>(i, n) == 0 || black.at<uchar>(i, n) == 255)
				{
					continue;
				}

				int kern = 1;

				int numBlacks = 0;
				auto lastBlackPos = pair<int, int>();

				for (int yOfs = -kern; yOfs <= kern; yOfs++)
				{
					for (int xOfs = -kern; xOfs <= kern; xOfs++)
					{
						int x = yOfs + n;
						int y = xOfs + i;

						if (img.at<uchar>(y, x) == 0)
						{
							numBlacks++;
							lastBlackPos = pair<int, int>(x, y);
						}
					}
				}

				if (numBlacks == 1)
				{
					auto topLeft = pair<int, int>(n - 1, i - 1);
					auto topRight = pair<int, int>(n + 1, i - 1);
					auto botLeft = pair<int, int>(n - 1, i + 1);
					auto botRight = pair<int, int>(n + 1, i + 1);

					auto top = pair<int, int>(n, i - 1);
					auto left = pair<int, int>(n - 1, i);
					auto right = pair<int, int>(n + 1, i);
					auto bot = pair<int, int>(n, i + 1);

					pair<int, int> corn1, corn2;
					bool set = false;

					if (lastBlackPos == top)
					{
						corn1 = topLeft;
						corn2 = topRight;
						set = true;
					}
					else if (lastBlackPos == left)
					{
						corn1 = topLeft;
						corn2 = botLeft;
						set = true;
					}
					else if (lastBlackPos == right)
					{
						corn1 = topRight;
						corn2 = botRight;
						set = true;
					}
					else if (lastBlackPos == bot)
					{
						corn1 = botLeft;
						corn2 = botRight;
						set = true;
					}

					if (set &&
						black.at<uchar>(corn1.second, corn1.first) == 255 &&
						black.at<uchar>(corn2.second, corn2.first) == 255)
					{
						black.at<uchar>(lastBlackPos.second, lastBlackPos.first) = (uchar)255;
					}
				}

			}
		}

		return black;
	}

private:

	static void joinPixelFragsVert(Mat &bin, Mat &res, int x, int y, int maxGap)
	{
		int topY = y - maxGap - 1;
		int botY = y + maxGap + 1;

		if ((y - 1) >= 0 && bin.at<uchar>(y - 1, x) == 0)
		{
			while (topY < 0 || bin.at<uchar>(topY, x) <= 0)
			{
				topY++;
			}
			while (topY != y)
			{
				res.at<uchar>(topY, x) = (uchar)255;
				topY++;
			}
		}

		if ((y + 1) < bin.rows && bin.at<uchar>(y + 1, x) == 0)
		{
			while (botY >= bin.rows || bin.at<uchar>(botY, x) <= 0)
			{
				botY--;
			}
			while (botY != y)
			{
				res.at<uchar>(botY, x) = (uchar)255;
				botY--;
			}
		}
	}

	static void joinPixelFragsHor(Mat &bin, Mat &res, int x, int y, int maxGap)
	{
		int leftX = x - maxGap - 1;
		int rightX = x + maxGap + 1;

		if ((x - 1) >= 0 && bin.at<uchar>(y, x - 1) == 0)
		{
			while (leftX < 0 || bin.at<uchar>(y, leftX) <= 0)
			{
				leftX++;
			}
			while (leftX != x)
			{
				res.at<uchar>(y, leftX) = (uchar)255;
				leftX++;
			}
		}

		if ((x + 1) < bin.cols && bin.at<uchar>(y, x + 1) == 0)
		{
			while (rightX >= bin.cols || bin.at<uchar>(y, rightX) <= 0)
			{
				rightX--;
			}
			while (rightX != x)
			{
				res.at<uchar>(y, rightX) = (uchar)255;
				rightX--;
			}
		}
	}

	/**
	 *	@leftRight If true then joining by diagonal from top left corner to bottom right corner
	 *			   otherwise from top right corner to bottom left corner
	 */
	static Mat joinPixelFragsDiag(Mat &bin, Mat &res, int x, int y, int maxGap, bool leftRight = true)
	{
		int offsetX = -1;
		int offsetY = -1;
		int topX = x, topY = y;
		int botX = x, botY = y;

		if (!leftRight)
		{
			offsetX = 1;
		}

		for (int i = 0; i < maxGap + 1; i++)
		{
			topX += offsetX;
			topY += offsetY;

			botX -= offsetX;
			botY -= offsetY;
		}

		if ((x + offsetX) >= 0 && (x + offsetX) < bin.cols &&
			(y + offsetY) >= 0 && (y + offsetY) < bin.rows &&
			bin.at<uchar>(y + offsetY, x + offsetX) == 0)
		{

			while ((topX < 0 || topX >= bin.cols) || (topY < 0 || topY >= bin.rows) || bin.at<uchar>(topY, topX) == 0)
			{
				topX -= offsetX;
				topY -= offsetY;
			}

			while (topX != x && topY != y)
			{
				res.at<uchar>(topY, topX) = (uchar)255;
				topX -= offsetX;
				topY -= offsetY;
			}
		}



		if ((x - offsetX) >= 0 && (x - offsetX) < bin.cols &&
			(y - offsetY) >= 0 && (y - offsetY) < bin.rows &&
			bin.at<uchar>(y - offsetY, x - offsetX) == 0)
		{

			while ((botX < 0 || botX >= bin.cols) || (botY < 0 || botY >= bin.rows) || bin.at<uchar>(botY, botX) == 0)
			{
				botX += offsetX;
				botY += offsetY;
			}

			while (botX != x && botY != y)
			{
				res.at<uchar>(botY, botX) = (uchar)255;
				botX += offsetX;
				botY += offsetY;
			}
		}

		return res;
	}

public:

	/**
	 *	@bin a binary image
	 *	@maxGap should be larger or equal to 1
	 */
	static Mat joinPixelFragments(Mat bin, int maxGap = 1)
	{
		Mat resHor = bin.clone();
		Mat resVer = bin.clone();
		Mat resDiagLR = bin.clone();
		Mat resDiagRL = bin.clone();

		for (int i = 0; i < bin.rows; i++)
		{
			for (int n = 0; n < bin.cols; n++)
			{
				if (bin.at<uchar>(i, n) == 0)
				{
					continue;
				}

				//vertical
				joinPixelFragsVert(bin, resVer, n, i, maxGap);
				//horizontal
				joinPixelFragsHor(bin, resHor, n, i, maxGap);
				// vertical \ 
				joinPixelFragsDiag(bin, resDiagLR, n, i, maxGap);
				// vertical /
				joinPixelFragsDiag(bin, resDiagRL, n, i, maxGap, false);

			}
		}

		Mat horVer = mergeBinImgs(resHor, resVer);
		Mat bothDiag = mergeBinImgs(resDiagLR, resDiagRL);

		return mergeBinImgs(horVer, bothDiag);
	}

private:
	static Triple<vector<vector<int>>, bool, pair<int, int>> gatherRegionPixelVals(Mat grey, int x, int y, int size)
	{
		int diagonalRad = floor((double)size / 2);
		int topX = (x - diagonalRad);
		int topY = (y - diagonalRad);
		int currX, currY;
		
		vector<vector<int>> res = vector<vector<int>>();
		for (int i = 0; i < size; i++)
		{
			res.push_back(vector<int>(size));
		}

		// gets the pixel values of the region of interest
		for (int i = 0; i < size; i++)
		{
			currY = topY + i;

			for (int n = 0; n < size; n++)
			{
				currX = topX + n;

				if ((currY < 0 || currY >= grey.rows) || (currX < 0 || currX >= grey.cols))
				{
					res[i][n] = -1;
					continue;
				}

				uchar pixelVal = grey.at<uchar>(currY, currX);
				res[i][n] = (int)pixelVal;
			}
		}

		bool connected = isConnectedToBorder(grey, res, topX, topY);
		return Triple<vector<vector<int>>, bool, pair<int, int>>(res, connected, pair<int, int>(topX, topY));
	}

	static bool isConnectedToBorder(Mat grey, vector<vector<int>> &res, int topX, int topY)
	{
		int offset = 1;

		// checks for top and bottom border
		for (int i = 0; i < res.size(); i += res.size() - 1)
		{
			for (int n = 0; n < res[0].size(); n++)
			{
				if (res[i][n] > 0)
				{
					int leftX = topX + n - 1;
					int leftY = topY + i - offset;

					for (int j = 0; j < 3; j++)
					{
						if ((leftY >= 0 && leftY < grey.rows) && ((leftX + j) >= 0 && (leftX + j) < grey.cols))
						{
							if (grey.at<uchar>(leftY, leftX + j) > 0)
							{
								return true;
							}
						}
					}

				}
			}

			offset = -1;
		}

		offset = 1;
		// checks for left and right border
		for (int i = 0; i < res[0].size(); i += res[0].size() - 1)
		{
			for (int n = 0; n < res.size(); n++)
			{
				if (res[n][i] > 0)
				{
					int nTopX = topX + i - offset;
					int nTopY = topY + n - 1;

					for (int j = 0; j < 3; j++)
					{
						if (((nTopY + j) >= 0 && (nTopY + j) < grey.rows) && (nTopX >= 0 && nTopX < grey.cols))
						{
							if (grey.at<uchar>(nTopY + j, nTopX) > 0)
							{
								return true;
							}
						}
					}

				}
			}

			offset = -1;
		}

		return false;
	}

public:

	static bool inBoundsOfVoxelGridWithVal(int x, int y, int z, double val, vector<vector<vector<double>>> &grid)
	{
		if (y < 0 || y >= grid.size() || x < 0 || x >= grid[y].size() || z < 0 || z >= grid[y][x].size() || grid[y][x][z] != val)
		{
			return false;
		}
		return true;
	}

	static bool inBoundsWithVal(Mat &img, vec2<int> xy, int val)
	{
		if (xy.x < 0 || xy.x >= img.cols || xy.y < 0 || xy.y >= img.rows || img.at<uchar>(xy.y, xy.x) != val)
		{
			return false;
		}

		return true;
	}

	static bool inBoundsWithVal(Mat &img, pair<int, int> xy, int val)
	{
		if (inBounds(img, xy) && img.at<uchar>(xy.second, xy.first) == val)
		{
			return true;
		}

		return false;
	}

	static bool inBounds(Mat &img, vec2<int> xy)
	{
		return inBounds(img, make_pair(xy.x, xy.y));
	}

	static bool inBounds(Mat &img, pair<int, int> xy)
	{
		int x = xy.first;
		int y = xy.second;

		if (x < 0 || x >= img.cols || y < 0 || y >= img.rows)
		{
			return false;
		}

		return true;
	}

	static Mat getLargestRegion(Mat bin)
	{
		Mat visited(bin.size(), CV_8UC1, Scalar(0));
		Mat result(bin.size(), CV_8UC1, Scalar(0));
		auto regions = vector<vector<pair<int, int>>>();

		for (int i = 0; i < bin.rows; i++)
		{
			for (int n = 0; n < bin.cols; n++)
			{
				if (bin.at<uchar>(i, n) == 0 || visited.at<uchar>(i, n) == 255)
				{
					continue;
				}

				auto region = vector<pair<int, int>>();
				auto st = stack <pair<int, int>>();
				st.push(make_pair(n, i));
				visited.at<uchar>(i, n) = (uchar)255;

				while (!st.empty())
				{
					auto curPix = st.top();
					region.push_back(curPix);
					st.pop();
					int kern = 1;

					for (int yOfs = -kern; yOfs <= kern; yOfs++)
					{
						for (int xOfs = -kern; xOfs <= kern; xOfs++)
						{
							int x = curPix.first + xOfs;
							int y = curPix.second + yOfs;

							if (x < 0 || x >= visited.cols || y < 0 || y >= visited.rows ||
								visited.at<uchar>(y, x) == 255 || bin.at<uchar>(y, x) == 0)
							{
								continue;
							}

							visited.at<uchar>(y, x) = (uchar)255;
							st.push(make_pair(x, y));
						}
					}
				}

				regions.push_back(region);

			}
		}

		int maxReg = 0;
		vector<pair<int, int>> largReg;
		for (int i = 0; i < regions.size(); i++)
		{
			if (regions[i].size() > maxReg)
			{
				maxReg = regions[i].size();
				largReg = regions[i];
			}
		}


		for (int i = 0; i < largReg.size(); i++)
		{
			result.at<uchar>(largReg[i].second, largReg[i].first) = 255;
		}

		return result;
	}

	static Mat dropRegionsCloseToAvg(Mat bin, double perc)
	{
		Mat visited(bin.size(), CV_8UC1, Scalar(0));
		Mat result(bin.size(), CV_8UC1, Scalar(0));
		auto regions = vector<vector<pair<int, int>>>();

		for (int i = 0; i < bin.rows; i++)
		{
			for (int n = 0; n < bin.cols; n++)
			{
				if (bin.at<uchar>(i, n) == 0 || visited.at<uchar>(i, n) == 255)
				{
					continue;
				}

				auto region = vector<pair<int, int>>();
				auto st = stack <pair<int, int>>();
				st.push(make_pair(n, i));
				visited.at<uchar>(i, n) = (uchar)255;

				while (!st.empty())
				{
					auto curPix = st.top();
					region.push_back(curPix);
					st.pop();
					int kern = 1;

					for (int yOfs = -kern; yOfs <= kern; yOfs++)
					{
						for (int xOfs = -kern; xOfs <= kern; xOfs++)
						{
							int x = curPix.first + xOfs;
							int y = curPix.second + yOfs;

							if (x < 0 || x >= visited.cols || y < 0 || y >= visited.rows ||
								visited.at<uchar>(y, x) == 255 || bin.at<uchar>(y, x) == 0)
							{
								continue;
							}

							visited.at<uchar>(y, x) = (uchar)255;
							st.push(make_pair(x, y));
						}
					}
				}

				regions.push_back(region);

			}
		}

		int numRegs = regions.size();
		int pixelsInRegions = 0;
		for (int i = 0; i < numRegs; i++)
		{
			pixelsInRegions += regions[i].size();
		}

		double avgPixInReg = (double)pixelsInRegions / numRegs;
		double avgDelt = perc * avgPixInReg;
		auto outRegions = vector<vector<pair<int, int>>>();

		for (int i = 0; i < numRegs; i++)
		{
			double thresh = abs(regions[i].size() - avgPixInReg);
			if (thresh <= avgDelt)
			{
				continue;
			}
			else
			{
				outRegions.push_back(regions[i]);
			}
		}

		for (int i = 0; i < outRegions.size(); i++)
		{
			for (int n = 0; n < outRegions[i].size(); n++)
			{
				result.at<uchar>(outRegions[i][n].second, outRegions[i][n].first) = (uchar)255;
			}
		}

		return result;
	}

	static Mat dropRegionsBelowAvg(Mat bin)
	{
		Mat visited(bin.size(), CV_8UC1, Scalar(0));
		Mat result(bin.size(), CV_8UC1, Scalar(0));
		auto regions = vector<vector<pair<int, int>>>();

		for (int i = 0; i < bin.rows; i++)
		{
			for (int n = 0; n < bin.cols; n++)
			{
				if (bin.at<uchar>(i, n) == 0 || visited.at<uchar>(i, n) == 255)
				{
					continue;
				}

				auto region = vector<pair<int, int>>();
				auto st = stack <pair<int, int>>();
				st.push(make_pair(n, i));
				visited.at<uchar>(i, n) = (uchar)255;

				while (!st.empty())
				{
					auto curPix = st.top();
					region.push_back(curPix);
					st.pop();
					int kern = 1;

					for (int yOfs = -kern; yOfs <= kern; yOfs++)
					{
						for (int xOfs = -kern; xOfs <= kern; xOfs++)
						{
							int x = curPix.first + xOfs;
							int y = curPix.second + yOfs;

							if (x < 0 || x >= visited.cols || y < 0 || y >= visited.rows || 
								visited.at<uchar>(y, x) == 255 || bin.at<uchar>(y, x) == 0)
							{
								continue;
							}

							visited.at<uchar>(y, x) = (uchar)255;
							st.push(make_pair(x, y));
						}
					}
				}

				regions.push_back(region);

			}
		}

		int numRegs = regions.size();
		int pixelsInRegions = 0;
		for (int i = 0; i < numRegs; i++)
		{
			pixelsInRegions += regions[i].size();
		}

		double avgPixInReg = (double)pixelsInRegions / numRegs;
		auto outRegions = vector<vector<pair<int, int>>>();

		for (int i = 0; i < numRegs; i++)
		{
			if (regions[i].size() <= avgPixInReg)
			{
				continue;
			}
			else
			{
				outRegions.push_back(regions[i]);
			}
		}

		for (int i = 0; i < outRegions.size(); i++)
		{
			for (int n = 0; n < outRegions[i].size(); n++)
			{
				result.at<uchar>(outRegions[i][n].second, outRegions[i][n].first) = (uchar)255;
			}
		}
		
		return result;
	}


	static void clearRegion(Mat &grey, int topX, int topY, int size)
	{
		int currY, currX;
		for (int i = 0; i < size; i++)
		{
			currY = topY + i;

			for (int n = 0; n < size; n++)
			{
				currX = topX + n;
				if ((currY < 0 || currY >= grey.rows) || (currX < 0 || currX >= grey.cols))
				{
					continue;
				}

				grey.at<uchar>(currY, currX) = (uchar)0;
			}
		}
	}

	static Mat removeIsolatedRegions(Mat bin, int maxSize)
	{
		Mat res = bin.clone();
		parallel_for(0, bin.rows, 1, [&](int i)
		{
			parallel_for(0, bin.cols, 1, [&](int n)
			{
				Triple<vector<vector<int>>, bool, pair<int, int>> region = gatherRegionPixelVals(bin, n, i, maxSize);
				if (!region.scnd)
				{
					clearRegion(res, region.thrd.first, region.thrd.second, maxSize);
				}
			});
		});

		return res;
	}

	/**
		Calls removeIsolatedSpecks(...) till there is no change in the resulting image
	 */
	static Mat removeIsolatedSpecksTillNoChange(Mat bin)
	{
		bool loop = true;
		Mat res = bin.clone();

		while (loop)
		{
			res = removeIsolatedSpecks(res, &loop);
		}

		return res;
	}

	/**
		Border pixels of the image are not changed 
		Removes white pixels that are enclosed by a black border
	 */
	static Mat removeIsolatedSpecks(Mat bin, bool *change = NULL)
	{
		Mat res = bin.clone();
		bool removed = false;

		for (int i = 1; i < bin.rows - 1; i++)
		{
			for (int n = 1; n < bin.cols - 1; n++)
			{
				if (bin.at<uchar>(i, n) == 0)
				{
					continue;
				}
				else
				{
					uchar border[8] = { bin.at<uchar>(i - 1, n - 1), bin.at<uchar>(i - 1, n), bin.at<uchar>(i - 1, n + 1),
										bin.at<uchar>(i, n - 1),							  bin.at<uchar>(i, n + 1),
										bin.at<uchar>(i + 1, n - 1), bin.at<uchar>(i + 1, n), bin.at<uchar>(i + 1, n + 1) };
					
					bool isolated = true;
					for (int j = 0; j < 8; j++)
					{
						if (border[j] != 0)
						{
							isolated = false;
							break;
						}
					}

					if (isolated)
					{
						removed = true;
						res.at<uchar>(i, n) = (uchar)0;
					}

				}

			}
		}

		if (change != NULL)
		{
			*change = removed;
		}

		return res;
	}


	static Mat GBRToHSI(Mat src)
	{
		Mat hsi(src.rows, src.cols, src.type());

		float r, g, b, h, s, in;

		for (int i = 0; i < src.rows; i++)
		{
			for (int j = 0; j < src.cols; j++)
			{
				b = src.at<Vec3b>(i, j)[0];
				g = src.at<Vec3b>(i, j)[1];
				r = src.at<Vec3b>(i, j)[2];

				in = (b + g + r) / 3;

				int min_val = 0;
				min_val = std::min(r, std::min(b, g));

				s = 1 - 3 * (min_val / (b + g + r));
				if (s < 0.00001)
				{
					s = 0;
				}
				else if (s > 0.99999){
					s = 1;
				}

				if (s != 0)
				{
					h = 0.5 * ((r - g) + (r - b)) / sqrt(((r - g)*(r - g)) + ((r - b)*(g - b)));
					h = acos(h);

					if (b <= g)
					{
						h = h;
					}
					else{
						h = ((360 * 3.14159265) / 180.0) - h;
					}
				}

				hsi.at<Vec3b>(i, j)[0] = (h * 180) / 3.14159265;
				hsi.at<Vec3b>(i, j)[1] = s * 100;
				hsi.at<Vec3b>(i, j)[2] = in;
			}
		}

		return hsi;
	}


	static Mat doThreshold(Mat img, int thresh, int maxVal, ThresholdTypes type)
	{
		Mat res;
		threshold(img, res, thresh, maxVal, type);
		return res;
	}

	/**
	 * The input image will be lost after this function has finished
	 */
	static Mat skeleton(Mat binImg)
	{
		Mat res(binImg.size(), CV_8UC1, Scalar(0));
		Mat tmp(binImg.size(), CV_8UC1);

		Mat elem = getStructuringElement(MORPH_ELLIPSE, Size(3, 3));

		bool done;
		do
		{
			morphologyEx(binImg, tmp, MORPH_OPEN, elem);
			bitwise_not(tmp, tmp);
			bitwise_and(binImg, tmp, tmp);
			bitwise_or(res, tmp, res);
			erode(binImg, binImg, elem);

			double max;
			minMaxLoc(binImg, 0, &max);
			done = (max == 0);
		} while (!done);

		return res;
	}

	static Triple<Mat, vector<Vec4i>, vector<vector<Point>>> curvesFromBin(Mat binImg, bool drawToImg = false)
	{
		Mat curvesDrawn = Mat::zeros(binImg.size(), CV_8UC3);
		vector<vector<Point>> contours;
		vector<Vec4i> hierarchy;

		findContours(binImg, contours, hierarchy, RETR_TREE, CHAIN_APPROX_SIMPLE, Point(0, 0));

		if (drawToImg)
		{
			for (size_t i = 0; i< contours.size(); i++)
			{
				Scalar color = Scalar(Utility::random(0, 256), Utility::random(0, 256), Utility::random(0, 256));
				//Scalar color = Scalar(255, 255, 255);
				drawContours(curvesDrawn, contours, (int)i, color, 1, 8, hierarchy, 0, Point());
			}
		}

		return Triple<Mat, vector<Vec4i>, vector<vector<Point>>>(curvesDrawn, hierarchy, contours);
	}

	// Pixels combines with OR logic
	// Return image is of same size as first parameter
	// Second parameter must be larger or equal to the first parameter
	static Mat mergeBinImgs(Mat binImg1, Mat binImg2)
	{
		Mat res(binImg1.size(), CV_8UC1);

		for (int i = 0; i < binImg1.rows; i++)
		{
			for (int n = 0; n < binImg1.cols; n++)
			{
				if ((int)(binImg1.at<uchar>(i, n)) > 0 || (int)(binImg2.at<uchar>(i, n)) > 0)
				{
					res.at<uchar>(i, n) = (uchar)255;
				}
				else
				{
					res.at<uchar>(i, n) = (uchar)0;
				}
			}
		}

		return res;
	}


	static Mat mergeBinImgToColor(Mat binImg1, Vec3b col1, Mat binImg2, Vec3b col2)
	{
		Mat res(binImg1.size(), CV_8UC3);
		Vec3b emptyCol(0, 0, 0);

		for (int i = 0; i < binImg1.rows; i++)
		{
			for (int n = 0; n < binImg1.cols; n++)
			{
				if ((int)(binImg1.at<uchar>(i, n)) > 0) 
				{
					res.at<Vec3b>(i, n) = col1;
				}
				else if((int)(binImg2.at<uchar>(i, n)) > 0)
				{
					res.at<Vec3b>(i, n) = col2;
				}
				else
				{
					res.at<Vec3b>(i, n) = emptyCol;
				}
			}
		}

		return res;
	}



};